﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemyAI : EnemyAI
{
	//[SerializeField] private float 

	// Start is called before the first frame update
	void Start()
	{
        StartCoroutine(movementLogic());
        //StartCoroutine(fireballShooting());
        //StartCoroutine(buildMachineGun());
        //StartCoroutine(buildDemolisher2());
        //StartCoroutine(iceShard());
        StartCoroutine(darkOrb());
        //StartCoroutine(gargoylesSwarm());
	}

	// Update is called once per frame
	void Update()
	{

	}

	IEnumerator movementLogic()
	{
		yield return new WaitForSeconds(2f);
		int moveLeftProba;
		int noMoveProba;
		Champion currentChampion;
		//int moveRightProba; turns out not used
		while (true)
		{
			currentChampion = championsHandler.CurrentChampion;
			// make it more likely to go left, or right by looking at the position
			if (currentChampion.transform.position.x <= 1)
			{
				moveLeftProba = 0;
				noMoveProba = 15;
				//moveRightProba = 85;
			}
			else if (currentChampion.transform.position.x >= 8)
			{
				moveLeftProba = 85;
				noMoveProba = 15;
				//moveRightProba = 0;
			}
			else if (currentChampion.transform.position.x > 4.5)
			{
				moveLeftProba = 40;
				noMoveProba = 40;
				//moveRightProba = 20;
			}
			else
			{
				moveLeftProba = 40;
				noMoveProba = 40;
				//moveRightProba = 60;
			}

			int randomNum = Random.Range(0, 100);
			if (randomNum <= moveLeftProba)
			{
				sendMoveCommand(GameConstant.MoveDirection.STOP_MOVE_RIGHT);
				sendMoveCommand(GameConstant.MoveDirection.LEFT);
			}
			else if (randomNum <= moveLeftProba + noMoveProba)
			{
				sendMoveCommand(GameConstant.MoveDirection.STOP_MOVE_LEFT);
				sendMoveCommand(GameConstant.MoveDirection.STOP_MOVE_RIGHT);
			}

			else
			{
				sendMoveCommand(GameConstant.MoveDirection.STOP_MOVE_LEFT);
				sendMoveCommand(GameConstant.MoveDirection.RIGHT);
			}

			float secondsToWait = Random.Range(0.2f, 0.5f);
			yield return new WaitForSeconds(secondsToWait);
		}
	}

	IEnumerator fireballShooting()
	{
		yield return new WaitForSeconds(4f);
		sendSelectCommand(GameConstant.SpellcasterButton_Int.THIRD);
		while (true) {
            float posX = Random.Range(0, GameConstant.CONVERTED_RESO_X);
			float posY = Random.Range(0, GameConstant.CONVERTED_RESO_Y);
			sendCastCommand(new Vector2(posX, posY));
			yield return new WaitForSeconds(5f);
		}
	}

    IEnumerator iceShard()
    {
        yield return new WaitForSeconds(6f);
        sendSelectCommand(GameConstant.SpellcasterButton_Int.SECOND);
        while (true)
        {
            float posX = Random.Range(0, GameConstant.CONVERTED_RESO_X);
            float posY = Random.Range(0, GameConstant.CONVERTED_RESO_Y);
            sendCastCommand(new Vector2(posX, posY));
            float chargingTime = Random.Range(1, 4);
            yield return new WaitForSeconds(chargingTime);
            sendReleaseCommand();
            yield return new WaitForSeconds(8f);
        }
    }
    
    IEnumerator darkOrb()
    {
        yield return new WaitForSeconds(5f);
        sendSelectCommand(GameConstant.SpellcasterButton_Int.FIRST);
        while (true)
        {
            float posX = UnitsTracker.PlayerChampion.transform.position.x * GameConstant.PIXEL_PER_UNIT;
            float posY = UnitsTracker.PlayerChampion.transform.position.y * GameConstant.PIXEL_PER_UNIT;
            sendCastCommand(new Vector2(posX, posY));
            yield return new WaitForSeconds(5f);
        }
    }

    IEnumerator gargoylesSwarm()
    {
        yield return new WaitForSeconds(5f);
        sendSelectCommand(GameConstant.SpellcasterButton_Int.FIRST);
        while (true)
        {
            float posX = UnitsTracker.PlayerChampion.transform.position.x * GameConstant.PIXEL_PER_UNIT;
            float posY = UnitsTracker.PlayerChampion.transform.position.y * GameConstant.PIXEL_PER_UNIT;
            sendCastCommand(new Vector2(posX, posY));
            yield return new WaitForSeconds(6f);
        }
    }

    IEnumerator buildMachineGun()
	{
		yield return new WaitForSeconds(5f);
		sendSelectCommand(GameConstant.SpellcasterButton_Int.SECOND);
		while (true)
		{
			float posX = Random.Range(0, GameConstant.CONVERTED_RESO_X);
			float posY = Random.Range(0, GameConstant.CONVERTED_RESO_Y);
			sendCastCommand(new Vector2(posX, posY));
			sendReleaseCommand();
			yield return new WaitForSeconds(6f);
		}
	}

	IEnumerator buildDemolisher()
	{
		yield return new WaitForSeconds(5f);
		sendSelectCommand(GameConstant.SpellcasterButton_Int.THIRD);
		while (true)
		{
			float posX = Random.Range(0, GameConstant.CONVERTED_RESO_X);
			float posY = Random.Range(0, GameConstant.CONVERTED_RESO_Y);
			sendCastCommand(new Vector2(posX, posY));
			sendReleaseCommand();
			yield return new WaitForSeconds(6f);
		}
	}

	IEnumerator buildDemolisher2()
	{
		yield return new WaitForSeconds(1f);
		sendSelectCommand(GameConstant.SpellcasterButton_Int.THIRD);

		//Build a demolisher at fourth row first column
		float posX = 100;
		float posY = 800;
		sendCastCommand(new Vector2(posX, posY));
		sendReleaseCommand();
		yield return new WaitForSeconds(3f);

		Building b = UnitsTracker.getBuildingOnMapById(40);
		//b.userSelect();
		//sendSelectCommand(GameConstant.SpellcasterButton_Int.FIRST);
		while (true)
		{
			b.userSelect();
			sendSelectCommand(GameConstant.SpellcasterButton_Int.FIRST);
			posX = Random.Range(0, GameConstant.CONVERTED_RESO_X);
			posY = Random.Range(0, GameConstant.CONVERTED_RESO_Y);
			sendCastCommand(new Vector2(posX, posY));
			yield return new WaitForSeconds(2f);
			sendReleaseCommand();
			yield return new WaitForSeconds(3f);
		}
	}
}