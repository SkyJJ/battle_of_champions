﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCastleTest : MonoBehaviour, IDamageable
{
	[SerializeField] private float castleHp;
	[SerializeField] private Image castleHpUI;
	private float currentCastleHp;


	// Start is called before the first frame update
	void Start()
    {
		currentCastleHp = castleHp;
		castleHpUI.fillAmount = currentCastleHp / castleHp;
	}

	public void receiveDamage(float playerDamage, float buildingDamage)
	{
		//Debug.Log("Enemy castle takes damage");
		currentCastleHp -= buildingDamage;
		updateUI();
	}

	private void updateUI()
	{
		castleHpUI.fillAmount = currentCastleHp / castleHp;
	}

	public void receiveSpellEffects(ActiveSpellEffect[] spellEffects)
	{
		Debug.Log("SpellEffect not yet implemented in castle.");
		return;
	}

	public void removeSpellEffect(ActiveSpellEffect spellEffect)
	{
		Debug.Log("SpellEffect not yet implemented in castle.");
		return;
	}
}
