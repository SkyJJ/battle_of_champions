﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EffectsUIHandler : MonoBehaviour
{

	[SerializeField] private int numOfRings;
	[SerializeField] private Color redBackground;
	[SerializeField] private Color greenBackground;
	[SerializeField] private Image[] ringBackgrounds;
	[SerializeField] private Image[] timings;
	[SerializeField] private Image[] icons;


	private int passiveLength;


	// Start is called before the first frame update
	void Start()
    {
		for (int i = 0; i < numOfRings; i++) {
			ringBackgrounds[i].color = Color.clear;
			timings[i].color = Color.clear;
			icons[i].color = Color.clear;
		}
    }

	/*public void updatePassiveStatus(PassiveSpellEffect[] passives) {
		passiveLength = passives.Length;
		for (int i = 0; i < passiveLength; i++) {
			ringBackgrounds[i].color = greenBackground; //Passive is always buff
			timings[i].color = Color.green;
			timings[i].fillAmount = 1f;
			icons[i].color = Color.white;
			icons[i].sprite = passives[i].Icon;
		}
	}*/

	public void updatePassiveStatus(Dictionary<int, PassiveSpellEffect> passives)
	{
		passiveLength = passives.Count;
		int i = 0;
		foreach (KeyValuePair<int, PassiveSpellEffect> passive in passives) {
			ringBackgrounds[i].color = greenBackground; //Passive is always buff
			timings[i].color = Color.green;
			timings[i].fillAmount = 1f;
			icons[i].color = Color.white;
			icons[i].sprite = passive.Value.Icon;
			i++;
		}
	}

	public void updateBuffDebuffStatus(ActiveSpellEffect[] buffs, ActiveSpellEffect[] debuffs)
	{
		//TODO optimize this function pls. Now you draw the icon everyframe. 
		//This function runs, Every, fking, frame.
		int i = passiveLength - 1;
		//buffs status
		for (int j = 0; j < buffs.Length && i < numOfRings; j++)
		{
			if (!(buffs[i] is NullSpellEffect)) {
				ringBackgrounds[i].color = greenBackground;
				timings[i].color = Color.green;
				timings[i].fillAmount = buffs[i].NormalizedTiming;
				icons[i].color = Color.white;
				icons[i].sprite = buffs[i].Icon;
				i++;
			}
		}

		for (int j = 0; j < debuffs.Length && i < numOfRings; j++)
		{
			if (!(debuffs[i] is NullSpellEffect))
			{
				ringBackgrounds[i].color = redBackground;
				timings[i].color = Color.red;
				timings[i].fillAmount = debuffs[i].NormalizedTiming;
				icons[i].color = Color.white;
				icons[i].sprite = debuffs[i].Icon;
				i++;
			}
		}
	}

}
