﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullCaster : ActiveSpellCaster
{
	private static NullCaster nullCaster;

	private void Awake()
	{
		if (nullCaster == null) {
			tag = GameConstant.NEUTRAL_TAG;
			spellName = "NullCaster";
			nullCaster = this;
		}
		else
			Destroy(this);
	}

	public static NullCaster Instance() {
		return nullCaster;
	}

	public override void cancelCast()
	{
		return;
	}

	public override void cast(Vector2 inputPos)
	{
		return;
	}

	public override void release()
	{
		return;
	}

	public override void select()
	{
		return;
	}
}
