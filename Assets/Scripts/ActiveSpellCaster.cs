﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActiveSpellCaster : SpellCaster
{
	/*
	 * Inherited from SpellCaster : icon, spellName
	 * */
	[SerializeField] protected float manaCost;
	[SerializeField] protected float cooldownTime;
	[SerializeField] protected float precastTime;

	protected float currentCooldownTime;
	protected float currentPrecastTime;
	protected PrefabPool prefabPool;

	protected virtual void Start(){
		currentCooldownTime = 0;
		currentPrecastTime = precastTime;
		//Debug.Log("ActiveSpellCaster name and tag : " + this.spellName + ", " + this.tag);
		prefabPool = new PrefabPool(this.tag);
	}

	public abstract void select();
	public abstract void cast(Vector2 inputPos);
	public abstract void cancelCast();
	public abstract void release();

	public float CurrentTiming {
		get {
			if (currentCooldownTime > 0) //cooling down
				return currentCooldownTime;
			else if (currentPrecastTime < precastTime) //precasting
				return currentPrecastTime;
			else //resting
				return 0;
		}	
	}
	
	public float CurrentNormalizedTiming {
		get {
			if (currentCooldownTime > 0) //cooling down
				return currentCooldownTime / cooldownTime;
			else if (currentPrecastTime < precastTime) //precasting
				return currentPrecastTime / precastTime;
			else //resting
				return 0;
		}
	}

}
