﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class StandardBuildingCaster : ActiveSpellCaster
{
	[SerializeField] private StandardBuilding standardBuildingPrefab; //the complete building
	[SerializeField] private BuildingLocations.BuildingRow buildRow;

	private const int BUILDING_ID = 0;
	private GameObject inUsePreBuildObj;
	private bool casting;

	protected override void Start()
	{
		Assert.IsTrue(!buildRow.Equals(BuildingLocations.BuildingRow.CASTLE));
		Assert.IsTrue(!buildRow.Equals(BuildingLocations.BuildingRow.ENEMY_CASTLE));
		base.Start();
		casting = false;
		prefabPool.addToPool(BUILDING_ID, standardBuildingPrefab.gameObject, 2);
	}

	void Update()
	{
		if (currentCooldownTime > 0)
		{
			currentCooldownTime -= Time.deltaTime;
		}
	}

	public override void select()
	{
		return;
	}

	public override void cast(Vector2 inputPos)
	{
		if (currentCooldownTime <= 0)
		{
			if (casting)
			{
				inUsePreBuildObj.transform.position = calculateBuildingLocation(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
			}
			else
			{
				casting = true;
				inUsePreBuildObj = prefabPool.getFromPool(BUILDING_ID, standardBuildingPrefab.gameObject);
				inUsePreBuildObj.GetComponent<StandardBuilding>().setToBuildTargetDisplayConfiguration();
				inUsePreBuildObj.transform.position = calculateBuildingLocation(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
				inUsePreBuildObj.SetActive(true);
			}
		}
	}

	public override void cancelCast()
	{
		inUsePreBuildObj.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		inUsePreBuildObj.SetActive(false);
		inUsePreBuildObj = null;
		casting = false;
	}

	public override void release()
	{
		if (casting)
		{
			if (inUsePreBuildObj.transform.position.Equals(GameConstant.OUT_OF_CAMERA_POS))
			{
				Debug.Log("Building can't be built at the target location.");
			}
			else
			{
				casting = false;
				inUsePreBuildObj.GetComponent<Building>().spellRelease(Vector2.zero, null);
				inUsePreBuildObj = null;
				currentCooldownTime = cooldownTime;
				UnitsTracker.PlayerChampion.CurrentMana -= manaCost;
				//TODO AudioCenter.playOneShot(beginBuildClip, 1f);
			}
		}
	}


	private Vector3 calculateBuildingLocation(Vector2 worldSpacePos)
	{
		//TODO If tag player, then FIRST row, else FOURTH row
		Vector3 result = UnitsTracker.getStandardBuildLocation_FirstFourthRow(worldSpacePos, buildRow);
		if (result.Equals(GameConstant.OUT_OF_CAMERA_POS))
			return result;
		Building buildingScript = standardBuildingPrefab.GetComponent<Building>();
		return new Vector2(result.x + buildingScript.BuildOffsetX, result.y + buildingScript.BuildOffsetY);
	}


	//=================== FUTURE IMPLEMENTATION ========================
	public void onReceiveBuildingUnitsChange()
	{
		//TODO onReceiveBuildingUnitsChange() MachineGun
		//update possible build location
		//Think you will want to conver this to a interface or something. Research how C# or unity event works first 
	}
}

