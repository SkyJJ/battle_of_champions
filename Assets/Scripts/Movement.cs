﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
	[SerializeField] private float boundary_MinX, boundary_MaxX;

	private Champion champion;
	private Rigidbody2D rbChampion;
	private Animator animatorChampion;
	private float movementSpeed;
	private bool moveRight, moveLeft, directionChanged;
	private int mostRecentMoveDirection;
	const string strRunningDir = "runningDirection";

	void Start()
	{
		champion = this.GetComponent<Champion>();
		rbChampion = this.GetComponent<Rigidbody2D>();
		animatorChampion = this.GetComponent<Animator>();
		movementSpeed = this.GetComponent<Champion>().CurrentMovementSpeed / 10; //divide by 10 for the game movement speed.
		moveRight = false;
		moveLeft = false;
		directionChanged = false;
		mostRecentMoveDirection = (int)GameConstant.MoveDirection.REST;
	}

	public void move(GameConstant.MoveDirection moveDirection) {	
		switch (moveDirection)
		{
			case GameConstant.MoveDirection.RIGHT:
				moveRight = true;
				directionChanged = true;
				mostRecentMoveDirection = (int)GameConstant.MoveDirection.RIGHT;
				return; //careful this is return

			case GameConstant.MoveDirection.LEFT:
				moveLeft = true;
				directionChanged = true;
				mostRecentMoveDirection = (int)GameConstant.MoveDirection.LEFT;
				return;

			case GameConstant.MoveDirection.STOP_MOVE_RIGHT:
				moveRight = false;
				break; //careful this is break

			case GameConstant.MoveDirection.STOP_MOVE_LEFT:
				moveLeft = false;
				break; 
		}

		if (!(moveRight || moveLeft)) {
			directionChanged = true;
			mostRecentMoveDirection = (int)GameConstant.MoveDirection.REST;
		}			
	}

    void Update()
    {
		if (!champion.ActionIsBlocked && directionChanged) {
			animatorChampion.SetInteger(strRunningDir, (int)mostRecentMoveDirection);
			directionChanged = false;
		}
	}

	void FixedUpdate() {
		if (!champion.ActionIsBlocked && (moveLeft || moveRight))
		{
			rbChampion.MovePosition(new Vector2(
				Mathf.Clamp(rbChampion.position.x + movementSpeed * (int)mostRecentMoveDirection * Time.fixedDeltaTime, boundary_MinX, boundary_MaxX)
				, rbChampion.position.y)
				);
		}
	}
}

