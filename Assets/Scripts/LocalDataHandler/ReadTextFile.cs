﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadTextFile : MonoBehaviour
{
    public string filePath;

    private string[] lines;

    public string[] GetLines()
    {
        ReadLines();
        return lines;
    }

    private void ReadLines()
    {
        lines = System.IO.File.ReadAllLines(filePath);
    }
}
