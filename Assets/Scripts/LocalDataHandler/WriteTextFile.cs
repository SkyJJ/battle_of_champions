﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WriteTextFile : MonoBehaviour
{
    public string filePath;

    private string[] lines;

    public void SetLines(string[] lines)
    {
        this.lines = lines;
        WriteLines();
    }

    private void WriteLines()
    {
        System.IO.File.WriteAllLines(filePath,lines);
    }
}
