﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class NullSpellEffect : ActiveSpellEffect
{
	private static ActiveSpellEffect spellEffect;

	private void Awake()
	{
		if (spellEffect == null)
		{
			Debug.Log("NullSpellEffect awake");
			tag = GameConstant.NEUTRAL_TAG;
			spellEffect = this;
		}
		else
			Destroy(this);
	}

	public static ActiveSpellEffect Instance()
	{
		Assert.IsNotNull(spellEffect, "NullSpellEffect is null while being accessed.");
		return spellEffect;
	}

	public override void apply(Champion champion)
	{
		return;
	}

	protected override void applyFirstTimeEffect(Champion champion)
	{
		return;
	}

	protected override void applyMiddleEffect(Champion champion)
	{
		return;
	}

	protected override void applyEndEffect(Champion champion)
	{
		return;
	}
}
