﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ActiveSpellEffect : SpellEffect
{
	protected enum SpellEffectState { ApplyFirstTime, Applying, ApplyEnd };

	[SerializeField] protected float effectDuration;

	protected int id;
	protected bool isBuff;		
	protected float currentEffectDuration;

	protected SpellEffectState state;

	protected virtual void Start()
    {
		state = SpellEffectState.ApplyFirstTime;
    }

	/// <summary>
	/// This function will be called by the object possessing the effect each frame.
	/// </summary>
	public override void apply(Champion champion) {
		switch (state) {
			case SpellEffectState.ApplyFirstTime:
				//Apply first time effect
				applyFirstTimeEffect(champion);
				state = SpellEffectState.Applying;
				break;
			case SpellEffectState.Applying:
				//Apply effect. This is per frame. You can use a duration variable to control the effect interval.
				applyMiddleEffect(champion);
				break;
			case SpellEffectState.ApplyEnd:
				//Apply the last effect. It can be removing the effects from the champion, or deal a last burst damage etc.
				applyEndEffect(champion);
				champion.removeSpellEffect(this);
				resetSpellEffect();
				break;
		}

		updateSpellEffect();
	}

	public virtual void updateSpellEffect() {
		currentEffectDuration -= Time.deltaTime;
		if (currentEffectDuration <= 0)
		{
			state = SpellEffectState.ApplyEnd;
		}

	}

	/// <summary>
	/// To be called not only when SpellEffect ends, but also when Champion purge the debuff etc.
	/// </summary>
	public virtual void resetSpellEffect() {
		this.gameObject.SetActive(false);
		state = SpellEffectState.ApplyFirstTime;
		currentEffectDuration = effectDuration;
	}

	protected abstract void applyFirstTimeEffect(Champion champion);

	protected abstract void applyMiddleEffect(Champion champion);

	protected abstract void applyEndEffect(Champion champion);

	// =====================================================================
	// ======================== PROPERTIES =================================
	// =====================================================================
	public bool IsBuff { get { return isBuff; } set { isBuff = value; } }
	public int Id { get { return id; } set { id = value; } }
	public float NormalizedTiming { get { return currentEffectDuration / effectDuration; } }
}
