﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlayerData")]
public class PlayerData : ScriptableObject
{
	public string playerName; //{ get { return playerName; } set { playerName = value; } }
	public Sprite medal;
	public int mmr;
	public GameObject[] champions;
}
