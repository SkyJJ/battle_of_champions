﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChampionsHandler : ChampionsHandler
{
	private const string PERCENTAGE_FORMAT = "#\\%";

	[Header("Champion Stats UI")]
	[SerializeField] private Image championHpUI;
	[SerializeField] private Image championManaUI;
	[SerializeField] private Text physicalDefenseUI;
	[SerializeField] private Text magicalDefenseUI;
	[SerializeField] private Text movementSpeedUI;

	[Header("Champion Button UI")]
	[SerializeField] private Image championButtonIcon_left;
	[SerializeField] private Image championButtonHp_left;
	[SerializeField] private Image championButtonMana_left;
	[SerializeField] private Image championButtonIcon_right;
	[SerializeField] private Image championButtonHp_right;
	[SerializeField] private Image championButtonMana_right;

	[SerializeField] private EffectsUIHandler effectsUIHandler;


	void LateUpdate()
	{
		if (championSpawned) {
			updateChampionHpManaUI(currentChampionIndex);
			effectsUIHandler.updateBuffDebuffStatus(champions[currentChampionIndex].Buffs, champions[currentChampionIndex].Debuffs);
		}
			
	}


	protected override void updateChampionButton(GameConstant.ChampionButton_Int championButton, int championIndex)
	{
		base.updateChampionButton(championButton, championIndex);
		if (championButton == GameConstant.ChampionButton_Int.RIGHT)
		{
			championButtonIcon_right.sprite = champions[championIndex].Icon;
			championButtonHp_right.fillAmount = champions[championIndex].CurrentHp;
			championButtonMana_right.fillAmount = champions[championIndex].CurrentMana;
		}
		else
		{
			championButtonIcon_left.sprite = champions[championIndex].Icon;
			championButtonHp_left.fillAmount = champions[championIndex].CurrentHp;
			championButtonMana_left.fillAmount = champions[championIndex].CurrentMana;
		}
	}

	protected override void spawnChampion(int championIndex, float oldChampPosX)
	{
		base.spawnChampion(championIndex, oldChampPosX);
		UnitsTracker.PlayerChampion = champions[championIndex];
		fillChampionUIStatus(championIndex);
		effectsUIHandler.updatePassiveStatus(champions[championIndex].Passives);
	}


	private void fillChampionUIStatus(int championIndex)
	{
		championHpUI.fillAmount = champions[championIndex].NormalizedHp;
		//hp regen
		championManaUI.fillAmount = champions[championIndex].NormalizedMana;
		//mana regen
		physicalDefenseUI.text = champions[championIndex].CurrentPhysicalDefense.ToString(PERCENTAGE_FORMAT);
		magicalDefenseUI.text = champions[championIndex].CurrentMagicalDefense.ToString(PERCENTAGE_FORMAT);
		movementSpeedUI.text = champions[championIndex].CurrentMovementSpeed.ToString();
	}

	private void updateChampionHpManaUI(int championIndex)
	{
		championHpUI.fillAmount = champions[championIndex].NormalizedHp;
		championManaUI.fillAmount = champions[championIndex].NormalizedMana;
	}
}

