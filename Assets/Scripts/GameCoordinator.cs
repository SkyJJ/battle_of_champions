﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Assertions;

/**
 * NOTE
 * This class acts as the central piece to coordinate player data, player's input and UI. 
 * This class has been implemented rapidly allow rapid prototyping.
 * There will be changes in the future to adapt to networking. Eg: maybe there will be more classes, client-server code etc. 
 * 
 * */
public class GameCoordinator : MonoBehaviour
{
	/*
	Input				   Status
	      \               /
		   GameCoordinator
		         | 
			  Champion
	
	
	DATA FLOW
	Inputs send their commands to GameCoordinator
	GameCoordinator passes these commands to Champion -> Champion does certain action based on these commands.
	GameCoordinator updates Status every frame by sending Champion's data (currentHp etc.) and games' logic (currentCooldownTime of a spellcaster etc.) 
	
	REFERENCE LINK	 
	All Input will hold reference to GameCoordinator (Input send command to GameCoordinator). 
	GameCoordinator will hold a reference to Champion (GameCoordinator passes command to Champion).
	GameCoordinator will hold references to all of the Status that need to be updated (GameCoordinator sends data to Status). 

	ALTERNATIVE (09/07/2019)
	The above data flow requires input to pass from gamecoordinator to champion to spellcaster.
	Then to update spellcaster UI, gamecoordinator needs to ask champion to ask his spellcasters to pass the spellcasters data.

		  Input							 Input
		    |							   |
	SpellCastersHandler	 <-------  ChampionsHandler
			|							   |
	 SpellCasters UI				  Champion UI

	So SpellCastersHandler will have the interface to work with spell casters. Eg select(), cast() etc. Then it also has methods to update UI.
	ChampionsHandler will have the interface to work with champions. Eg swapChampion(), move(), Then it also has methods to update UI.

	Implications and Modularity?
	Now SpellCasters don't have to change anything. Neither do Input. Same with Champions.
	Now what about relation between spellcasters and champion? 
	Certain actions on spellcasters WILL have an effect on champion. How do you handle those? Eg when spellcaster is casting there will be 
	champion casting animation for that certain spell. Then when spellcasting champions cannot move etc. 
	-> SpellCaster job to tell the Champion in use what to do.
	How do spellcaster know who is in used? Or even, how do spell casters UI initialize themselves? 

	Organization
	SpellCastersHandler will store the 3 ongoing spellcasters. 
	ChampionsHandler will store 3 champions, and receive input and update according to champion in use.
	It means, it is ChampionsHandler job to tell spellCastersHandler when champion is spawned or swapped (to update the spellcasters).
	ChampionsHandler will hold a reference to SpellCastersHandler.

	*/

	[Header("General Information UI")]
	[SerializeField] private Text timer;
	[SerializeField] private TextMeshProUGUI startingTextPlayer1;
	[SerializeField] private TextMeshProUGUI startingTextPlayer2;
    [SerializeField] private GameObject gameStartingCover;
    private float startGameDelay; //starting cover duration
    [SerializeField] private GameObject powerUpCover;


	// ==== CHAMPION ====
	[Header("Game data")]
	[SerializeField] private PlayerData playerData;
	[SerializeField] private PlayerData enemyData;
	[SerializeField] private float gameStartDelay;
	[SerializeField] private int gameDuration;
	//[SerializeField] private int championSwapCooldownTime;

	[SerializeField] private ChampionsHandler playerChampionsHandler;
	[SerializeField] private ChampionsHandler enemyChampionsHandler;

	void Awake() {
		gameObject.AddComponent<NullCaster>();
		gameObject.AddComponent<NullSpellEffect>();
		StartCoroutine(GameStartingDelay());
		Invoke("spawnFirstChampion", gameStartDelay);
	}

    private IEnumerator GameStartingDelay()
    {
		gameStartingCover.SetActive(true);
        Animation anim = gameStartingCover.GetComponent<Animation>();
        //startGameDelay = anim.clip.averageDuration; <- works in editor but not in build
        startGameDelay = 8.0f;
        yield return new WaitForSeconds(startGameDelay);
        gameStartingCover.SetActive(false);
        StartCoroutine(StartGameTime());
        yield return null;
		
	}

	private IEnumerator StartGameTime()
	{
		Debug.Log("ActiveSelf 4 : " + this.gameObject.activeSelf);
		TimeSpan ts = new TimeSpan(0, 0, gameDuration);
		string timeFormat = @"mm\:ss";
		float championSpawnTime = gameDuration - gameStartDelay;
		timer.text = ts.ToString(timeFormat);

		//Give 0.5 sec for all objects instantiation, and then fill in UI 
		//yield return new WaitForSeconds(0.5f);
		//updateChampionUIStatus(currentChampionIndex);
		//things to be fixed: initialize champion and deactivate champion
		//updateChampionButton(GameConstant.Button_Int.LEFT, leftButtonChampionIndex);
		//updateChampionButton(GameConstant.Button_Int.RIGHT, rightButtonChampionIndex);
		//championsHandler.deactivateChampions();
		//yield return new WaitForSeconds(0.5f);

		//Game timer
		while (ts.TotalSeconds >= 0)
		{
			timer.text = ts.ToString(timeFormat);
			yield return new WaitForSeconds(1.0f);
			ts -= TimeSpan.FromSeconds(1);
            if (ts.TotalSeconds == (int)gameDuration/2)
            {
                Debug.Log("Power Up");
              //  StartCoroutine(powerUp());
            }
		}
	}

	private void spawnFirstChampion() {
		playerChampionsHandler.spawnChampionFirstTime(0, 4.5f);
		enemyChampionsHandler.spawnChampionFirstTime(0, 4.5f);
	}

   /* private IEnumerator powerUp()
    {
        powerUpCover.SetActive(true);
        yield return new WaitForSeconds(3.0f);
        powerUpCover.SetActive(false);
        yield return null;
    }*/
}
