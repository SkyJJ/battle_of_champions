﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpellCaster : MonoBehaviour {

	[SerializeField] protected Sprite icon;
	[SerializeField] protected string spellName;

	public Sprite Icon { get { return icon; } }
}
