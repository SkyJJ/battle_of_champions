﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveButton : MonoBehaviour, IPointerExitHandler, IPointerDownHandler //, IPointerEnterHandler
{
	//[SerializeField] private Champion champion;
	[SerializeField] private GameConstant.MoveDirection moveDirection;
	[SerializeField] private GameConstant.MoveDirection exitCommand;
	[SerializeField] private ChampionsHandler championsHandler;

	public void OnPointerDown(PointerEventData eventData)
	{
		//Debug.Log("eventData.pointerId : " + eventData.pointerId);
		if (eventData.pointerId == 0 || eventData.pointerId == 1)
		{
			//champion.move(moveDirection);
			championsHandler.moveChampion(moveDirection);
		}
	}

	/*public void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.pointerId >= 0)
		{
			//champion.move(moveDirection);
			championsHandler.moveChampion(moveDirection);
		}

	}*/

	public void OnPointerExit(PointerEventData eventData)
	{
		if (eventData.pointerId == 0 || eventData.pointerId == 1)
		{
			//champion.move(exitCommand);
			championsHandler.moveChampion(exitCommand);
		}

	}

}
