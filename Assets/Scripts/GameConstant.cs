﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstant  
{
	public const string PLAYER_TAG = "Player";
	public const string ENEMY_TAG = "Enemy";
	public const string NEUTRAL_TAG = "Neutral";
	public const string UNTAGGED_TAG = "Untagged";

	public enum MoveDirection { STOP_MOVE_LEFT = -2, LEFT = -1, REST = 0, RIGHT = 1 , STOP_MOVE_RIGHT = 2 }
	public enum ChampionButton_Int { LEFT = -1, RIGHT = 1}
	public enum SpellcasterButton_Int { NONE, FIRST, SECOND, THIRD }
	public enum LayerId
	{
		Castle = 1 << 8,
		SpellHeight3 = 1 << 9,
		BuildingHeight1 = 1 << 10,
		SpellHeight2 = 1 << 11,
        BuildingHeight2 = 1 << 12,
        SpellHeight2AffectChampion = 1 << 13,
		Champion = 1 << 14
	}

	public static Vector3 OUT_OF_CAMERA_POS = new Vector3(0, 0, -20);

	public const int MAX_SPELL_HIT_OBJECTS = 3;

	/// <summary>
	/// Boundary for determining if a spell is being activated or cancelled by user's drag.
	/// </summary>
	/// <remarks>
	/// GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT should be higher that the lower bound of background input. 
	/// Else we will receive release() and cancel() event
	/// </remarks>
	private const float REFERENCE_SPELL_ACTIVATION_CROSS_HEIGHT = 580;

	// ========================================================================================================================
	// ====================================== RESOLUTION / PIXEL PER UNIT CALCULATION =========================================
	// ========================================================================================================================

	// !!!!! RIGHT NOW WE HAVEN'T TAKEN INTO ACCOUNT PHONE WITH LARGER WIDTH THAN 16:9 !!!!!
	private const float REFERENCE_RESO_X = 1440;
	private const float REFERENCE_RESO_Y = 2560;

	public static float PIXEL_PER_UNIT = Screen.width / 9;
	public static float CONVERTED_RESO_Y = PIXEL_PER_UNIT * 16;
	public static float CONVERTED_RESO_X = PIXEL_PER_UNIT * 9;
	private static float SCALED_OFF_HEIGHT = (Screen.height - CONVERTED_RESO_Y) / 2;
	private static float UIPlatHeight = (REFERENCE_SPELL_ACTIVATION_CROSS_HEIGHT / REFERENCE_RESO_Y) * CONVERTED_RESO_Y;
	public static float SPELL_ACTIVATION_CROSS_HEIGHT = SCALED_OFF_HEIGHT + UIPlatHeight;

	public static void debug_showResoData() {
		Debug.Log("Screen.width : " + Screen.width);
		Debug.Log("Screen.height : " + Screen.height);
		Debug.Log("PIXEL_PER_UNIT : " + PIXEL_PER_UNIT);
		Debug.Log("CONVERTED_RESO_Y : " + CONVERTED_RESO_Y);
		Debug.Log("SCALED_OFF_HEIGHT : " + SCALED_OFF_HEIGHT);
		Debug.Log("UIPlatHeight : " + UIPlatHeight);
		Debug.Log("SPELL_ACTIVATION_CROSS_HEIGHT : " + SPELL_ACTIVATION_CROSS_HEIGHT);
	}

	public static float convertToWorldSpaceX(float inputPosX) {
		//Right now we assume phone width not larger than 16:9 
		return inputPosX / PIXEL_PER_UNIT;
	}

	public static float convertToWorldSpaceY(float inputPosY){
		return Mathf.Clamp((inputPosY - SCALED_OFF_HEIGHT), 0, CONVERTED_RESO_Y) / PIXEL_PER_UNIT;
	}

}
