﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpellCasterButton : MonoBehaviour, IPointerDownHandler, IDragHandler
{
	[SerializeField] private SpellCastersHandler spellCastersHandler;
	[SerializeField] private GameConstant.SpellcasterButton_Int buttonInt;
	[SerializeField] private AudioClip selectSound;
	private bool entered = false;
	private int pointerId;

	public void OnPointerDown(PointerEventData eventData)
	{
		//if (eventData.pointerId >= 0 && eventData.pointerId < Input.touchCount)
		/*if (eventData.pointerId == 0)
		{
			AudioCenter.playOneShot(selectSound, 0.4f);
			spellCastersHandler.selectSpellCaster(buttonInt);
		}*/
		AudioCenter.playOneShot(selectSound, 0.4f);
		spellCastersHandler.selectSpellCaster(buttonInt);
		pointerId = eventData.pointerId;
		/*Debug.Log("===============================================================================================");
		Debug.Log("Input touch count : " + Input.touchCount);
		foreach (Touch t in Input.touches) {
			Debug.Log("Touch position : " + t.position);
		}
		Debug.Log("SpellCasterButton OnPointerDown : " + pointerId);
		Debug.Log("SpellCasterButton OnPointerDown : " + eventData.ToString());
		Debug.Log("===============================================================================================");*/

	}

	public void OnDrag(PointerEventData eventData)
	{
		//Debug.Log("InputPos : " + eventData.position.x + " " + eventData.position.y);

		if (Input.touchCount == 1 && eventData.pointerId == pointerId)
		{
			/*Debug.Log("pointerId spellcasterbutton " + pointerId);
			Debug.Log("On drag touchCount " + Input.touchCount);*/
			if (eventData.position.y > GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT){
				entered = true;
				spellCastersHandler.castSpell(eventData.position);
			}
			else if (entered) {
				entered = false;
				spellCastersHandler.cancelSpell();
			}			
		}
	}
}
 