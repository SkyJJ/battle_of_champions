﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BattleGroundInput : MonoBehaviour, IPointerExitHandler, IDragHandler, IPointerDownHandler//, IPointerEnterHandler
{
	[SerializeField] private SpellCastersHandler spellCastersHandler;
	[SerializeField] private AudioClip selectBuildingSound;
	private bool entered = false;
	private int pointerId;
	/*public void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.pointerId == 0)
		{
			//Debug.Log("Canvas enter");
			//canvasEnter.Invoke();
			//sendTouchPos.Invoke(eventData.position.x, eventData.position.y);
			//canvasEnter.Invoke(eventData.position.x, eventData.position.y);
			spellCastersHandler.castSpell(eventData.position);
		}

	}*/

	public void OnPointerDown(PointerEventData eventData)
	{
		Debug.Log("touch count : " + Input.touchCount);
		if (Input.touchCount >= 3) {
			Debug.Log("=== BACKGROUND TOUCH COUNT BIGGER THAN 3 ===");
			foreach (Touch t in Input.touches)
			{
				Debug.Log("Touch position : " + t.position);
			}
			Debug.Log("=== BACKGROUND END ===");
		}
		//if (eventData.pointerId == 0 && eventData.position.y > GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT)
		if (Input.touchCount == 1 && eventData.position.y > GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT)
		{
			pointerId = eventData.pointerId;
			//Debug.Log("Screen space pos : " + eventData.position.x + ", " + eventData.position.y);

			Building b = UnitsTracker.getPlayerBuilding(eventData.position);
			if (b == null)
			{
				spellCastersHandler.castSpell(eventData.position);
			}
			else {
				AudioCenter.playOneShot(selectBuildingSound, 0.4f);
				b.userSelect();
			}

			entered = true;
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		//Debug.Log("InputPos : " + eventData.position.x + " " + eventData.position.y);

		//if (eventData.pointerId == 0)
		//if (Input.touchCount == 1)
		if (Input.touchCount == 1 && eventData.pointerId == pointerId)
		{
			//Debug.Log("Canvas enter x,y == " + eventData.position.x + ", " + eventData.position.y);
			if (eventData.position.y > GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT){
				entered = true;
				spellCastersHandler.castSpell(eventData.position);
			}
			else if (entered) {
				entered = false;
				spellCastersHandler.cancelSpell();
			}
		}
	}

	/*
	it gets released before it receives cancelled. 
	release isn't supposed to be called.
	that means

	*/


	public void OnPointerExit(PointerEventData eventData)
    {
		//if (eventData.pointerId == 0)
		//if (Input.touchCount == 1)
		if (Input.touchCount == 1 && eventData.pointerId == pointerId)
		{
            if (eventData.position.y > GameConstant.SPELL_ACTIVATION_CROSS_HEIGHT)
                spellCastersHandler.releaseSpell();
			//else
			//	spellCastersHandler.cancelSpell();
		}
	}


}
