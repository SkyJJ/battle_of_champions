﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
	void receiveDamage(float playerDamage, float buildingDamage);
	void receiveSpellEffects(ActiveSpellEffect[] spellEffects);
	void removeSpellEffect(ActiveSpellEffect spellEffect);
}
