﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpell 
{
	void spellRelease(Vector2 targetDestination, PrefabPool pool);
	void spellInit();
	void spellReset();
}
