﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PrefabPool 
{
	private string tag;
	private Dictionary<int, List<GameObject>> pool;

	//init Dict and List
	public PrefabPool(string tag) {
		//If this ever equals to true, it might be because of spell prefabs instantiation is faster than tag setting. You'll need a more deterministic way to set the tags
		Assert.IsFalse(tag.Equals(GameConstant.UNTAGGED_TAG));
		this.tag = tag;
		pool = new Dictionary<int, List<GameObject>>();
	}


	public void addToPool(int objectType_id, GameObject gameObject, int numOfObj) {
		for (int i = 0; i < numOfObj; i++) {
			addObject(objectType_id, gameObject);
		}
	}


	public GameObject getFromPool(int objectType_id, GameObject gameObject) {
		Assert.IsTrue(pool.ContainsKey(objectType_id));
		List<GameObject> l = pool[objectType_id];
		foreach(GameObject go in l) {
			if (go.activeSelf == false)
				return go; //available GO is found
		}
		//When no pool objects are available, we create a new one in the pool
		GameObject newGo = addObject(objectType_id, gameObject);
		return newGo;
	}


	private GameObject addObject(int objectType_id, GameObject gameObject){
		Assert.IsNotNull(gameObject);
		if (!pool.ContainsKey(objectType_id))
			pool[objectType_id] = new List<GameObject>();
		GameObject newGo = GameObject.Instantiate(gameObject);
		newGo.tag = this.tag;
		ISpell spellScript = newGo.GetComponent<ISpell>();
		if (spellScript != null) 
			spellScript.spellInit();
		pool[objectType_id].Add(newGo);
		return newGo;
	}
}
