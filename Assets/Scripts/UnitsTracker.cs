﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

/// <summary>
/// Main purpose of UnitsTracker
///	- Keeps track of all the units in the game
///	- Handle targeting(spells target and tower build target location)
///	
///	UnitsTracker and BuildingLocations are like friends.
/// UnitsTracker keeps track of the units using a grid system.
/// BuildingLocations knows all the standard location (the coordinate) on the map. 
/// 
/// When we ask UnitsTracker for target display, it looks into its grid, check if there's a unit there, and if there's one,
/// it asks BuildingsLocation to give the standard coordinate corresponding to that grid. 
/// </summary>
public class UnitsTracker : MonoBehaviour
{
	/// <summary>
	/// Target displays. Used to show spell target area. 
	/// </summary>
	public enum TargetDisplayType { standardAttack };
	public enum TargetMask { Enemy, EnemyBuildings, AllyBuildings, EnemyAndAllyBuildings};
	[Serializable] private struct TargetDisplayStruct {
		public TargetDisplayType targetDisplayType;
		public GameObject targetDisplayObject;
	}
	[SerializeField] private TargetDisplayStruct[] listTargetDisplays;
	private static TargetDisplayStruct[] var_ListTargetDisplays;

	/// <summary>
	/// Singleton. Prevent second instance of UnitsTracker to be created.
	/// </summary>
	private static bool unitsTrackerCreated = false;

	/// <summary>
	/// This 2d array stores the mask int and their corresponding affected rows.
	/// A mask int will be used to determine which rows of buildings can be targeted. All spells have 
	/// their specific mask (cf TargetMask above). 
	/// E.g If a spell can target friendly and enemy buildings, its mask int will be 3.
	/// Here's the mask table: 
	/// Ally	Foe		Mask int		Affected rows(indicated by 1)
	/// 0		0		0				0, 0, 0, 0, 0, 1
	/// 0		1		1				0, 0, 0, 1, 1, 1
	///	1		0		2				0, 1, 1, 0, 0, 0
	///	1		1		3				0, 1, 1, 1, 1, 1
	///	(Cf BuildingLocations.BuildingRow for all the rows)
	/// </summary>
	private static int[][] targetMaskList;

	/// <summary>
	/// Stores all the buildings on the battle field.
	/// buildingsOnMap will be of size [4][] with each row storing 4, 7, 7, 4 buildings maximum
	/// 0th row :  (no building, with the current game design)
	/// 1st row :	_	_	_	_	(has max 4 buildings)
	/// 2nd row :	_	_	_	_	_	_	_ (since 2nd and 3rd row don't have standard x building pos, 
	/// 3rd row :	_	_	_	_	_	_	_  we assume the max buildings to never exceed 7)
	/// 4th row :	_	_	_	_
	/// 5th row :  (no building, with the current game design)
	/// </summary>
	private static Building[][] buildingsOnMap;

	private static Dictionary<string, Champion[]> championsList;
	private static Champion enemyChampion;
	private static Champion playerChampion;

	// Start is called before the first frame update
	void Awake()
	{
		if (unitsTrackerCreated == false)
		{
			targetMaskList = new int[4][];
			targetMaskList[(int)TargetMask.Enemy] = new int[BuildingLocations.TOTAL_ROWS] { 0, 0, 0, 0, 0, 1 };
			targetMaskList[(int)TargetMask.EnemyBuildings] = new int[BuildingLocations.TOTAL_ROWS] { 0, 0, 0, 1, 1, 1 };
			targetMaskList[(int)TargetMask.AllyBuildings] = new int[BuildingLocations.TOTAL_ROWS] { 0, 1, 1, 0, 0, 0 };
			targetMaskList[(int)TargetMask.EnemyAndAllyBuildings] = new int[BuildingLocations.TOTAL_ROWS] { 0, 1, 1, 1, 1, 1 };

			buildingsOnMap = new Building[6][];
			buildingsOnMap[(int)BuildingLocations.BuildingRow.CASTLE] = new Building[0];
			buildingsOnMap[(int)BuildingLocations.BuildingRow.FIRST] = new Building[4]; //four standard building locations
			buildingsOnMap[(int)BuildingLocations.BuildingRow.SECOND] = new Building[7]; //seven max build locations
			buildingsOnMap[(int)BuildingLocations.BuildingRow.THIRD] = new Building[7];
			buildingsOnMap[(int)BuildingLocations.BuildingRow.FOURTH] = new Building[4];
			buildingsOnMap[(int)BuildingLocations.BuildingRow.ENEMY_CASTLE] = new Building[0];

			var_ListTargetDisplays = listTargetDisplays;
			championsList = new Dictionary<string, Champion[]>();

			unitsTrackerCreated = true;
		}
		else {
			Destroy(this);
		}
	}


	// ========================================================================================================================
	// ======================== PUBLIC INTERFACE FOR REGISTERING BUILDINGS INTO UNITS TRACKER =================================
	// ========================================================================================================================

	/// <summary>
	/// Register a building to the 2d array buildingsOnMap.
	/// </summary>
	/// <param name="building"></param>
	/// <returns></returns>
	public static int registerBuilding(GameObject building) {
		int col = BuildingLocations.determineColumn(building.transform.position.x);
		BuildingLocations.BuildingRow row = BuildingLocations.determineRow(building.transform.position.y);
		int id = -1;
		switch (row) {
			case BuildingLocations.BuildingRow.FIRST:
			case BuildingLocations.BuildingRow.FOURTH:
				//Debug.Log("registerBuilding() buildingsOnMap[(int)row][col] == " + buildingsOnMap[intRow][col]);
				Assert.IsNull(buildingsOnMap[(int)row][col]);
				buildingsOnMap[(int)row][col] = building.GetComponent<Building>();
				id = calculateBuildingOnMapId((int)row, col);
				break;
			case BuildingLocations.BuildingRow.SECOND:
			case BuildingLocations.BuildingRow.THIRD:
				
				id = addBuildingToMiddleRow(building, row);
				break;
			default:
				Assert.IsTrue(false, "Building's row is supposed to be within 1 and 4");
				break;
		}

		//TODO !!!! raise building list change event !!!!
		Assert.IsTrue(id != -1);
		return id;
	}

	public static void unregisterBuilding(int id) {
		int row = (id / 10);
		int col = id % 10;
		Assert.IsNotNull(buildingsOnMap[row][col]);
		buildingsOnMap[row][col] = null;
		//TODO !!!! raise building list change event !!!!
	}

	// ========================================================================================================================
	// ============================= SELECT BUILDING INPUT QUERY (Background OnPointerDown) ===================================
	// ========================================================================================================================

	/// <summary>
	/// Used to determine if there's a building on the given screen space input position.
	/// </summary>
	/// <param name="screenSpaceInputPos"></param>
	/// <returns>The building located on the given screen space input position. Else null. </returns>
	public static Building getBuilding(Vector2 screenSpaceInputPos) {
		Vector2 worldPos = new Vector2(GameConstant.convertToWorldSpaceX(screenSpaceInputPos.x), GameConstant.convertToWorldSpaceY(screenSpaceInputPos.y));
		BuildingLocations.BuildingRow row = BuildingLocations.determineRow(worldPos.y);
		Building b = null;
		switch (row)
		{
			case BuildingLocations.BuildingRow.FIRST:
			case BuildingLocations.BuildingRow.FOURTH:
				int column = BuildingLocations.determineColumn(worldPos.x);
				b = buildingsOnMap[(int)row][column];
				break;
			case BuildingLocations.BuildingRow.SECOND:
			case BuildingLocations.BuildingRow.THIRD:
				b = getMiddleRowBuilding(worldPos.x, row);
				break;
				//Other cases (Ally castle and enemy castle) are not treated and variable b will remain null
		}
		return b;
	}

	public static Building getPlayerBuilding(Vector2 screenSpaceInputPos)
	{
		Building b = getBuilding(screenSpaceInputPos);
		//Enemy building shouldnt be selectable by player
		if (b != null && b.tag.Equals(GameConstant.ENEMY_TAG)) {
			b = null;
		}
		return b;
	}

	// ========================================================================================================================
	// ================================= PUBLIC INTERFACE FOR GETTING TARGET DISPLAY LOCATION =================================
	// ========================================================================================================================

	/// <summary>
	/// Get target location from a given input. Use to determine the target location of a spell.
	/// </summary>
	/// <remarks>
	/// For each mask we have different rows for targeting. The algorithm calculates the row of inputPos, then use that row
	/// to determine from where it will start looking for targets.
	/// E.g If input pos is at row 4, and that the mask is 01 (target enemy buildings), then we'll look for target units from row 4 to 5.
	/// </remarks>
	/// <param name="inputPos"></param>
	/// <param name="buildingTargetMask"></param>
	/// <returns>Vector3 of the targetDisplay position. If target destination not available, it will return GameConstant.OUT_OF_CAMERA_POS</returns>
	public static Vector3 getStandardTargetLocation(Vector2 inputPos, TargetMask buildingTargetMask, string tag) {
		int[] targetRows = targetMaskList[(int)buildingTargetMask];
		BuildingLocations.BuildingRow inputRow = BuildingLocations.determineRow(inputPos.y);
		
		Vector3 vectTemp = GameConstant.OUT_OF_CAMERA_POS;

		//search from left to right (scan from lowest row to the highest row) for the targeted building
		for (int i = (int)inputRow; i < BuildingLocations.TOTAL_ROWS && vectTemp.Equals(GameConstant.OUT_OF_CAMERA_POS); i++)
			if ((targetRows[i] == 1))
			{
				vectTemp = (tag.Equals(GameConstant.PLAYER_TAG)) ?
							getBuildingOnMapPosition(inputPos.x, (BuildingLocations.BuildingRow)i) :
							getBuildingOnMapPosition(inputPos.x, (BuildingLocations.BuildingRow)(BuildingLocations.TOTAL_ROWS - 1 - i));
			}


		//search from right to left
		for (int i = (int)inputRow - 1; i >= 0 && vectTemp.Equals(GameConstant.OUT_OF_CAMERA_POS); i--)
			if ((targetRows[i] == 1))
			{
				vectTemp = (tag.Equals(GameConstant.PLAYER_TAG)) ?
							getBuildingOnMapPosition(inputPos.x, (BuildingLocations.BuildingRow)i) :
							getBuildingOnMapPosition(inputPos.x, (BuildingLocations.BuildingRow)(BuildingLocations.TOTAL_ROWS - 1 - i));
			}

		return vectTemp;
	}


	/// <summary>
	/// Get build location from a given input. Used to determine the build location of a building spell (row second or third).
	/// </summary>
	/// <param name="inputPos"></param>
	/// <param name="row"></param>
	/// <param name="halfBuildingWidth"></param>
	/// <returns></returns>
	public static Vector3 getStandardBuildLocation_MiddleRow(Vector2 inputPos, BuildingLocations.BuildingRow row, float halfBuildingWidth)
	{
		Assert.IsTrue(row == BuildingLocations.BuildingRow.SECOND || row == BuildingLocations.BuildingRow.THIRD);
		if (!occupiedByBuilding(inputPos.x - halfBuildingWidth, row) && !occupiedByBuilding(inputPos.x + halfBuildingWidth, row))
			return BuildingLocations.getStandardBuildingLocation(inputPos.x, row);
		return GameConstant.OUT_OF_CAMERA_POS;
	}

	/// <summary>
	/// Get build location from a given input. Used to determine the build location of a building spell (row first or fourth).
	/// </summary>
	/// <param name="inputPos"></param>
	/// <param name="row"></param>
	/// <returns></returns>
	public static Vector3 getStandardBuildLocation_FirstFourthRow(Vector2 inputPos, BuildingLocations.BuildingRow row)
	{
		Assert.IsTrue(row == BuildingLocations.BuildingRow.FIRST || row == BuildingLocations.BuildingRow.FOURTH);
		if (!occupiedByBuilding(inputPos.x, row)) {
			return BuildingLocations.getStandardBuildingLocation(inputPos.x, row);
		}
			
		return GameConstant.OUT_OF_CAMERA_POS;
	}


	// ====================================================================================================================
	// =========================================== PRIVATE HELPER METHODS =================================================
	// ====================================================================================================================


	/// <summary>
	/// Return the position of a building on map, determined by the given position x and the respective row.
	/// Algo: First it checks the row, then it asks: is there a building on it? 
	/// </summary>
	/// <param name="posX"></param>
	/// <param name="row"></param>
	/// <returns>Position of building if found. Else GameConstant.OUT_OF_CAMERA_POS</returns>
	private static Vector3 getBuildingOnMapPosition(float posX, BuildingLocations.BuildingRow row) {
		Vector3 vectTemp = GameConstant.OUT_OF_CAMERA_POS;
		switch (row)
		{
			case BuildingLocations.BuildingRow.CASTLE:
				vectTemp = BuildingLocations.getStandardBuildingLocation(posX, BuildingLocations.BuildingRow.CASTLE);
				break;
			case BuildingLocations.BuildingRow.ENEMY_CASTLE:
				vectTemp = BuildingLocations.getStandardBuildingLocation(posX, BuildingLocations.BuildingRow.ENEMY_CASTLE);
				break;
			case BuildingLocations.BuildingRow.FIRST:
			case BuildingLocations.BuildingRow.FOURTH:
				//int column = BuildingLocations.determineColumn(posX);
				//check if a building exists on the corresponding column and row 
				if (occupiedByBuilding(posX, row)) {
					vectTemp = BuildingLocations.getStandardBuildingLocation(posX, row);
				}
				break;
			default: //SECOND and THIRD
				//vectTemp = getMiddleRowBuildingPosition(posX, row);
				Building b = getMiddleRowBuilding(posX, row);
				vectTemp = (b == null)? GameConstant.OUT_OF_CAMERA_POS : b.transform.position;
				break;
		}
		return vectTemp;
	}

	private static bool occupiedByBuilding(float posX, BuildingLocations.BuildingRow row)
	{
		Assert.IsTrue(posX >= 0 && posX <= 9);
		switch (row) {
			case BuildingLocations.BuildingRow.FIRST:
			case BuildingLocations.BuildingRow.FOURTH:
				int column = BuildingLocations.determineColumn(posX);
				if (buildingsOnMap[(int)row][column] == null)
					return false;
				else
					return true;
			case BuildingLocations.BuildingRow.SECOND:
			case BuildingLocations.BuildingRow.THIRD:
				if (getMiddleRowBuilding(posX, row) == null)
					return false;
				else
					return true;
			default:
				Assert.IsTrue(false, "BuildingRow unsupported. Not expect to happen.");
				return false;
		}
	}

	/// <summary>
	/// Get the building in the second or third row by a given posX and row. If pos X is contained in the building with the respective
	/// row, the building is returned.
	/// </summary>
	/// <param name="posX">Position x of input</param>
	/// <param name="row">Second or third row</param>
	/// <returns>The Building containing posX if there's one. Else return null.</returns>
	private static Building getMiddleRowBuilding(float posX, BuildingLocations.BuildingRow row)
	{
		Assert.IsTrue(row == BuildingLocations.BuildingRow.SECOND || row == BuildingLocations.BuildingRow.THIRD);
		Vector2 vectInputStandardPos = BuildingLocations.getStandardBuildingLocation(posX, row);
		Building[] buildingList = buildingsOnMap[(int)row];
		int numBuildingContainingPosX = 0;
		Building buildingContainingPosX = null;
		foreach (Building b in buildingList)
		{
			if (b != null && b.containsHorizontally(vectInputStandardPos)) {
				numBuildingContainingPosX++;
				buildingContainingPosX = b;
			}
		}
		//By how buildings are built in the game (cf getStandardBuildLocation), there should never be an overlapping of Building in the game.
		Assert.IsTrue(numBuildingContainingPosX <= 1);  

		return buildingContainingPosX;
	}

	private static int addBuildingToMiddleRow(GameObject go, BuildingLocations.BuildingRow row) {
		Building[] buildingList = buildingsOnMap[(int)row];
		int id = -1;
		for (int i = 0; i < buildingList.Length; i++) {
			if (buildingList[i] == null) {
				buildingList[i] = go.GetComponent<Building>();
				id = calculateBuildingOnMapId((int)row, i);
				break;
			}
		}
		return id;
	}

	private static int calculateBuildingOnMapId(int i, int j) {
		return i * 10 + j;
	}

	public static GameObject getTargetDisplayObject(TargetDisplayType type, string tag) {

		foreach (TargetDisplayStruct td in var_ListTargetDisplays) {
			if (td.targetDisplayType.Equals(type)) {
				GameObject go = GameObject.Instantiate(td.targetDisplayObject);
				if (tag.Equals(GameConstant.ENEMY_TAG)) {
					SpriteRenderer s = go.GetComponent<SpriteRenderer>();
					Debug.Log("Enemy caster transparency turned off for debug.");
					//s.color = new Color(s.color.r, s.color.g, s.color.b, 0); //transparent		
				}
	
				return go;
			}
				
		}

		Assert.IsTrue(false, "TargetDisplayObject cannot be found. Type didn't manage to match.");
		return null;
	}

	// =======================================================================================
	// ================================= CHAMPION PROPERTIES =================================
	// =======================================================================================

	public static Champion PlayerChampion { get { return playerChampion; } set { playerChampion = value; } }

	public static Champion EnemyChampion { get { return enemyChampion; } set { enemyChampion = value; } }

	public static Champion getChampionByTag(string tag) {
		switch (tag) {
			case GameConstant.PLAYER_TAG:
				return playerChampion;
			case GameConstant.ENEMY_TAG:
				return enemyChampion;
			default:
				Assert.IsFalse(true, "Champion with tag : " + tag + " is unexpected.");
				return null;
		}
	}

	public static void minusChampionMana(float manaCost, string tag) {
		switch (tag){
			case GameConstant.PLAYER_TAG:
				playerChampion.CurrentMana -= manaCost;
				break;
			case GameConstant.ENEMY_TAG:
				enemyChampion.CurrentMana -= manaCost;
				break;
			default:
				Assert.IsFalse(true, "Champion with tag : " + tag + " is unexpected.");
				break;
		}
	}

	public static List<Building> getBuildingsOnMapByMask(TargetMask targetMask, string tag)
	{
		Assert.IsTrue(tag.Equals(GameConstant.PLAYER_TAG) || tag.Equals(GameConstant.ENEMY_TAG));
		List<Building> buildingsInMask = new List<Building>();
		int[] targetRows = targetMaskList[(int)targetMask];
		for (int i = (int)BuildingLocations.BuildingRow.FIRST; i <= (int)BuildingLocations.BuildingRow.FOURTH; i++)
			if ((targetRows[i] == 1))
			{
				Building[] bList = (tag.Equals(GameConstant.PLAYER_TAG)) ? buildingsOnMap[i]: buildingsOnMap[BuildingLocations.TOTAL_ROWS - 1 - i];
				foreach (Building b in bList)
				{
					if (b != null)
						buildingsInMask.Add(b);
				}
			}
		return buildingsInMask;
	}

	public static Building getBuildingOnMapById(int id) {
		return buildingsOnMap[id / 10][id % 10];
	}

	public static void addToChampionsList(string tag, Champion[] champions){
		Assert.IsFalse(championsList.ContainsKey(tag));
		championsList.Add(tag, champions);
	}

	public static Champion[] getAllyChampionsByTag(string tag) {
		Assert.IsTrue(championsList.ContainsKey(tag));
		return championsList[tag];
	}

}
