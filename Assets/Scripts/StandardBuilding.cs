﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StandardBuilding : Building
{
	// buildId, currentHp, currentBuildTime, selected, isBuilt, spellCasters;
	[SerializeField] protected GameObject canvas;

	[SerializeField] protected AudioClip buildingCompleteAudio;
	[SerializeField] protected float buildCompleteAudioVolume;

	protected bool isBuilding;
	protected float hpPerBuildTime;

	public override void spellInit()
	{
		base.spellInit();
		isBuilding = false;
		hpPerBuildTime = totalHp / buildTime;
	}

	public virtual void setToBuildTargetDisplayConfiguration()
	{
		setPreBuildDisplay();
		canvas.SetActive(false);
		selectOutline.color = Color.yellow;
		buildingCollider.enabled = false;
	}

	protected abstract void setPreBuildDisplay();

	public override void spellRelease(Vector2 targetDestination, PrefabPool pool)
	{
		canvas.SetActive(true);
		buildingCollider.enabled = true;
		isBuilding = true;
		selectOutline.color = Color.clear;
		registerToBuildingList();
	}

	protected virtual void Update()
	{
		if (isBuilding)
		{
			build();

			if (currentBuildTime <= 0)
			{
				buildComplete();
			}
		}
	}

	protected virtual void build() {
		currentBuildTime -= Time.deltaTime;
		currentHp = Mathf.MoveTowards(currentHp, totalHp, hpPerBuildTime * Time.deltaTime);
		updateUpUI();
		//TODO Show build time UI
	}

	protected virtual void buildComplete(){
		isBuilding = false;
		isBuilt = true;
		AudioCenter.playOneShot(buildingCompleteAudio, buildCompleteAudioVolume);
		reactivateSpellCasters();
		setBuiltDisplay();
	}

	protected abstract void setBuiltDisplay();





}

