﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/// <summary>
/// Main purpose of BuildingLocations
///	- Store all the standard locations (world coordinates) of buildings
///	
///	(Cf. UnitsTracker for more information how BuildingLocations class is being used)
/// </summary>
public class BuildingLocations : MonoBehaviour
{
	/// <summary>
	/// Singleton. Prevent second instance of BuildingLocations to be created.
	/// </summary>
	private static bool buildingLocationsCreated = false;

	/// <summary>
	/// All the classified rows in the game. 
	/// Player's territory : CASTLE, FIRST, SECOND
	/// Enemy's territory : THIRD, FOURTH, ENEMY_CASTLE
	/// </summary>
	public enum BuildingRow { CASTLE, FIRST, SECOND, THIRD, FOURTH, ENEMY_CASTLE };
	public const int TOTAL_ROWS = 6;
	
	/// <summary>
	/// All the standard building locations in the game.
	/// We will need this data when we want to build buildings on the battleground.
	/// </summary>
	[SerializeField] private Transform castleLocation;
	[SerializeField] private Transform[] firstRowBuildingLocations;
	[SerializeField] private Transform secondRowBuildingLocation;
	[SerializeField] private Transform thirdRowBuildingLocation;
	[SerializeField] private Transform[] fourthRowBuildingLocations;
	[SerializeField] private Transform castleEnemyLocation;

	/// <summary>
	/// The lines which we will be using to determine which row an input position y is on.
	/// </summary>
	[SerializeField] private Transform castleRowBoundary;
	[SerializeField] private Transform firstRowBoundary;
	[SerializeField] private Transform secondRowBoundary;
	[SerializeField] private Transform thirdRowBoundary;
	[SerializeField] private Transform fourthRowBoundary;
	[SerializeField] private Transform enemyCastleBoundary;

	/// <summary>
	/// The boundary of target spell on castle.
	/// </summary>
	[SerializeField] private float castleStandardTargetLocationMinX;
	[SerializeField] private float castleStandardTargetLocationMaxX;

	/// <summary>
	/// Store the references in static so that other classes can access their data by static
	/// getter methods without needing an instance of BuildingLocations.
	/// These data won't change during the game (they are just standard building locations data), so there
	/// won't be any risk doing this (as long as we don't provide setter method).
	/// </summary>
	private static Transform[][] buildingLocations;
	private static Transform[] rowBoundaries;
	private static float var_castleTargetMinX;
	private static float var_castleTargetMaxX;


	// Start is called before the first frame update
	void Awake(){
		if (buildingLocationsCreated == false) {
			//build locations
			buildingLocations = new Transform[TOTAL_ROWS][];
			buildingLocations[(int)BuildingRow.CASTLE] = new Transform[1] { castleLocation };
			buildingLocations[(int)BuildingRow.FIRST] = firstRowBuildingLocations;
			buildingLocations[(int)BuildingRow.SECOND] = new Transform[1] { secondRowBuildingLocation };
			buildingLocations[(int)BuildingRow.THIRD] = new Transform[1] { thirdRowBuildingLocation };
			buildingLocations[(int)BuildingRow.FOURTH] = fourthRowBuildingLocations;
			buildingLocations[(int)BuildingRow.ENEMY_CASTLE] = new Transform[1] { castleEnemyLocation };

			//row lines
			rowBoundaries = new Transform[TOTAL_ROWS];
			rowBoundaries[(int)BuildingRow.CASTLE] = castleRowBoundary;
			rowBoundaries[(int)BuildingRow.FIRST] = firstRowBoundary;
			rowBoundaries[(int)BuildingRow.SECOND] = secondRowBoundary;
			rowBoundaries[(int)BuildingRow.THIRD] = thirdRowBoundary;
			rowBoundaries[(int)BuildingRow.FOURTH] = fourthRowBoundary;
			rowBoundaries[(int)BuildingRow.ENEMY_CASTLE] = enemyCastleBoundary;

			var_castleTargetMinX = castleStandardTargetLocationMinX;
			var_castleTargetMaxX = castleStandardTargetLocationMaxX;

			buildingLocationsCreated = true;
		}
		else 
			Destroy(this);
	}

	/// <summary>
	/// Get standard build location based on the given row and an input position x.
	/// </summary>
	/// <param name="row">The row that the building will be built on.</param>
	/// <param name="posX">Input position x</param>
	/// <returns>The standard location</returns>
	public static Vector2 getStandardBuildingLocation(float posX, BuildingRow row)
	{
		Assert.IsTrue(posX >= 0 && posX <= 9);
		switch (row)
		{
			case BuildingRow.FIRST:
			case BuildingRow.FOURTH:
				return buildingLocations[(int)row][determineColumn(posX)].position;
			case BuildingRow.CASTLE:
			case BuildingRow.ENEMY_CASTLE:
				return new Vector2(clampTargetCastlePosX(posX), buildingLocations[(int)row][0].position.y);
			default:
				return new Vector2(clampMiddleRowBuildingPosX(posX), buildingLocations[(int)row][0].position.y);
		}
	}

	public static Vector2 getStandardBuildingLocation(int column, BuildingRow row) {
		Assert.IsTrue(column >= 0 && column <= 3);
		switch (row) {
			case BuildingRow.FIRST:
			case BuildingRow.FOURTH:
				return buildingLocations[(int)row][column].position;
			default:
				return new Vector2(buildingLocations[(int)BuildingLocations.BuildingRow.FIRST][column].position.x
					,buildingLocations[(int)row][0].position.y);
		}
	}

	public static int determineColumn(float x) {
		//Game world's width : 0 to 9
		if (x < 2.25) return 0;
		else if (x < 4.5) return 1;
		else if (x < 6.75) return 2;
		else return 3;
	}

	public static BuildingRow determineRow(float y)
	{
		if (y < rowBoundaries[(int)BuildingRow.CASTLE].position.y) return BuildingRow.CASTLE; 
		else if (y < rowBoundaries[(int)BuildingRow.FIRST].position.y) return BuildingRow.FIRST; 
		else if (y < rowBoundaries[(int)BuildingRow.SECOND].position.y) return BuildingRow.SECOND; 
		else if (y < rowBoundaries[(int)BuildingRow.THIRD].position.y) return BuildingRow.THIRD;
		else if (y < rowBoundaries[(int)BuildingRow.FOURTH].position.y) return BuildingRow.FOURTH; 
		else return BuildingRow.ENEMY_CASTLE;
	}

	private static float clampTargetCastlePosX(float x)
	{
		return Mathf.Clamp(x, var_castleTargetMinX, var_castleTargetMaxX);
	}

	private static float clampMiddleRowBuildingPosX(float x) {
		return Mathf.Clamp(x, 
			buildingLocations[(int)BuildingRow.FIRST][0].position.x,
			buildingLocations[(int)BuildingRow.FIRST][3].position.x);
	}
}
