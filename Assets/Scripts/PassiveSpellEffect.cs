﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class PassiveSpellEffect : SpellEffect
{
	public abstract void apply(Champion champion, int level);

}
