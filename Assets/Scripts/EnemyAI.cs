﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAI : MonoBehaviour
{
	[SerializeField] protected ChampionsHandler championsHandler;
	[SerializeField] protected SpellCastersHandler spellCastersHandler;

	// ===================================================================================
	// ================================= CHAMPION INPUT ==================================
	// ===================================================================================
	protected void sendMoveCommand(GameConstant.MoveDirection direction)
	{
		championsHandler.moveChampion(direction);
	}

	protected void sendSwapCommand(GameConstant.ChampionButton_Int championButtonInt)
	{
		championsHandler.swapChampion(championButtonInt);
	}

	// ========================================================================================
	// ================================= SPELL CASTERS INPUT ==================================
	// ========================================================================================
	protected void sendSelectCommand(GameConstant.SpellcasterButton_Int spellCasterInt)
	{
		spellCastersHandler.selectSpellCaster(spellCasterInt);
	}

	protected void sendCastCommand(Vector2 inputPos)
	{
		spellCastersHandler.castSpell(inputPos);
	}

	protected void sendCancelCommand()
	{
		spellCastersHandler.cancelSpell();
	}

	protected void sendReleaseCommand()
	{
		spellCastersHandler.releaseSpell();
	}
}
