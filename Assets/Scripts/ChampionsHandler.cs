﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChampionsHandler : MonoBehaviour
{
	[SerializeField] protected PlayerData playerData;

	protected SpellCastersHandler spellCastersHandler;
	protected BuildingsHandler buildingsHandler;

	protected Champion[] champions;
	protected int currentChampionIndex; // Index 0, 1 and 2 each represents a champion. 
	protected int leftButtonChampionIndex; // Store the index representing the champion this button is currently holding.
	protected int rightButtonChampionIndex;

	protected bool championSpawned = false;

	/// <summary>
	/// Using awake because ChampionsHandler is the start of the execution of the chain of initialization. 
	/// </summary>
	protected void Awake()
    {
		champions = new Champion[3];
		currentChampionIndex = 0;
		leftButtonChampionIndex = 1;
		rightButtonChampionIndex = 2;
		instantiateChampionPrefabs(playerData.champions);
		updateChampionButton(GameConstant.ChampionButton_Int.LEFT, 1);
		updateChampionButton(GameConstant.ChampionButton_Int.RIGHT, 2);
		spellCastersHandler = this.GetComponent<SpellCastersHandler>();
		buildingsHandler = this.GetComponent<BuildingsHandler>();
	}

	protected void Start() {
		UnitsTracker.addToChampionsList(tag, champions);
	}

	// ===================================================================================
	// ================================= INPUT INTERFACE =================================
	// ===================================================================================

	public void moveChampion(GameConstant.MoveDirection direction){
		//if (!champions[currentChampionIndex].ActionIsBlocked) {
			buildingsHandler.deselectCurrentBuilding();
			champions[currentChampionIndex].move(direction);
		//}
	}

	/// <summary>
	/// Interface for swapping champion.
	/// </summary>
	/// <param name="championButton">The champion button that was pressed. It will be either left or right.</param>
	public void swapChampion(GameConstant.ChampionButton_Int championButton)
	{
		if (!champions[currentChampionIndex].ActionIsBlocked)
		{
			int newChampionIndex = (championButton == GameConstant.ChampionButton_Int.RIGHT) ? rightButtonChampionIndex : leftButtonChampionIndex;
			int oldChampionIndex = currentChampionIndex;

			spawnChampion(newChampionIndex, champions[oldChampionIndex].transform.position.x);
			champions[oldChampionIndex].transform.position = GameConstant.OUT_OF_CAMERA_POS;
			updateChampionButton(championButton, oldChampionIndex);

			currentChampionIndex = newChampionIndex;
			buildingsHandler.deselectCurrentBuilding();
		}
	}

	// ==========================================================================================
	// ================================= END OF INPUT INTERFACE =================================
	// ==========================================================================================

	public void spawnChampionFirstTime(int championIndex, float oldChampPosX)
	{
		spawnChampion(championIndex, oldChampPosX);
		championSpawned = true;
	}


	protected virtual void updateChampionButton(GameConstant.ChampionButton_Int championButton, int championIndex)
	{
		if (championButton == GameConstant.ChampionButton_Int.RIGHT)
			rightButtonChampionIndex = championIndex;
		else
			leftButtonChampionIndex = championIndex;
	}


	protected virtual void spawnChampion(int championIndex, float oldChampPosX) {
		spellCastersHandler.fillNewChampionSpellCasters(champions[championIndex].SpellCasters);
		champions[championIndex].spawn(oldChampPosX);
	}



	protected virtual void instantiateChampionPrefabs(GameObject[] championsGO) {
		GameObject go;
		int length = playerData.champions.Length;
		for (int i = 0; i < length; i++) {
			go = GameObject.Instantiate(championsGO[i]);
			go.tag = this.tag;
			champions[i] = go.GetComponent<Champion>();
		}
	}

	public Champion CurrentChampion { get { return champions[(int)currentChampionIndex]; } }
}
