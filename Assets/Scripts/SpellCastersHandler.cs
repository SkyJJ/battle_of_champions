﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class SpellCastersHandler : MonoBehaviour
{
	/*
	Overview diagram
	BuildingsHandler	ChampionsHandler
			\				/
			SpellCastersHandler
					|
				SpellCaster

	*/

	protected const int MAX_NUM_USABLE_CASTERS = 3;

	// ==== Core data structures === 
	protected ActiveSpellCaster[] inUseSpellCasters;
	protected GameConstant.SpellcasterButton_Int currentSpellCaster;
	protected bool needUpdateSpellCastersTiming;

	// ==== INFORMATION STORING ==== 
	// to allow swapping of Champion and Building's spell casters efficiently
	protected bool championSpellCastersInUse;
	protected ActiveSpellCaster[] storedChampionSpellCasters;
	protected GameConstant.SpellcasterButton_Int storedSelectedChampionSpellCaster;

	protected virtual void Start(){
		inUseSpellCasters = new ActiveSpellCaster[4];
		inUseSpellCasters[0] = NullCaster.Instance();
		currentSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		needUpdateSpellCastersTiming = false;

		championSpellCastersInUse = true; //start of the game champion's spell casters will always be in used (instead of building's)
		storedChampionSpellCasters = new ActiveSpellCaster[4];
		storedChampionSpellCasters[0] = NullCaster.Instance();
		storedSelectedChampionSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		//storedSpellCasterOutlineColorUI = new Color[3];
		//storedSpellCasterIconSpriteUI = new Sprite[3];
	}

	// ===================================================================================
	// ================================= INPUT INTERFACE =================================
	// ===================================================================================

	public virtual void selectSpellCaster(GameConstant.SpellcasterButton_Int spellCasterInt)
	{
		NullCaster nc = inUseSpellCasters[(int)spellCasterInt] as NullCaster;
		//If it is not NullCaster
		if (nc == null)
		{
			//Update spellcaster logic
			currentSpellCaster = spellCasterInt;
			if (championSpellCastersInUse) storedSelectedChampionSpellCaster = currentSpellCaster;
			inUseSpellCasters[(int)currentSpellCaster].select();
		}
	}

	public void castSpell(Vector2 inputPos)
	{
		inUseSpellCasters[(int)currentSpellCaster].cast(inputPos);
	}

	public void cancelSpell()
	{
		inUseSpellCasters[(int)currentSpellCaster].cancelCast();
	}

	public void releaseSpell()
	{
		inUseSpellCasters[(int)currentSpellCaster].release();
	}


	// =======================================================================================================================
	// ================================= PUBLIC INTERFACE FOR UPDATING LIST OF SPELL CASTERS =================================
	// =======================================================================================================================

	public virtual void fillNewChampionSpellCasters(SpellCaster[] championSpellCasters)
	{
		//i  starts from 1 because index 0 is reserved for NullCaster
		for (int i = 0; i < MAX_NUM_USABLE_CASTERS; i++)
		{
			ActiveSpellCaster caster = championSpellCasters[i] as ActiveSpellCaster;
			if (caster != null)
			{
				//if caster is active
				inUseSpellCasters[i + 1] = caster;
				storedChampionSpellCasters[i + 1] = caster;
			}
			else
			{
				//if caster is passive
				inUseSpellCasters[i + 1] = NullCaster.Instance();
				storedChampionSpellCasters[i + 1] = NullCaster.Instance();
			}
		}

		//4 additional variables to update
		championSpellCastersInUse = true;
		currentSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		storedSelectedChampionSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		needUpdateSpellCastersTiming = true;
	}

	public virtual void fillBuildingSpellCasters(SpellCaster[] buildingSpellCasters)
	{
		//i  starts from 1 because index 0 is reserved for NullCaster
		for (int i = 0; i < buildingSpellCasters.Length; i++)
		{
			ActiveSpellCaster caster = buildingSpellCasters[i] as ActiveSpellCaster;
			if (caster != null)
				inUseSpellCasters[i + 1] = caster;
			else
				inUseSpellCasters[i + 1] = NullCaster.Instance();
		}

		//Fill the rest of the array to nullcaster if buildingSpellCasters.Length is not 3
		for (int i = buildingSpellCasters.Length; i < MAX_NUM_USABLE_CASTERS; i++)
			inUseSpellCasters[i + 1] = NullCaster.Instance();

		//4 additional variables to update
		if (championSpellCastersInUse)
		{
			storedSelectedChampionSpellCaster = currentSpellCaster;
			championSpellCastersInUse = false;
		}
		currentSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		needUpdateSpellCastersTiming = true;
	}

	public virtual void fillBuildingUndeployedSpellCasters(SpellCaster[] buildingSpellCasters)
	{
		//Nullify every user input
		for (int i = 0; i < MAX_NUM_USABLE_CASTERS; i++)
			inUseSpellCasters[i + 1] = NullCaster.Instance();

		//4 additional variables to update
		if (championSpellCastersInUse)
		{
			storedSelectedChampionSpellCaster = currentSpellCaster;
			championSpellCastersInUse = false;
		}
		currentSpellCaster = GameConstant.SpellcasterButton_Int.NONE;
		needUpdateSpellCastersTiming = false;
	}

	public virtual void resetToChampionSpellCasters()
	{
		if (!championSpellCastersInUse)
		{
			//Reset the array
			storedChampionSpellCasters.CopyTo(inUseSpellCasters, 0);

			//We don't use selectSpellCaster(championLastSpellCaster) here because we just reset the logic. We don't send the select() command
			currentSpellCaster = storedSelectedChampionSpellCaster;
			needUpdateSpellCastersTiming = true;
			championSpellCastersInUse = true;
		}
	}

	//public void showCurrentSpellCaster
}