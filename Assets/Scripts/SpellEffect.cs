﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpellEffect : MonoBehaviour
{
	//protected string spellEffectName;

	protected Sprite icon;
	public Sprite Icon { get { return icon; } set { icon = value; } }
	public abstract void apply(Champion champion);
	

}
