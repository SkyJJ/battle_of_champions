﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Champion : MonoBehaviour, IDamageable
{
	// ChampionStats : hp, mana, hpRegen, manaRegen, physicalDefense, magicalDefense, movementSpeed
	[System.Serializable] public struct ChampionStats
	{
		[SerializeField] private float hp;
		public float Hp { get { return hp; } set { hp = value; } }
		[SerializeField] private float mana;
		public float Mana { get { return mana; } set { mana = value; } }
		[SerializeField] private float hpRegen;
		public float HpRegen { get { return hpRegen; } set { hpRegen = value; } }
		[SerializeField] private float manaRegen;
		public float ManaRegen { get { return manaRegen; } set { manaRegen = value; } }
		[Range(0f, 100f)]
		[SerializeField]
		private float physicalDefense;
		public float PhysicalDefense
		{
			get { return physicalDefense; }
			set { physicalDefense = Mathf.Clamp(value, 0f, 100f); }
		}
		[Range(0f, 100f)]
		[SerializeField]
		private float magicalDefense;
		public float MagicalDefense
		{
			get { return magicalDefense; }
			set { magicalDefense = Mathf.Clamp(value, 0f, 100f); }
		}
		[Range(0f, 100f)]
		[SerializeField]
		private float movementSpeed;
		public float MovementSpeed
		{
			get { return movementSpeed; }
			set { movementSpeed = Mathf.Clamp(value, 0f, 100f); }
		}
	}

	// ==== Champion design data ==== 
	[SerializeField] private string championName;
	[SerializeField] private Sprite icon;
	[SerializeField] private float spawnPosY;
	[SerializeField] private ChampionStats championStatsLevel1;
	[SerializeField] private ChampionStats championStatsLevel2;
	[SerializeField] private ChampionStats championStatsLevel3;
	[SerializeField] private GameObject[] spellCasterPrefabs;

	// ==== Champion game data ==== 
	private ChampionStats currentStats;
	private float totalHp;
	private float totalMana;
	private int currentLevel;
	private Animator animator;
	private Movement movement;
	private SpellCaster[] spellCasters;
	private Dictionary<int, PassiveSpellEffect> passives;
	private List<ActiveSpellEffect> buffs;
	private List<ActiveSpellEffect> debuffs;
	private bool actionBlocked;

	void Awake()
	{
		currentStats.Hp = championStatsLevel1.Hp;
		totalHp = championStatsLevel1.Hp;
		currentStats.Mana = championStatsLevel1.Mana;
		totalMana = championStatsLevel1.Mana;
		currentStats.HpRegen = championStatsLevel1.HpRegen;
		currentStats.ManaRegen = championStatsLevel1.ManaRegen;
		currentStats.PhysicalDefense = championStatsLevel1.PhysicalDefense;
		currentStats.MagicalDefense = championStatsLevel1.MagicalDefense;
		currentStats.MovementSpeed = championStatsLevel1.MovementSpeed;
		currentLevel = 1;
		actionBlocked = false;
	}

	void Start()
	{
		animator = this.GetComponent<Animator>();
		movement = this.GetComponent<Movement>();
		passives = new Dictionary<int, PassiveSpellEffect>();
		initialiseBuffsAndDebuffs(2);
		instantiateSpellCasterPrefabs();

		//gameObject.SetActive(false);
	}

	private void initialiseBuffsAndDebuffs(int size) {
		buffs = new List<ActiveSpellEffect>(size);
		debuffs = new List<ActiveSpellEffect>(size);
		for (int i = 0; i < size; i++)
		{
			buffs.Add(NullSpellEffect.Instance());
			debuffs.Add(NullSpellEffect.Instance());
		}

		/*for (int i = 0; i < size; i++)
		{
			Debug.Log("buffs[i] " + buffs[i]);
			Debug.Log("debuffs[i] " + debuffs[i]);
		}*/
	}

	private void instantiateSpellCasterPrefabs()
	{
		int size = spellCasterPrefabs.Length;
		spellCasters = new SpellCaster[size];
		GameObject go;
		for (int i = 0; i < size; i++)
		{
			go = GameObject.Instantiate(spellCasterPrefabs[i]);
			go.tag = this.tag;
			spellCasters[i] = go.GetComponent<SpellCaster>();
		}

		//Set fourth spell caster and onwards to inactive. When champion reaches lvl 4 we will activate it again and swap with one of the 
		//three existing spells (in a normal scenario)
		for (int i = 3; i < size; i++)
		{
			spellCasters[i].gameObject.SetActive(false);
		}
	}

	void Update()
	{
		applyBuffs();
		applyDebuffs();
		updateHpMana();
		//Debug.Log("Update() Champion. Mana : " + currentStats.Mana);
		//currentStats.Hp += currentStats.HpRegen * Time.deltaTime;
		//currentStats.Mana += currentStats.ManaRegen * Time.deltaTime;
	}

	public void move(GameConstant.MoveDirection direction) {
		movement.move(direction);
	}

	public void spawn(float posX) {
		this.gameObject.transform.position = new Vector2(posX, this.spawnPosY);
		this.gameObject.SetActive(true);
		/*
		Futher implementation
		setActive
		set transform (on air probably)
		play spawn animation
		*/
	}


	private void updateHpMana() {
		currentStats.Hp = Mathf.MoveTowards(currentStats.Hp, totalHp, currentStats.HpRegen * Time.deltaTime);
		currentStats.Mana = Mathf.MoveTowards(currentStats.Mana, totalMana, currentStats.ManaRegen * Time.deltaTime);
	}

	public void receiveDamage(float playerDamage, float buildingDamage)
	{
		currentStats.Hp -= playerDamage;
        if (currentStats.Hp > totalHp) currentStats.Hp = totalHp;
	}

	public void receiveSpellEffects(ActiveSpellEffect[] spellEffects)
	{
		bool avaiIndexFound;
		List<ActiveSpellEffect> listTemp;

		foreach (ActiveSpellEffect effect in spellEffects)
		{
			avaiIndexFound = false;
			listTemp = (effect.IsBuff) ? buffs : debuffs;

			for (int i = 0; !avaiIndexFound && i < listTemp.Count; i++)
			{
				if (listTemp[i] is NullSpellEffect)
				{
					effect.Id = i;
					listTemp[i] = effect;
					avaiIndexFound = true;
				}
			}

			if (!avaiIndexFound)
				listTemp.Add(effect);
		}
	}

	public void removeSpellEffect(ActiveSpellEffect spellEffect)
	{
		List<ActiveSpellEffect> listTemp = (spellEffect.IsBuff) ? buffs : debuffs;
		listTemp[spellEffect.Id] = NullSpellEffect.Instance();
	}

	private void applyBuffs() {
		//Debug.Log("Buffs size : " + buffs.Count);
		for (int i = 0; i < buffs.Count; i++) {
			buffs[i].apply(this);

		}
		/*foreach (ActiveSpellEffect buff in buffs) {
			buff.apply(this);
		}*/
	}

	private void applyDebuffs() {
		for (int i = 0; i < buffs.Count; i++)
		{
			debuffs[i].apply(this);
		}

		/*foreach (ActiveSpellEffect debuff in debuffs){
			debuff.apply(this);
		}*/
	}

	/// <summary>
	/// Called by PassiveSpellCaster to send the passive to the champion.
	/// </summary>
	/// <param name="passiveInstanceID"></param>
	/// <param name="passive"></param>
	public void receivePassive(int passiveInstanceID, PassiveSpellEffect passive) {
		passive.apply(this); //IMPLEMENT APPLY FOR ARCANE BLESSING
		passives[passiveInstanceID] = passive;
	}

	/// <summary>
	/// Called by PassiveSpellCaster to level up the passive of the champion.
	/// </summary>
	/// <param name="passiveInstanceID"></param>
	/// <param name="passive"></param>
	public void levelUpPassive(int passiveInstanceID, PassiveSpellEffect passive, int level)
	{
		Assert.IsTrue(passives.ContainsKey(passiveInstanceID));
		passive.apply(this, level);
	}


	/*
	 * Later implement. Not now
	public void updateLevel(int level) {
		Assert.IsTrue(level >= 1 && level <= 4);
		switch (level) {
			case 1:
				currentLevel = 1;
				totalHp = championStatsLevel1.Hp;
				totalMana = championStatsLevel1.Mana;
				break;
			case 2:
				break;
			case 3:
				break;
			default:
				break;
		}
	}

	private void updateChampionStatus(ChampionStats stats) {
		totalHp = stats.Hp;
		totalMana = stats.Mana;
		//percentage of previous hp * totalHp
		currentStats.Hp = stats.Hp;
		currentStats.Mana = stats.Mana;
		currentStats.HpRegen = stats.HpRegen;
		currentStats.ManaRegen = stats.ManaRegen;
		currentStats.PhysicalDefense = stats.PhysicalDefense;
		currentStats.MagicalDefense = stats.MagicalDefense;
		currentStats.MovementSpeed = stats.MovementSpeed;
	}*/

	// ==== Properties ====
	public ChampionStats ChampionStatsLevel1 { get { return championStatsLevel1; } }

	public Sprite Icon { get { return icon; } }
	public string ChampionName { get { return championName; } }
	public float CurrentHp { get { return currentStats.Hp; } }
	public float CurrentMana { get { return currentStats.Mana; } set { currentStats.Mana = value; } }
	public float CurrentHpRegen { get { return currentStats.HpRegen; } }
	public float CurrentManaRegen { get { return currentStats.ManaRegen; } }
	public float CurrentPhysicalDefense { get { return currentStats.PhysicalDefense; } }
	public float CurrentMagicalDefense { get { return currentStats.MagicalDefense; } set { currentStats.MagicalDefense = value; } }
	public float CurrentMovementSpeed { get { return currentStats.MovementSpeed; } set { currentStats.MovementSpeed = value; } }
	public float NormalizedHp { get { return currentStats.Hp / totalHp; } }
	public float NormalizedMana { get { return currentStats.Mana / totalMana; } }
	public SpellCaster[] SpellCasters { get { return spellCasters; } }
	public GameObject[] SpellCasterPrefabs { get { return spellCasterPrefabs; } }
	public bool ActionIsBlocked { get { return actionBlocked; } set { actionBlocked = value; } }
	public Dictionary<int, PassiveSpellEffect> Passives { get { return passives; } }
	public ActiveSpellEffect[] Buffs { get { return buffs.ToArray(); } }
	public ActiveSpellEffect[] Debuffs { get { return debuffs.ToArray(); } }
}
