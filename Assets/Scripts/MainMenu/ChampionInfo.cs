﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChampionInfo : MonoBehaviour
{
    public GameObject championInfoPanel;
    public GameObject champion;
    public bool isSelected;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
        gameObject.name = champion.GetComponent<Champion>().ChampionName;
    }

    // Update is called once per frame
    public void TaskOnClick()
    {
        championInfoPanel.SetActive(true);
        Text[] playerStats = championInfoPanel.GetComponentsInChildren<Text>();
        Champion championComponent = champion.GetComponent<Champion>();
        playerStats[0].text = championComponent.ChampionName;
        playerStats[2].text = championComponent.ChampionStatsLevel1.Hp.ToString();
        playerStats[4].text = championComponent.ChampionStatsLevel1.Mana.ToString();
        playerStats[6].text = championComponent.ChampionStatsLevel1.HpRegen.ToString();
        playerStats[8].text = championComponent.ChampionStatsLevel1.ManaRegen.ToString();
        playerStats[10].text = championComponent.ChampionStatsLevel1.PhysicalDefense.ToString();
        playerStats[12].text = championComponent.ChampionStatsLevel1.MagicalDefense.ToString();
        playerStats[14].text = championComponent.ChampionStatsLevel1.MovementSpeed.ToString();
        Image[] championIcon = championInfoPanel.GetComponentsInChildren<Image>();
        championIcon[1].sprite = gameObject.GetComponent<Image>().sprite;
    }
}
