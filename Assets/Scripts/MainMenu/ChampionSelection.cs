﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChampionSelection : MonoBehaviour
{
    public GameObject championIconObjects;
    public GameObject championInfoPanel;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(SelectChampion);
    }

    public void SelectChampion()
    {
        Image[] championIcons = championIconObjects.GetComponentsInChildren<Image>();
        int length = championIcons.Length;
        for (int i = 0; i < length; i++)
        {
            if (championInfoPanel.GetComponentInChildren<Text>().text == championIcons[i].name)
            {
                Image championIcon = championIcons[i].GetComponent<Image>();
                if (championIcons[i].GetComponent<ChampionInfo>().isSelected)
                {
                    championIcon.color = new Color32(200,38,38,255);
                    championIcons[i].GetComponent<ChampionInfo>().isSelected = false;
                    break;
                }
                else
                {
                    championIcon.color = new Color32(255,255,255,255);
                    championIcons[i].GetComponent<ChampionInfo>().isSelected = true;
                    break;
                }
            }
        }
        championInfoPanel.SetActive(false);
    }
}
