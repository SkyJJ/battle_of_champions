﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetInfoOnMedal : MonoBehaviour
{
    public GameObject medalInfoPanel;
    public PlayerData playerData;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
        int requiredMMR = GetUnlockInfo(gameObject.name);
        if (requiredMMR > playerData.mmr)
        {
            Image image = gameObject.GetComponent<Image>();
            image.color = new Color32(15, 15, 15,255);
        }
    }

    // Update is called once per frame
    public void TaskOnClick()
    {
        Text[] medalInfo = medalInfoPanel.GetComponentsInChildren<Text>();
        medalInfo[0].text = GetMedalName(gameObject.name);
        medalInfo[1].text = "To unlock this medal, a MMR of " + GetUnlockInfo(gameObject.name).ToString() + " is required";
        medalInfoPanel.SetActive(true);
        GameObject medalIconObject = GameObject.FindGameObjectWithTag("Medal");
        Image medalIcon = medalIconObject.GetComponent<Image>();
        medalIcon.sprite = gameObject.GetComponent<Image>().sprite;
    }

    public string GetMedalName(string iconName)
    {
        switch (iconName)
        {
            case "Medal1":
                return "Soldier";
            case "Medal2":
                return "Warrior";
            case "Medal3":
                return "Guardian";
            case "Medal4":
                return "Master";
            case "Medal5":
                return "Legend";
            default:
                return "Medal Name";
        }
    }

    public int GetUnlockInfo(string iconName)
    {
        switch (iconName)
        {
            case "Medal1":
                return 0;
            case "Medal2":
                return 1000;
            case "Medal3":
                return 2000;
            case "Medal4":
                return 5000;
            case "Medal5":
                return 10000;
            default:
                return 0;
        }
    }
}
