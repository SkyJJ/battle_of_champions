﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAttributeWithInput : MonoBehaviour
{ 
    public Text attributeText;
    public InputField inputField;
    public int maxCharacterNumber;

    private ChangeWindowOnClick changeWindowOnClick;
    public GameObject currentWindow;
    public GameObject nextWindow;

    private bool isClicked;
    private string inputText;

    private void Start()
    {
        changeWindowOnClick = new ChangeWindowOnClick();
        changeWindowOnClick.currentWindow = currentWindow;
        changeWindowOnClick.nextWindow = nextWindow;
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
        isClicked = false;
    }

    /*public void OpenKeyboard()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
        Debug.Log("Keyboard should open now!");
    }*/
    private void TaskOnClick()
    {
        isClicked = !isClicked;
        if (isClicked)
        {
            inputField.gameObject.SetActive(true);
        }
        else
        {
            if (inputText.Length > 0)
            {
                if (inputText.Length > maxCharacterNumber)
                {
                    changeWindowOnClick.TaskOnClick();
                }
                else
                {
                    attributeText.text = inputText;
                }
            }
            inputField.text = "";
            inputField.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        inputText = inputField.text;
    }
}
