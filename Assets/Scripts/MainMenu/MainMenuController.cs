﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public PlayerData playerData;
    public Text playerStatsText;
    //Is destined to be different from (containing more champions) playerData.champions once "unlocking champions" funtionality is implemented
    //public GameObject[] champions;
    
    //private UploadAndSavePlayerData uploadAndSavePlayerData;

    private void Awake()
    {
        /*uploadAndSavePlayerData = GetComponent<UploadAndSavePlayerData>();
        playerData.name = uploadAndSavePlayerData.GetSinglePlayerData(UploadAndSavePlayerData.PlayerDataAttribute.PLAYER_NAME);
        playerData.mmr = System.Int32.Parse(uploadAndSavePlayerData.GetSinglePlayerData(UploadAndSavePlayerData.PlayerDataAttribute.MMR));
        string medalName = uploadAndSavePlayerData.GetSinglePlayerData(UploadAndSavePlayerData.PlayerDataAttribute.MEDAL);
        foreach (Sprite medal in medals)
        {
            if (medal.name == medalName)
            {
                playerData.medal = medal;
            }
        }*/
    }
    private void Start()
    {
        //GameObject.FindGameObjectWithTag("MainMenu").SetActive(true);
        string playerStatsToBeDisplayed = playerData.playerName + "   " + playerData.mmr + "   ";
        int championsNumber = playerData.champions.Length;
        for (int i = 0; i < championsNumber; i++)
        {
            playerStatsToBeDisplayed += playerData.champions[i].name + "   ";
        }
        playerStatsText.text = playerStatsToBeDisplayed;
    }
}
