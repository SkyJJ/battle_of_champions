﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeWindowOnClick : MonoBehaviour
{
    public GameObject currentWindow;
    public GameObject nextWindow;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }

    public void TaskOnClick()
    {
        if (currentWindow.tag == "MainMenu")
        {
            Button[] buttons = currentWindow.GetComponentsInChildren<Button>();
            foreach(Button button in buttons)
            {
                button.enabled = false;
            }
            nextWindow.SetActive(true);
        }
        else if (nextWindow.tag == "MainMenu")
        {
            RectTransform[] imagesTransform = currentWindow.GetComponentsInChildren<RectTransform>();
            foreach(RectTransform imageT in imagesTransform)
            {
                imageT.localScale = new Vector3(1, 1, 1);
            }
            currentWindow.SetActive(false);
            Button[] buttons = nextWindow.GetComponentsInChildren<Button>();
            foreach(Button button in buttons)
            {
                button.enabled = true;
            }
        }
        else
        {
            RectTransform[] imagesTransform = currentWindow.GetComponentsInChildren<RectTransform>();
            foreach (RectTransform imageT in imagesTransform)
            {
                imageT.localScale = new Vector3(1, 1, 1);
            }
            currentWindow.SetActive(false);
            nextWindow.SetActive(true);
        }
    }
}
