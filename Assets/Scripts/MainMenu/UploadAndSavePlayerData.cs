﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UploadAndSavePlayerData : MonoBehaviour
{
    ReadTextFile readTextFile;
    WriteTextFile writeTextFile;

    private void Start()
    {
        readTextFile = GetComponent<ReadTextFile>();
        writeTextFile = GetComponent<WriteTextFile>();
    }

    public enum PlayerDataAttribute { PLAYER_NAME, MEDAL, MMR, CHAMPIONS } 

    public void SetPlayerData(PlayerDataAttribute playerDataAttribute, string newValue)
    {
        switch (playerDataAttribute)
        {
            case PlayerDataAttribute.PLAYER_NAME:
                ChangeLine(newValue, 0);
                break;
            case PlayerDataAttribute.MMR:
                ChangeLine(newValue, 1);
                break;
            case PlayerDataAttribute.MEDAL:
                ChangeLine(newValue, 2);
                break;
            case PlayerDataAttribute.CHAMPIONS:
                AddLine(newValue);
                break;
        }
    }

    public void ChangeLine(string newString,uint index)
    {
        string[] lines = readTextFile.GetLines();
        if (index >= lines.Length)
        {
            AddLine(newString);
        }
        else
        {
            lines[index] = newString;
            writeTextFile.SetLines(lines);
        }
    }

    public void AddLine(string addedString)
    {
        string[] lines = readTextFile.GetLines();
        int length = lines.Length;
        string[] newLines = new string[length+1];   
        for (int i = 0; i < length; i++)
        {
            newLines[i] = lines[i];
        }
        newLines[length] = addedString;
        writeTextFile.SetLines(newLines);
    }

    public string GetSinglePlayerData(PlayerDataAttribute playerDataAttribute)
    {
        string[] lines = readTextFile.GetLines();
        switch (playerDataAttribute)
        {
            case PlayerDataAttribute.PLAYER_NAME:
                return lines[0];
            case PlayerDataAttribute.MMR:
                return lines[1];
            case PlayerDataAttribute.MEDAL:
                return lines[2];
        }
        return "";
    }

    public string[] GetMultiplePlayerData(PlayerDataAttribute playerDataAttribute)
    {
        string[] lines = readTextFile.GetLines();
        switch (playerDataAttribute)
        {
            case PlayerDataAttribute.CHAMPIONS:
                int length = lines.Length;
                string[] champions = new string[length - 3];
                for (int i = 3; i < length; i++)
                {
                    champions[i - 3] = lines[i];
                }
                return champions;
        }
        return new string[] { };
    }
}
