﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorizontalAutoScrollText : MonoBehaviour
{
    /*public TextMeshProUGUI textMeshProComponent;
    public string message;
    private float scrollSpeed=50;

    private float width;
    private float scrollPosition;
    private Vector3 startPosition;
    private RectTransform textTransform;

    private TextMeshProUGUI cloneTextMeshProComponent;
    private string offset = "\t";

    private void Awake()
    {
        textMeshProComponent.text = message;
    }

    private void Start()
    {
        textMeshProComponent.text = textMeshProComponent.text + offset;
        textTransform = textMeshProComponent.GetComponent<RectTransform>();
        width = textMeshProComponent.preferredWidth;
        textTransform.position = textTransform.position + Vector3.right * width/2;
        startPosition = textTransform.position;
        scrollPosition = 0;

        cloneTextMeshProComponent = Instantiate(textMeshProComponent) as TextMeshProUGUI;
        RectTransform cloneTextTransform = cloneTextMeshProComponent.GetComponent<RectTransform>();
        cloneTextTransform.SetParent(textTransform);
        cloneTextTransform.position = textTransform.position + Vector3.right * width;
        cloneTextTransform.sizeDelta = new Vector2(0, 0);
        cloneTextTransform.localScale = new Vector3(1,1,1);
    }

    private void Update()
    {
        Scroll the text across the screen moving the RectTransform
        textTransform.position = startPosition - (scrollPosition % width) * Vector3.right;
        scrollPosition += scrollSpeed * Time.deltaTime;
    }*/
    public Text textComponent;
    public float scrollSpeed = 20;

    private Text cloneTextComponent;

    private RectTransform textTransform;
    private RectTransform cloneTextTransform;
    private string currentText;
    private float width;

    private void Awake()
    {
        currentText = textComponent.text;
        textTransform = textComponent.GetComponent<RectTransform>();
        cloneTextComponent = Instantiate(textComponent) as Text;
        cloneTextTransform = cloneTextComponent.GetComponent<RectTransform>();
        cloneTextTransform.SetParent(textTransform);
        width = textComponent.preferredWidth;
        cloneTextTransform.position = textTransform.position + Vector3.right * width;
        cloneTextTransform.localScale = new Vector3(1,1,1);

    }

    IEnumerator Start()
    {
        width = textComponent.preferredWidth;
        textTransform.position = textTransform.position + Vector3.right * width / 2;
        Vector3 startPosition = textTransform.position;

        float scrollPosition = 0;

        while (true)
        {
            if (textComponent.text != currentText)
            {
                currentText = textComponent.text;
                width = textComponent.preferredWidth;
                textTransform.position = textTransform.position + Vector3.right * width / 2;
                cloneTextTransform.position = textTransform.position + Vector3.right * width;
                cloneTextComponent.text = textComponent.text;
            }
            textTransform.position = new Vector3(-scrollPosition % width, startPosition.y, startPosition.z);
            scrollPosition += scrollSpeed * Time.deltaTime;

            yield return null;
        }
    }
}
