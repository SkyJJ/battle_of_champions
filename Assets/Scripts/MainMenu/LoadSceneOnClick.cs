﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{
    public GameObject panel;
    public AudioSource mainMenuMusic;

    private int sceneIndex;
    [SerializeField] private int secondsDelay;

    private bool loading;
    private float currentLoadingTime;

    private void Start()
    {
        loading = false;
    }

    private void Update()
    {
        if (loading)
        {
            currentLoadingTime -= Time.deltaTime;
            if (currentLoadingTime < 0) currentLoadingTime = 0;
            mainMenuMusic.volume = currentLoadingTime/secondsDelay;
        }
    }

    public void LoadByIndex(int sceneIndex)
    {
        this.sceneIndex = sceneIndex;
        loading = true;
        currentLoadingTime = secondsDelay;
        StartCoroutine(Load());
    }

    public IEnumerator Load()
    {
        Button[] panelButtons = panel.GetComponentsInChildren<Button>();
        foreach (Button panelButton in panelButtons)
        {
            panelButton.enabled = false;
        }
        yield return new WaitForSeconds(secondsDelay);
        SceneManager.LoadScene(sceneIndex);
        yield return null;
    }
}
