﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCenter : MonoBehaviour
{
	[SerializeField] private float playTimeDelay;
	private static AudioSource audioSource;

	void Start()
	{
		audioSource = this.GetComponent<AudioSource>();
		Invoke("playBackgroundMusic", playTimeDelay);
	}

	private void playBackgroundMusic()
	{
		audioSource.Play();
	}

	public static void playOneShot(AudioClip audioClip, float volume) {
		if (volume > 1) volume = 1;
		else if (volume < 0) volume = 0;
		audioSource.PlayOneShot(audioClip, volume);
	}
}
