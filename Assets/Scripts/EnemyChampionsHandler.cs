﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyChampionsHandler : ChampionsHandler
{
	[SerializeField] private Image championHpUI;

	void LateUpdate()
	{
		if (championSpawned)
			updateHpUI(currentChampionIndex);
	}

	protected override void spawnChampion(int championIndex, float oldChampPosX)
	{
		base.spawnChampion(championIndex, oldChampPosX);
		UnitsTracker.EnemyChampion = champions[championIndex];
		updateHpUI(championIndex);
	}

	private void updateHpUI(int championIndex)
	{
		championHpUI.fillAmount = champions[championIndex].NormalizedHp;
	}
}
