﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaler : MonoBehaviour
{
	private Camera m_camera;
	[SerializeField] private float reso_height = 16f;
	[SerializeField] private float reso_width = 9f;
    
    public float Reso_width { get { return reso_width; } }

	void Start()
	{
		m_camera = GetComponent<Camera>();
		// (reso_width / reso_height) gives resolution ratio
		if (m_camera.aspect <= (reso_width / reso_height))
			//(reso_width / 2) gives orthographic width 
			m_camera.orthographicSize = (float)((reso_width / 2) / Screen.width * Screen.height);
	}
}
