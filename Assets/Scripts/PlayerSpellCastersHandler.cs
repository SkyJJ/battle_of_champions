﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class PlayerSpellCastersHandler : SpellCastersHandler
{

	private const string EMPTY_STR = "";
	private const string TIMING_TEXT_FORMAT = "0.0";
	private static Color LIT_SPELL_OUTLINE = new Color(1f, 1f, 1f, 1f);
	private static Color UNLIT_SPELL_OUTLINE = new Color(0.6f, 0.6f, 0.6f, 1f);
	private static Color TRANSPARENT = new Color(0f, 0f, 0f, 0f);

	// ==== SpellCasters UI ====
	[SerializeField] private Image[] spellCasterOutlineUI;
	[SerializeField] private Image[] spellCasterIconUI;
	[SerializeField] private Image[] spellCasterTimingCircleUI;
	[SerializeField] private Text[] spellCasterTimingTextUI;

	// ==== INFORMATION STORING ==== 
	private Color[] storedSpellCasterOutlineColorUI;
	private Sprite[] storedSpellCasterIconSpriteUI;

	protected override void Start()
	{
		base.Start();
		storedSpellCasterOutlineColorUI = new Color[3];
		storedSpellCasterIconSpriteUI = new Sprite[3];
	}

	void LateUpdate()
	{
		if (needUpdateSpellCastersTiming)
			updateSpellCastersTiming();
	}


	public override void selectSpellCaster(GameConstant.SpellcasterButton_Int spellCasterInt)
	{
		//UI part
		if (!(inUseSpellCasters[(int)spellCasterInt] is NullCaster))
		{
			//Update spellcaster outline UI
			if (currentSpellCaster == GameConstant.SpellcasterButton_Int.NONE) //0 means no spellcaster has been selected before. 
				spellCasterOutlineUI[convertToUIIndex(spellCasterInt)].color = LIT_SPELL_OUTLINE;
			else
			{
				spellCasterOutlineUI[convertToUIIndex(currentSpellCaster)].color = UNLIT_SPELL_OUTLINE;
				spellCasterOutlineUI[convertToUIIndex(spellCasterInt)].color = LIT_SPELL_OUTLINE;
			}
		}

		//Logic part
		base.selectSpellCaster(spellCasterInt);
	}

	public override void fillNewChampionSpellCasters(SpellCaster[] championSpellCasters)
	{
		//Logic part
		base.fillNewChampionSpellCasters(championSpellCasters);

		//Fill UI - the visual part
		fillSpellCastersUI(championSpellCasters);

		//backup the UI - for reswapping from building spell casters to champion casters easier
		for (int i = 0; i < MAX_NUM_USABLE_CASTERS; i++)
		{	
			storedSpellCasterIconSpriteUI[i] = spellCasterIconUI[i].sprite;
			storedSpellCasterOutlineColorUI[i] = spellCasterOutlineUI[i].color;
		}
	}

	public override void fillBuildingSpellCasters(SpellCaster[] buildingSpellCasters)
	{
		//Logic part
		base.fillBuildingSpellCasters(buildingSpellCasters);

		//Fill UI - the visual part
		fillSpellCastersUI(buildingSpellCasters);

	}

	public override void fillBuildingUndeployedSpellCasters(SpellCaster[] buildingSpellCasters)
	{
		//Logic part
		base.fillBuildingUndeployedSpellCasters(buildingSpellCasters);

		//Fill UI
		for (int i = 0; i < buildingSpellCasters.Length; i++)
		{
			//Fill only the icon and outline. Then color the whole thing with grey color by making cooldown circle fillAmount = 1f.
			spellCasterIconUI[i].sprite = buildingSpellCasters[i].Icon;
			spellCasterOutlineUI[i].color = (buildingSpellCasters[i] is PassiveSpellCaster) ? TRANSPARENT : UNLIT_SPELL_OUTLINE;
			spellCasterTimingTextUI[i].text = EMPTY_STR;
			spellCasterTimingCircleUI[i].fillAmount = 1f;
		}
		fillInvisibleCasters(buildingSpellCasters.Length);
	}

	public override void resetToChampionSpellCasters()
	{
		//UI part
		if (!championSpellCastersInUse)
		{
			//No need to reset the timing UI because update will run and replace the timing UI
			for (int i = 0; i < MAX_NUM_USABLE_CASTERS; i++)
			{
				spellCasterIconUI[i].sprite = storedSpellCasterIconSpriteUI[i];
				spellCasterIconUI[i].color = Color.white;
				spellCasterOutlineUI[i].color = storedSpellCasterOutlineColorUI[i];
			}

			if (storedSelectedChampionSpellCaster != GameConstant.SpellcasterButton_Int.NONE)
				spellCasterOutlineUI[convertToUIIndex(storedSelectedChampionSpellCaster)].color = LIT_SPELL_OUTLINE;
		}

		//Logic part
		base.resetToChampionSpellCasters();
	}

	// ====================================================================================================================
	// =========================================== PRIVATE HELPER METHODS =================================================
	// ====================================================================================================================

	private void fillSpellCastersUI(SpellCaster[] spellCasters)
	{
		//Initalized UI elements : icon, outline, timingCircle, timingUI
		for (int i = 0; i < spellCasters.Length && i < MAX_NUM_USABLE_CASTERS; i++)
		{
			spellCasterIconUI[i].sprite = spellCasters[i].Icon;
			spellCasterIconUI[i].color = Color.white;
			fillCasterOutlineAndTiming(spellCasters[i], i);
		}

		//If spellCasters given has less than 3 spells, make the rest of the spell caster UI invisible
		fillInvisibleCasters(spellCasters.Length);
	}

	private void fillCasterOutlineAndTiming(SpellCaster caster, int UIIndex)
	{
		if (caster is NullCaster || caster is PassiveSpellCaster)
		{
			spellCasterOutlineUI[UIIndex].color = TRANSPARENT;
			updateTimingCircleAndText(0, 0, spellCasterTimingCircleUI[UIIndex], spellCasterTimingTextUI[UIIndex]);
		}
		else
		{
			spellCasterOutlineUI[UIIndex].color = UNLIT_SPELL_OUTLINE;
			updateTimingCircleAndText(((ActiveSpellCaster)caster).CurrentTiming, ((ActiveSpellCaster)caster).CurrentNormalizedTiming, spellCasterTimingCircleUI[UIIndex], spellCasterTimingTextUI[UIIndex]);
		}
	}

	private void fillInvisibleCasters(int startIndex)
	{
		for (int i = startIndex; i < MAX_NUM_USABLE_CASTERS; i++)
		{
			spellCasterIconUI[i].color = TRANSPARENT;
			spellCasterOutlineUI[i].color = TRANSPARENT;
			updateTimingCircleAndText(0, 0, spellCasterTimingCircleUI[i], spellCasterTimingTextUI[i]);
		}
	}

	private void updateSpellCastersTiming()
	{
		for (int i = 0; i < inUseSpellCasters.Length && i < MAX_NUM_USABLE_CASTERS; i++)
		{
			updateTimingCircleAndText(inUseSpellCasters[i + 1].CurrentTiming,
				inUseSpellCasters[i + 1].CurrentNormalizedTiming,
				spellCasterTimingCircleUI[i],
				spellCasterTimingTextUI[i]);
		}
	}

	private void updateTimingCircleAndText(float timing, float normalizedTiming, Image timingCircle, Text timingText)
	{
		timingText.text = (timing <= 0) ? EMPTY_STR : timing.ToString(TIMING_TEXT_FORMAT);
		timingCircle.fillAmount = normalizedTiming;
	}

	/// <summary>
	/// Index for UI starts from 0, while index for spellCasters array start for 1 (since index 0 is NullCaster).
	/// </summary>
	/// <param name="spellCasterInt"></param>
	/// <returns></returns>
	private int convertToUIIndex(GameConstant.SpellcasterButton_Int spellCasterInt)
	{
		return (int)spellCasterInt - 1;
	}
}
