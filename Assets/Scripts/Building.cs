﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public abstract class Building : MonoBehaviour, IDamageable, ISpell
{
	//TODO building buffs & debuffs
	[SerializeField] protected Image hpUI;
	[SerializeField] protected SpriteRenderer selectOutline;

	[SerializeField] protected float totalHp;
	[SerializeField] protected float buildTime;
	[SerializeField] protected float buildOffsetX;
	[SerializeField] protected float buildOffsetY;
	[SerializeField] protected float buildingWidth; //Use to space out building (having some white space between building)
	[SerializeField] protected GameObject[] spellCasterPrefabs;

	private BuildingsHandler buildingsHandler;
	protected Collider2D buildingCollider;

	protected int buildId;
	protected float currentHp;
	protected float currentBuildTime;
	protected bool selected;
	protected bool isBuilt;
	protected SpellCaster[] spellCasters;

	public virtual void spellInit() {
		//TODO Init buildingsHandler with proper tag
		//buildingsHandler = GameObject.FindGameObjectWithTag("GameCoordinatorPlayer").GetComponent<BuildingsHandler>();
		buildingsHandler = searchBuildingsHandler(tag);
		buildingCollider = GetComponent<Collider2D>();
		hpUI.fillAmount = 0f;
		currentHp = 0;
		selectOutline.color = Color.clear;
		currentBuildTime = buildTime;
		selected = false;
		isBuilt = false;
		instantiateSpellCasterPrefabs();
	}

	private BuildingsHandler searchBuildingsHandler(string tag) {
		Assert.IsFalse(tag.Equals(GameConstant.UNTAGGED_TAG));
		BuildingsHandler bh = (tag.Equals(GameConstant.PLAYER_TAG)) ?
								GameObject.Find("PlayerHandlers").GetComponent<BuildingsHandler>():
								GameObject.Find("EnemyHandlers").GetComponent<BuildingsHandler>();
		return bh;
	}

	public virtual void spellReset()
	{
		hpUI.fillAmount = 0f;
		currentHp = 0;
		selectOutline.color = Color.clear;
		currentBuildTime = buildTime;
		selected = false;
		isBuilt = false;
		deactivateSpellCasters();
	}

	protected void registerToBuildingList() {
		buildId = UnitsTracker.registerBuilding(this.gameObject);
	}

	protected void unregisterFromBuildingList() {
		UnitsTracker.unregisterBuilding(buildId);
	}

	public bool containsHorizontally(Vector2 point) {
		//return this.GetComponent<Collider2D>().bounds.Contains(point);
		return (point.x <= (this.transform.position.x + buildingWidth / 2) &&
			point.x >= (this.transform.position.x - buildingWidth / 2));
	}

	public void receiveDamage(float playerDamage, float buildingDamage)
	{
		currentHp -= buildingDamage;
        if (currentHp > totalHp) currentHp = totalHp;
		updateUpUI();
		checkDestroy();
	}

	protected void updateUpUI() {
		hpUI.fillAmount = currentHp / totalHp;
	}

	protected void checkDestroy() {
		if (currentHp <= 0) {
			this.gameObject.SetActive(false);
			unregisterFromBuildingList();
			buildingsHandler.deselectBuilding(this);
			spellReset();
		}
	}

	public void userSelect() {
		buildingsHandler.selectBuilding(this);
	}

	public void select() {
		selected = true;
		selectOutline.color = Color.yellow;
	}

	public void deselect() {
		selected = false;
		selectOutline.color = Color.clear;
	}

	protected void reactivateSpellCasters() {
		for (int i = 0; i < spellCasters.Length; i++)
		{
			spellCasters[i].gameObject.SetActive(true);
		}
		if (selected) {
			buildingsHandler.fillSpellCastersHandler(this);
		}
	}

	protected void deactivateSpellCasters()
	{
		for (int i = 0; i < spellCasters.Length; i++)
		{
			spellCasters[i].gameObject.SetActive(false);
		}
	}

	protected void instantiateSpellCasterPrefabs()
	{
		int size = spellCasterPrefabs.Length;
		spellCasters = new SpellCaster[size];
		GameObject go; ;
		for (int i = 0; i < size; i++)
		{
			//Instantiate caster and child it to the building
			go = GameObject.Instantiate(spellCasterPrefabs[i], this.gameObject.transform);
			go.tag = this.tag;
			spellCasters[i] = go.GetComponent<SpellCaster>();
			spellCasters[i].gameObject.SetActive(false);
		}
	}

	public abstract void spellRelease(Vector2 targetDestination, PrefabPool pool);

	public void receiveSpellEffects(ActiveSpellEffect[] spellEffects)
	{
		Debug.Log("SpellEffect not yet implemented in Building.");
		return;
	}

	public void removeSpellEffect(ActiveSpellEffect spellEffect)
	{
		Debug.Log("SpellEffect not yet implemented in Building.");
		return;
	}

	// ============= PROPERTIES ==============

	public int BuildId { get { return buildId; } }

	public SpellCaster[] SpellCasters { get { return spellCasters; } }

	public bool IsBuilt { get { return isBuilt; } }

	public float BuildOffsetX { get { return buildOffsetX; } }

	public float BuildOffsetY { get { return buildOffsetY; } }

	public float BuildTime { get { return buildTime; } }

}
