﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PassiveSpellCaster : SpellCaster
{
	[SerializeField] private PassiveSpellEffect passiveSpellEffectPrefab;

	protected PassiveSpellEffect passiveSpellEffect;

	protected virtual void Start() {
		passiveSpellEffect = GameObject.Instantiate(passiveSpellEffectPrefab);
		passiveSpellEffect.Icon = this.icon;
	}

	/*protected virtual void sendPassiveToChampion(Champion champion) {
		champion.receivePassive(passiveSpellEffect.GetInstanceID(), passiveSpellEffect);
	}*/

}
