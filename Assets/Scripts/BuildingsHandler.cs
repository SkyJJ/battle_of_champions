﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingsHandler : MonoBehaviour
{
	private SpellCastersHandler spellCastersHandler;
	private Building currentBuilding;


    // Start is called before the first frame update
    void Start()
    {
		spellCastersHandler = this.GetComponent<SpellCastersHandler>();
	}


	/// <summary>
	/// Ask BuildingsHandler to handle the selection of  a a new building 
	/// </summary>
	/// <remarks>
	/// Three cases
	/// i) no currentBuilding and select only the new one
	/// ii) there's a currentBuilding and select a new one
	/// iii) there's a currentBuilding and reselect currentBuilding
	/// </remarks>
	/// <param name="newBuilding"></param>
	public void selectBuilding(Building newBuilding) {
		if (currentBuilding == null)
		{
			currentBuilding = newBuilding;
			currentBuilding.select();
			fillSpellCastersHandler(currentBuilding);
		}
		else if (newBuilding.BuildId != currentBuilding.BuildId)
		{
			currentBuilding.deselect(); 
			currentBuilding = newBuilding;
			currentBuilding.select();
			fillSpellCastersHandler(currentBuilding);
		}
		else {
			currentBuilding.deselect();
			currentBuilding = null;
			spellCastersHandler.resetToChampionSpellCasters();
		}
	}


	public void deselectCurrentBuilding()
	{
		deselectBuilding(currentBuilding);
	}

	/// <summary>
	/// Use this function to deselectBuilding if we're not sure what is the currentBuilding. 
	/// </summary>
	/// <remarks>
	/// Typically this happens when other classes need to deselect a building, but not sure if the building in question is the 
	/// currentBuilding.
	/// </remarks>
	/// <param name="building"></param>
	public void deselectBuilding(Building building)
	{
		if (currentBuilding != null && building.BuildId == currentBuilding.BuildId) {
			currentBuilding.deselect();
			currentBuilding = null;
			spellCastersHandler.resetToChampionSpellCasters();
		}
	}


	public void fillSpellCastersHandler(Building building) {
		if (building.IsBuilt)
			spellCastersHandler.fillBuildingSpellCasters(building.SpellCasters);
		else
			spellCastersHandler.fillBuildingUndeployedSpellCasters(building.SpellCasters);
	}
}
