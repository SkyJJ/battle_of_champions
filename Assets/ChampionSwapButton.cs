﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChampionSwapButton : MonoBehaviour
{
	[SerializeField] private GameConstant.ChampionButton_Int buttonInt;
	[SerializeField] private ChampionsHandler championsHandler;

	public void swapChampion() {
		championsHandler.swapChampion(buttonInt);
	}


}
