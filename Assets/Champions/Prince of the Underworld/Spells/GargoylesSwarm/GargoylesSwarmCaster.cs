﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargoylesSwarmCaster : ActiveSpellCaster
{
    [SerializeField] private GameObject PrecastPrefab;
    [SerializeField] private GameObject SpawnPrefab;
    [SerializeField] private GameObject GargoylePrefab;
    private const int PRECAST_ID = 0;
    private const int SPAWN_ID = 1;
    private const int GARGOYLE_ID = 2;

    [SerializeField] private GargoylesSwarm gargoylesSwarmScript;   

    private bool precasting;
    private GameObject inUsePrecastObj;

    [SerializeField] private float yFirstDestination;
    private CameraScaler cameraScaler;
    private float resoWidth;

    [SerializeField] private AudioClip gargoyleAudio;
    private AudioSource audioSourceForGargoyle;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        precasting = false;
        fillPrefabPool();
        cameraScaler = GameObject.FindGameObjectWithTag("MainCamera").gameObject.GetComponent<CameraScaler>();
        resoWidth = cameraScaler.Reso_width;
        audioSourceForGargoyle = GetComponent<AudioSource>();
        audioSourceForGargoyle.volume = 0.5f;
        Debug.Log("Hello gargoyleSwarm Start");
    }

    // Update is called once per frame
    void Update()
    {
        if (precasting)
        {
            currentPrecastTime -= Time.deltaTime;
            if (currentPrecastTime <= 0)
                release();
        }
        else if (currentCooldownTime > 0)
        {
            currentCooldownTime -= Time.deltaTime;
        }
    }

    public override void select()
    {
        //Do nothing
    }

    public override void cast(Vector2 inputPos)
    {
        if (currentCooldownTime <= 0 && !precasting)
        {
            // ==== LOGIC ====
            precasting = true;
            currentPrecastTime = precastTime;
            //Precast effects
            //inUsePrecastObj = prefabPool.getFromPool(PRECAST_ID, PrecastPrefab);
            //Vector2 championPos = UnitsTracker.getChampionByTag(tag).transform.position;
            //inUsePrecastObj.transform.position = championPos;
            //inUsePrecastObj.SetActive(true);
            // ==== AUDIO ====
            audioSourceForGargoyle.clip = gargoyleAudio;
            audioSourceForGargoyle.Play();
        }
    }

    public override void cancelCast()
    {
        if (precasting)
        {
            audioSourceForGargoyle.Stop();
            audioSourceForGargoyle.clip = null;
            resetPreCasting();
        }
    }

    public override void release()
    {
        if (currentPrecastTime <= 0)
        {
            // ==== LOGIC ====
            resetPreCasting();
            currentCooldownTime = cooldownTime;
            UnitsTracker.getChampionByTag(tag).CurrentMana -= manaCost;
            Vector2 championPos = UnitsTracker.getChampionByTag(tag).transform.position;
            // activate spawn prefab
            GameObject gargoyleSpawn = prefabPool.getFromPool(SPAWN_ID, SpawnPrefab);
            gargoyleSpawn.transform.position = championPos;
            gargoyleSpawn.transform.SetParent(UnitsTracker.getChampionByTag(tag).transform);
            gargoyleSpawn.SetActive(true);
            // send gargoyles
            GameObject gargoyle;
            float xFirstDestination;
            int offset = gargoyleColumnOffset();
            for (int i = 0; i < 3; i++)
            {
                gargoyle = prefabPool.getFromPool(GARGOYLE_ID,GargoylePrefab);
                gargoyle.transform.position = championPos;
                gargoyle.SetActive(true);
                xFirstDestination = Random.Range((float)(i+offset) ,(float)(i+1+offset)) * resoWidth/5;
                xFirstDestination = Mathf.Clamp(xFirstDestination, resoWidth / 18, resoWidth * 17 / 18);
                Vector2 targetDestination = new Vector2(xFirstDestination, yFirstDestination);
                gargoyle.GetComponent<GargoylesSwarm>().spellRelease(targetDestination, prefabPool);
            }
        }
    }

    //gargoyle first destination amongst 5 columns. This function determines whether to use 3
    //starting from left, 3 in the middle or 3 starting from right
    private int gargoyleColumnOffset()
    {
        string enemyTag = (CompareTag(GameConstant.PLAYER_TAG)) ? GameConstant.ENEMY_TAG : GameConstant.PLAYER_TAG;
        float enemyPosx = UnitsTracker.getChampionByTag(enemyTag).transform.position.x;
        float champPosx = UnitsTracker.getChampionByTag(tag).transform.position.x;
        //3 middle columns
        if ((enemyPosx >= resoWidth / 3 && enemyPosx <= resoWidth * 2/3) || Mathf.Abs(enemyPosx - champPosx) >= resoWidth /3)
            return 1;
        //3 columns starting from left
        else if (enemyPosx <= resoWidth / 3)
            return 0;
        //3 columns starting from right
        else
            return 2;
    }

    private void resetPreCasting()
    {
        precasting = false;
        currentPrecastTime = precastTime;
        //Reset inUsePrecastObj
        //inUsePrecastObj.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        //inUsePrecastObj.SetActive(false);
        //inUsePrecastObj = null;
        //Reset precast audio
    }

    private void fillPrefabPool()
    {
        prefabPool.addToPool(PRECAST_ID, PrecastPrefab, 1);
        prefabPool.addToPool(SPAWN_ID, SpawnPrefab, 1);
        prefabPool.addToPool(GARGOYLE_ID, GargoylePrefab, 3);
        gargoylesSwarmScript.fillPrefabPool(prefabPool);
    }
}
