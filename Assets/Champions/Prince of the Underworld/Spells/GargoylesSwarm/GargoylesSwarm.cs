﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GargoylesSwarm : MonoBehaviour, ISpell
{
    [SerializeField] private GameObject HitPrefab;
    [SerializeField] private GameObject DisappearPrefab;
    [SerializeField] private GameObject RegeneratePrefab;
    private const int HIT_ID = 3;
    private const int DISAPPEAR_ID = 4;
    private const int REGENERATE_ID = 5;

    [SerializeField] private float gargoyleSpeed;
    [SerializeField] private float championDamage;
    private const float buildingDamage = 0;
    [SerializeField] private float hitRangeRadius;

    private bool move;
    private bool crossedFirstDestination;
    private bool hitOccurred;
    private Vector2 targetDestination;
    private PrefabPool gargoylePool;
    private Collider2D[] affectedObjects;
    private GameObject affectedChampion;
    private Rigidbody2D rb;
    private bool enemyDarkOrbAffected;
    [SerializeField] private GameConstant.LayerId[] hitAffectedLayers;
    private string enemyTag;
    private Vector2 firstDestination;

    [SerializeField] private float curveController1; //before first destination
    [SerializeField] private float curveController2; //after first destination
    [SerializeField] private AudioClip hitAudio;

    public void spellInit()
    {
        affectedObjects = new Collider2D[3];
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (move)
        {
            if (!crossedFirstDestination && Mathf.Abs(rb.position.y - targetDestination.y) <= 0.1)
            {
                crossedFirstDestination = true;
            }
            else if (crossedFirstDestination)
            {
                if (!hitOccurred)
                {
                    targetDestination = UnitsTracker.getChampionByTag(enemyTag).transform.position;
                    if (Mathf.Abs(rb.position.y - targetDestination.y) <= 0.1)
                    {
                        enemyDarkOrbAffected = false; //TO BE CHANGED BY BUFF
                        ActivateHit();
                     }
                }
                else
                {
                    targetDestination = UnitsTracker.getChampionByTag(tag).transform.position;
                    if (Mathf.Abs(rb.position.y - targetDestination.y) <= 0.1)
                    {
                        ActivateRegeneration();
                        ActivateDisappearance();
                        spellReset();
                    }
                }
            }
            Vector2 direction = (targetDestination - (Vector2)transform.position).normalized;
            rb.MovePosition((Vector2)transform.position + direction * gargoyleSpeed * Time.fixedDeltaTime);
            // Add curve to trajectory
            if (!crossedFirstDestination) rb.AddForce(Vector2.right * direction.x * curveController1, ForceMode2D.Force);
            else rb.AddForce(Vector2.right * direction.x * curveController2, ForceMode2D.Force);
            // Rotate
            UpdateRotation(direction);
        }
    }

    public void spellRelease(Vector2 targetDestination, PrefabPool pool)
    {
        this.gargoylePool = pool;
        move = true;
        enemyDarkOrbAffected = false;
        crossedFirstDestination = false;
        hitOccurred = false;
        enemyTag = (CompareTag(GameConstant.PLAYER_TAG)) ? GameConstant.ENEMY_TAG : GameConstant.PLAYER_TAG;
        this.targetDestination = targetDestination + Vector2.up * UnitsTracker.getChampionByTag(tag).transform.position.y;
        firstDestination = targetDestination;
    }

    private void ActivateHit()
    {
        hitOccurred = true;
        crossedFirstDestination = false;
        firstDestination = new Vector2(firstDestination.x,-firstDestination.y);
        targetDestination = firstDestination + Vector2.up * UnitsTracker.getChampionByTag(enemyTag).transform.position.y; ;
        GameObject HitObj = gargoylePool.getFromPool(HIT_ID, HitPrefab);
        HitObj.transform.position = this.transform.position;
        HitObj.SetActive(true);
        //send damage
        int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, hitRangeRadius, affectedObjects, getGargoylesSwarmLayerMask());
        sendHpChangeToAffectedObjects(affectedObjects, numObjAffected,championDamage);
        //Attach hit prefab to champion
        affectedChampion = affectedObjects[0].gameObject;
        HitObj.transform.SetParent(affectedChampion.transform);
        // Enemy dark orb affected
        if (affectedChampion.GetComponent<SpriteRenderer>().color.b != 1) // <- this is not the way to do it -- just testing to see if it works
        {
            enemyDarkOrbAffected = true;
        }
        //Audio
        AudioCenter.playOneShot(hitAudio, 0.2f);
    }

    private void ActivateDisappearance()
    {
        GameObject DisappearObj = gargoylePool.getFromPool(DISAPPEAR_ID, DisappearPrefab);
        DisappearObj.transform.position = this.transform.position;
        DisappearObj.SetActive(true);
        //Attach prefab to affected Champion
        DisappearObj.transform.SetParent(affectedChampion.transform);
        if (enemyDarkOrbAffected)
        {
            GameObject RegenerateObj = gargoylePool.getFromPool(REGENERATE_ID, RegeneratePrefab);
            RegenerateObj.transform.position = this.transform.position;
            RegenerateObj.SetActive(true);
            //Attach prefab to affected Champion
            RegenerateObj.transform.SetParent(affectedChampion.transform);
        }

    }

    private void ActivateRegeneration()
    {
        int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, hitRangeRadius, affectedObjects, getGargoylesSwarmLayerMask());
        affectedChampion = affectedObjects[0].gameObject;
        if (!enemyDarkOrbAffected)
            sendHpChangeToAffectedObjects(affectedObjects, numObjAffected, -championDamage/10);
        else
            sendHpChangeToAffectedObjects(affectedObjects, numObjAffected, -championDamage);
    }

    private void sendHpChangeToAffectedObjects(Collider2D[] affectedObjs, int numObj,float hpChange)
    {
        Assert.IsTrue(numObj <= 2);
        for (int i = 0; i < numObj; i++)
        {
            ((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(hpChange, buildingDamage);
        }
    }

    public void spellReset()
    {
        this.gameObject.SetActive(false);
        this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        move = false;
        hitOccurred = false;
        crossedFirstDestination = false;
        enemyDarkOrbAffected = false;
    }

    public void fillPrefabPool(PrefabPool pool)
    {
        pool.addToPool(HIT_ID, HitPrefab, 3);
        pool.addToPool(DISAPPEAR_ID, DisappearPrefab, 3);
        pool.addToPool(REGENERATE_ID, RegeneratePrefab, 3);
    }

    private int getGargoylesSwarmLayerMask()
    {
        //return (int)GameConstant.LayerId.CastleAndPlayer + (int)GameConstant.LayerId.BuildingHeight1 + (int)GameConstant.LayerId.BuildingHeight2;
        int mask = 0;
        foreach (GameConstant.LayerId id in hitAffectedLayers)
        {
            mask += (int)id;
        }

        Assert.IsTrue(mask != 0);
        return mask;
    }

    private void UpdateRotation(Vector2 direction)
    {
        float alpha = Mathf.Acos(Vector2.Dot(Vector2.up,direction));
        if (direction.x > 0) alpha *= -1;
        alpha *= 180 / Mathf.PI;
        transform.rotation = Quaternion.Euler(0, 0, alpha);
    }
}
