﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class DarkOrb : MonoBehaviour, ISpell
{
    //public DarkOrbCaster darkOrbCaster;

    [SerializeField] private float orbSpeed;
    [SerializeField] private float championDamage;
    private const float buildingDamage=0;
    private float absorptionRangeRadius;
    [SerializeField] private GameConstant.LayerId[] absorptionAffectedLayers;
    [SerializeField] private float orbFixationPhaseTime;
    //[SerializeField] private float championAbsorptionTime;
    private float currentFixationTime;
    //private float localAbsorptionTime;

    private bool move;
    private Vector2 targetDestination;
    private PrefabPool darkOrbPool;
    private Collider2D[] affectedObjects;
    private Rigidbody2D rb;
    private float spawnPosY;

    private const int FADE_ID = 2;
    private const int ABSORB_ID = 3;
    [SerializeField] private GameObject FadePrefab;
    [SerializeField] private GameObject AbsorbPrefab;
    private GameObject spellEffectObj;
    public GameObject SpellEffectObj { get { return spellEffectObj; } set { spellEffectObj = value; } }

    private SpriteRenderer championSpriteRenderer;
    [SerializeField] private AudioClip absorptionAudio;
   

    // Start is called before the first frame update
    public void spellInit()
    {
        affectedObjects = new Collider2D[3];
        absorptionRangeRadius = this.gameObject.GetComponent<CircleCollider2D>().radius;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (move)
        {
            Vector2 direction = (targetDestination - (Vector2)transform.position).normalized;
            rb.MovePosition((Vector2)transform.position + direction * orbSpeed * Time.fixedDeltaTime);
            if (crossedDestination())
            {
                move = false;
                currentFixationTime = orbFixationPhaseTime;
            }
        }
        else if (currentFixationTime > 0)
        {
            currentFixationTime -= Time.deltaTime;
            if (currentFixationTime <= 0)
            {
                activateFade();
                spellReset();
            }
        }
    }

    public void spellRelease(Vector2 targetDestination, PrefabPool pool)
    {
        this.darkOrbPool = pool;
        move = true;
        this.targetDestination = targetDestination;
        spawnPosY = transform.position.y;
        currentFixationTime = 0;
    }

    private bool crossedDestination()
    {
        if (targetDestination.y > spawnPosY)
            return (transform.position.y > targetDestination.y);
        else
            return (transform.position.y < targetDestination.y);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Collision with champion
        if (Mathf.Pow(2, other.gameObject.layer) == (int)GameConstant.LayerId.Champion)
        {
            activateFade();
            int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, absorptionRangeRadius, affectedObjects, getDarkOrbLayerMask());
            sendDamageToAffectedObjects(affectedObjects, numObjAffected);
            currentFixationTime = 0;
            spellReset();
            //Activate absorb prefab
            GameObject absorbObj = darkOrbPool.getFromPool(ABSORB_ID, AbsorbPrefab);
            absorbObj.transform.position = other.transform.position;
            absorbObj.SetActive(true);
            // ==== AUDIO ====
            AudioCenter.playOneShot(absorptionAudio, 0.5f);
            //Spell Effect
            spellEffectObj.SetActive(true);
            spellEffectObj.GetComponent<DarkOrbSpellEffect>().apply(other.GetComponent<Champion>());
        }
        //Collision with building height 2
        else
        {
            activateFade();
            spellReset();
        }
    }  

    private void activateFade()
    {
        //Visual
        GameObject fadeObj = darkOrbPool.getFromPool(FADE_ID, FadePrefab);
        fadeObj.transform.position = this.transform.position;
        fadeObj.SetActive(true);
    }

    private void sendDamageToAffectedObjects(Collider2D[] affectedObjs, int numObj)
    {
        Assert.IsTrue(numObj <= 2);
        for (int i = 0; i < numObj; i++)
        {
            ((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(championDamage, buildingDamage);
			//((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveSpellEffects();

		}
    }

    public void spellReset()
    {
        this.gameObject.SetActive(false); //set projectile to not active	
        this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        move = false;
        currentFixationTime = 0;
    }

    public void fillPrefabPool(PrefabPool pool)
    {
        pool.addToPool(FADE_ID,FadePrefab,4);
        pool.addToPool(ABSORB_ID, AbsorbPrefab, 2);
    }

    private int getDarkOrbLayerMask()
    {
        //return (int)GameConstant.LayerId.CastleAndPlayer + (int)GameConstant.LayerId.BuildingHeight1 + (int)GameConstant.LayerId.BuildingHeight2;
        int mask = 0;
        foreach (GameConstant.LayerId id in absorptionAffectedLayers)
        {
            mask += (int)id;
        }

        Assert.IsTrue(mask != 0);
        return mask;
    }
}
