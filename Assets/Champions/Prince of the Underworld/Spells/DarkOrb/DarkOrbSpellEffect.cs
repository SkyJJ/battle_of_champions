﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkOrbSpellEffect : ActiveSpellEffect
{
	[SerializeField] private float movementSpeedReduction;
	[SerializeField] private Color32 colorChange;

	private Color32 oriColor;

    private Champion affectedChampion;

    protected override void Start()
    {
        base.Start();
        isBuff = true;
    }

    private void Update()
    {
        updateSpellEffect();
        if (state == SpellEffectState.ApplyEnd)
        {
            apply(affectedChampion);
        }
    }

    protected override void applyFirstTimeEffect(Champion c) {
        affectedChampion = c;
		SpriteRenderer s = c.GetComponent<SpriteRenderer>();
		oriColor = s.color;
		s.color = colorChange;
		c.CurrentMovementSpeed -= movementSpeedReduction;
        currentEffectDuration = effectDuration;
	}

	protected override void applyMiddleEffect(Champion champion)
	{
		currentEffectDuration = effectDuration;
	}

	protected override void applyEndEffect(Champion champion)
	{
		champion.GetComponent<SpriteRenderer>().color = oriColor;
		champion.CurrentMovementSpeed += movementSpeedReduction;
	}


}
