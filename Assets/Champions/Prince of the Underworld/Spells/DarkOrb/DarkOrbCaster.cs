﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkOrbCaster : ActiveSpellCaster
{
    [SerializeField] private GameObject PrecastPrefab;
    [SerializeField] private GameObject OrbPrefab;
    [SerializeField] private GameObject spellEffectPrefab;
    private GameObject spellEffectObj;

    [SerializeField] private DarkOrb darkOrbScript;

    [SerializeField] private float ySpawnOffsetFromChampion;
    [SerializeField] private UnitsTracker.TargetDisplayType targetDisplayType;
    [SerializeField] private UnitsTracker.TargetMask targetMask;

    [SerializeField] private AudioClip precastAudio;
    [SerializeField] private AudioClip spawnAudio;
    private AudioSource audioSourceForPrecast;

    private const int PRECAST_ID = 0;
    private const int ORB_ID = 1;

    private bool precasting;

    private GameObject targetDisplayObject;
    private GameObject inUsePrecastObj;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        precasting = false;
        fillPrefabPool();
        targetDisplayObject = UnitsTracker.getTargetDisplayObject(targetDisplayType, tag);
        audioSourceForPrecast = GetComponent<AudioSource>();
        audioSourceForPrecast.volume = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (precasting)
        {
            currentPrecastTime -= Time.deltaTime;
            if (currentPrecastTime <= 0)
                release();
        }
        else if (currentCooldownTime > 0)
        {
            currentCooldownTime -= Time.deltaTime;
        }
    }

    public override void select()
    {
        //Do nothing
    }

    public override void cast(Vector2 inputPos)
    {
        if (currentCooldownTime <= 0)
        {
            if (precasting)
                //if it's already casting you only need to update the target display
                targetDisplayObject.transform.position = calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
            else
            {
                // ==== LOGIC ====
                precasting = true;
                currentPrecastTime = precastTime;
                // ==== VISUAL ====
                //Show target 
                targetDisplayObject.transform.position = calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
                //Precast effects
                inUsePrecastObj = prefabPool.getFromPool(PRECAST_ID, PrecastPrefab);
                Vector2 championPos = UnitsTracker.getChampionByTag(tag).transform.position;
                inUsePrecastObj.transform.position = new Vector2(championPos.x, championPos.y + ySpawnOffsetFromChampion);
                inUsePrecastObj.SetActive(true);
                // ==== AUDIO ====
                audioSourceForPrecast.clip = precastAudio;
                audioSourceForPrecast.Play();
            }
        }
    }

    public override void cancelCast()
    {
        if (precasting)
        {
            resetPreCasting();
        }
    }

    public override void release()
    {
        if (currentPrecastTime <= 0)
        {
            // ==== LOGIC ====
            Vector2 tempTargetPos = targetDisplayObject.transform.position;
            resetPreCasting();
            currentCooldownTime = cooldownTime;
            UnitsTracker.getChampionByTag(tag).CurrentMana -= manaCost;
            // send dark orb
            GameObject darkOrb = prefabPool.getFromPool(ORB_ID, OrbPrefab);
            Vector2 championPos = UnitsTracker.getChampionByTag(tag).transform.position;
            darkOrb.transform.position = new Vector2(championPos.x, championPos.y + ySpawnOffsetFromChampion);
            darkOrb.SetActive(true);
            darkOrb.GetComponent<DarkOrb>().SpellEffectObj = spellEffectObj;
            darkOrb.GetComponent<DarkOrb>().spellRelease(tempTargetPos, prefabPool);
            // ==== AUDIO ====
            AudioCenter.playOneShot(spawnAudio, 0.8f);
        }
    }

    private void resetPreCasting()
    {
        precasting = false;
        currentPrecastTime = precastTime;
        //Reset inUsePrecastObj
        inUsePrecastObj.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        inUsePrecastObj.SetActive(false);
        inUsePrecastObj = null;
        //Reset target dispaly
        targetDisplayObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        //Reset precast audio
        audioSourceForPrecast.Stop();
        audioSourceForPrecast.clip = null;
    }

    private void fillPrefabPool()
    {
        prefabPool.addToPool(PRECAST_ID, PrecastPrefab, 2);
        prefabPool.addToPool(ORB_ID, OrbPrefab, 4);
        darkOrbScript.fillPrefabPool(prefabPool);
        spellEffectObj = GameObject.Instantiate(spellEffectPrefab);
        spellEffectObj.tag = this.tag;
        spellEffectObj.SetActive(false);
    }

    private Vector3 calculateTargetDisplayPosition(Vector2 pos)
    {
        return UnitsTracker.getStandardTargetLocation(pos, targetMask, tag);
    }
}
