﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeCaster : ActiveSpellCaster
{
	private enum FadeState { NotActivated, BeforeFade, Fading, AfterFade };

	[SerializeField] private Champion princeOfTheUnderworldRef;
	[SerializeField] private float delayBeforeFade;
	[SerializeField] private float fadeTime;
	[SerializeField] private float animTimeAfterFade;
	private Champion princeOfTheUnderworld;
	private Collider2D princeCollider;
	private SpriteRenderer spriteRenderer;
	private float currentDelayBeforeFade;
	private float currentFadeTime;
	private float currentAnimTimeAfterFade;
	private FadeState fadeState;
	private float tColor;

	private Champion doppelganger;

	protected override void Start() {
		base.Start();
		Champion[] champions = UnitsTracker.getAllyChampionsByTag(tag);
		for (int i = 0; i < champions.Length; i++){
			//TODO With this implementation we can't have two same champions in the team.
			if (champions[i].ChampionName.Equals(princeOfTheUnderworldRef.ChampionName)) {
				princeOfTheUnderworld = champions[i];
				break;
			}
		}

		princeCollider = princeOfTheUnderworld.GetComponent<Collider2D>();
		spriteRenderer = princeOfTheUnderworld.GetComponent<SpriteRenderer>();
		currentDelayBeforeFade = delayBeforeFade;
		currentFadeTime = fadeTime;
		currentAnimTimeAfterFade = animTimeAfterFade;
		fadeState = FadeState.NotActivated;
		tColor = 0;
	}

	void Update() {
		switch (fadeState)
		{
			case FadeState.BeforeFade:
				beforeFade();
				break;

			case FadeState.Fading:
				fading();
				break;

			case FadeState.AfterFade:
				afterFade();
				break;
		}

		if (currentCooldownTime > 0) {
			currentCooldownTime -= Time.deltaTime;
		}
	}

	private void beforeFade() {
		tColor += Time.deltaTime / delayBeforeFade;
		spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.clear, tColor);
		Color c = spriteRenderer.color;
		spriteRenderer.color = new Color(c.r / 2, c.g / 2, c.b / 2, c.a);
		//spriteRenderer.color = Color.Lerp(spriteRenderer.color, new Color(0,0,0, ), tColor * 2);
		//Play fade sound

		currentDelayBeforeFade -= Time.deltaTime;
		//if (currentDelayBeforeFade <= 0)
		if(spriteRenderer.color.Equals(Color.clear))
		{
			Debug.Log("currentDelayBeforeFade : " + currentDelayBeforeFade);
			fadeState = FadeState.Fading;
			tColor = 0;
		}
	}

	private void fading() {
		princeCollider.enabled = false;

		currentFadeTime -= Time.deltaTime;
		if (currentFadeTime <= 0)
		{
			fadeState = FadeState.AfterFade;
		}
	}

	private void afterFade() {
		princeCollider.enabled = true;
		tColor += Time.deltaTime / animTimeAfterFade;
		spriteRenderer.color = Color.Lerp(spriteRenderer.color, Color.white, tColor);

		currentAnimTimeAfterFade -= Time.deltaTime;
		if (spriteRenderer.color.Equals(Color.white))
		{
			Debug.Log("currentAnimTimeAfterFade " + currentAnimTimeAfterFade);
			resetFade();
		}
	}

	private void resetFade() {
		fadeState = FadeState.NotActivated;
		currentDelayBeforeFade = delayBeforeFade;
		currentFadeTime = fadeTime;
		currentAnimTimeAfterFade = animTimeAfterFade;
		fadeState = FadeState.NotActivated;
		tColor = 0;
	}

	public override void select()
	{
		if (currentCooldownTime <= 0) {
			fadeState = FadeState.BeforeFade;
			currentCooldownTime = cooldownTime;
			princeOfTheUnderworld.CurrentMana -= manaCost;
		} 	
	}

	public override void cast(Vector2 inputPos)
	{
		return;
	}

	public override void cancelCast()
	{
		return;
	}

	public override void release()
	{
		return;
	}

}
