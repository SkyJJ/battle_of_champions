﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoppelgangerCaster : PassiveSpellCaster
{
	[SerializeField] private Champion princeOfTheUnderworldRef;
	private Champion princeOfTheUnderworld;

	protected override void Start()
	{
		base.Start();
		Champion[] champions = UnitsTracker.getAllyChampionsByTag(tag);

		for (int i = 0; i < champions.Length; i++)
		{
			//TODO With this implementation we can't have two same champions in the team.
			if (champions[i].ChampionName.Equals(princeOfTheUnderworldRef.ChampionName))
			{
				princeOfTheUnderworld = champions[i];
				break;
			}
		}

		//sendPassiveToChampion(princeOfTheUnderworld);
		princeOfTheUnderworld.receivePassive(passiveSpellEffect.GetInstanceID(), passiveSpellEffect);
	}



}
