﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDoppelganger 
{
	void initDoppelganger(Champion doppelganger);
}
