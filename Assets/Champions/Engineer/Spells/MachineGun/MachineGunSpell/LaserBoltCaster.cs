﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/*
 * Precast, wait, targetable
 * */
public class LaserBoltCaster : ActiveSpellCaster
{
	[SerializeField] private GameObject laserBoltManager;

	private const int LASER_BOLT_MANAGER_ID = 0;

	[SerializeField] private UnitsTracker.TargetDisplayType targetDisplayType;
	[SerializeField] private UnitsTracker.TargetMask targetMask;

	private bool casting;
	private GameObject targetDisplayObject;

	protected override void Start()
	{
		base.Start();
		casting = false;
		fillPrefabPool();
		targetDisplayObject = UnitsTracker.getTargetDisplayObject(targetDisplayType, tag);
	}

	private void fillPrefabPool()
	{
		prefabPool.addToPool(LASER_BOLT_MANAGER_ID, laserBoltManager, 1);
		laserBoltManager.GetComponent<LaserBoltManager>().fillPrefabPool(prefabPool);
	}

	// Update is called once per frame
	void Update()
    {
		if (currentCooldownTime > 0)
		{
			currentCooldownTime -= Time.deltaTime;
		}
	}

	public override void select()
	{
		return;
	}

	public override void cast(Vector2 inputPos)
	{
		if (currentCooldownTime <= 0)
		{
			if (casting)
				//if it's already casting you only need to update the target display
				targetDisplayObject.transform.position = calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
			else
			{
				casting = true;
				targetDisplayObject.transform.position = calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
				targetDisplayObject.gameObject.SetActive(true);
			}
		}

	}

	public override void cancelCast()
	{
		targetDisplayObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		targetDisplayObject.gameObject.SetActive(false);
		casting = false;
	}

	public override void release()
	{
		if (casting)
		{
			Assert.IsFalse(targetDisplayObject.transform.position.Equals(GameConstant.OUT_OF_CAMERA_POS));
			prefabPool.getFromPool(LASER_BOLT_MANAGER_ID, laserBoltManager).GetComponent<LaserBoltManager>().laserBoltManagerSpellRelease(targetDisplayObject.transform.position, prefabPool, this.transform.parent);
			//TODO sync machine gun rotation !!
			targetDisplayObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
			targetDisplayObject.gameObject.SetActive(false);
			casting = false;
			currentCooldownTime = cooldownTime;
		}
	}

	private Vector3 calculateTargetDisplayPosition(Vector2 pos)
	{
		return UnitsTracker.getStandardTargetLocation(pos, targetMask, tag);
	}




}
