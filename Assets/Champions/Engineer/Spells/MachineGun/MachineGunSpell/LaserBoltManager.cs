﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoltManager : MonoBehaviour, ISpell
{
	[SerializeField] private GameObject laserBoltPrefab;

	private const int LASER_BOLT_ID = 1;
	
	[SerializeField] private AudioClip shootAudio;
	[SerializeField] private float spawnOffSetFromBuildingY;
	[SerializeField] private int totalBulletsToFire;
	[SerializeField] private float fireRate; //How many bullets fired per second
	[SerializeField] private float bulletSpeed;
	[SerializeField] private float bulletChampionDamage;
	[SerializeField] private float bulletBuildingDamage;
	[SerializeField] private GameConstant.LayerId[] bulletAffectedLayers;

	private bool beginFiring;
	private int remainingBulletsToFire;
	private float waitTimeBetweenFire;
	private float currentWaitTimeBetweenFire;
	private Vector2 fireDestination;
	private PrefabPool prefabPool;
	private Transform parentBuildingTransform;

	public void spellInit()
	{
		beginFiring = false;
		remainingBulletsToFire = 0;
		waitTimeBetweenFire = 1f / fireRate;
		currentWaitTimeBetweenFire = 0f;
		fireDestination = GameConstant.OUT_OF_CAMERA_POS;
		gameObject.SetActive(false);
		parentBuildingTransform = null;
	}

	public void spellReset() {
		gameObject.SetActive(false);
		beginFiring = false;
		remainingBulletsToFire = 0;
		waitTimeBetweenFire = 1f / fireRate;
		currentWaitTimeBetweenFire = 0f;
		fireDestination = GameConstant.OUT_OF_CAMERA_POS;
		transform.parent = null;
		parentBuildingTransform = null;
	}

	void Update()
    {
		//if time passed is over wait time
		if (beginFiring)
		{
			if (remainingBulletsToFire > 0 && currentWaitTimeBetweenFire <= 0) {
				fireBullet();
				remainingBulletsToFire--;
				currentWaitTimeBetweenFire = waitTimeBetweenFire;
			}
			currentWaitTimeBetweenFire -= Time.deltaTime;

			if (remainingBulletsToFire <= 0) {
				spellReset();
			}
		}
    }

	private void fireBullet() {
		prefabPool.getFromPool(LASER_BOLT_ID, laserBoltPrefab).GetComponent<LaserBolt>().laserSpellRelease(
			fireDestination, prefabPool,
			new Vector2(parentBuildingTransform.position.x, parentBuildingTransform.position.y + spawnOffSetFromBuildingY)
			, bulletSpeed, bulletChampionDamage, bulletBuildingDamage, bulletAffectedLayers);
		//== AUDIO ==
		AudioCenter.playOneShot(shootAudio, 0.18f);
	}

	public void laserBoltManagerSpellRelease(Vector2 targetDestination, PrefabPool pool, Transform parentBuildingTrans) {
		parentBuildingTransform = parentBuildingTrans;
		//child the manager to the building, so that if the building is destroyed, the manager is also destroyed (thus stopping bolts firing)
		this.transform.SetParent(parentBuildingTrans, false); 
		spellRelease(targetDestination,pool);
	}

	public void spellRelease(Vector2 targetDestination, PrefabPool pool)
	{
		prefabPool = pool;
		fireDestination = targetDestination;
		remainingBulletsToFire = totalBulletsToFire;
		beginFiring = true;
		this.gameObject.SetActive(true);
	}

	public void fillPrefabPool(PrefabPool prefabPool) {
		prefabPool.addToPool(LASER_BOLT_ID, laserBoltPrefab, 18);
		this.prefabPool = prefabPool;
		laserBoltPrefab.GetComponent<LaserBolt>().fillPrefabPool(prefabPool);
	}


}
