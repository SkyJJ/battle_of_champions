﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class LaserBolt : MonoBehaviour, ISpell
{
	[SerializeField] private GameObject bulletHitPrefab;
	[SerializeField] private AudioClip hitAudio;
	

	private const int BULLET_HIT_ID = 2;

	private Rigidbody2D rb;
	private CapsuleCollider2D capsuleCollider;
	private PrefabPool prefabPool;
	private Vector2 targetDestination;
	private float spawnPosY;
	private float speed;
	private float championDamage;
	private float buildingDamage;
	private GameConstant.LayerId[] affectedLayers;
	private bool moving;
	//private Vector2 bulletSize;
	private Collider2D[] affectedObjects = new Collider2D[GameConstant.MAX_SPELL_HIT_OBJECTS];

	public void spellInit() {
		rb = GetComponent<Rigidbody2D>();
		capsuleCollider = GetComponent<CapsuleCollider2D>();
		//bulletSize = GetComponent<CapsuleCollider2D>().size;
	}

	public void laserSpellRelease(Vector2 targetDestination, PrefabPool pool, Vector2 spawnDestination, float speed, float championDamage, float buildingDamage, GameConstant.LayerId[] affectedLayers)
	{
		spellRelease(targetDestination, pool);
		this.transform.position = spawnDestination;
		this.spawnPosY = transform.position.y;
		this.speed = speed;
		this.championDamage = championDamage;
		this.buildingDamage = buildingDamage;
		this.affectedLayers = affectedLayers;
		this.moving = true;
		this.transform.rotation = Quaternion.Euler(0, 0, -90 + Mathf.Atan2(targetDestination.y - spawnDestination.y, targetDestination.x - spawnDestination.x) * 180 / Mathf.PI);
		this.gameObject.SetActive(true);
	}

	public void spellRelease(Vector2 targetDestination, PrefabPool pool) {
		this.targetDestination = targetDestination;
		this.prefabPool = pool;
	}

	public void spellReset()
	{
		this.gameObject.SetActive(false); //set projectile to not active	
										  //this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		moving = false;
	}


	void FixedUpdate() {
		if (moving)
		{
			moveBolt();
			if (crossedDestination()){
				activateExplosion();
				spellReset();
			}
		}
	}

	private bool crossedDestination()
	{
		if (targetDestination.y > spawnPosY)
			return (transform.position.y > targetDestination.y);
		else
			return (transform.position.y < targetDestination.y);
	}

	private void moveBolt() {
		Vector2 direction = (targetDestination - (Vector2)transform.position).normalized;
		rb.MovePosition((Vector2)transform.position + direction * speed * Time.fixedDeltaTime);
	}

	private void activateExplosion()
	{
		// ==== VISUAL ====
		
		GameObject explosionObj = prefabPool.getFromPool(BULLET_HIT_ID, bulletHitPrefab);
		explosionObj.transform.position = this.transform.position;
		explosionObj.SetActive(true);
		// ==== AUDIO ====
		AudioCenter.playOneShot(hitAudio, 0.2f);
		// ==== LOGIC ====
		//int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, bulletSize, affectedObjects, getLayerMask());
		int numObjAffected = Physics2D.OverlapCapsuleNonAlloc(this.transform.position, capsuleCollider.size, capsuleCollider.direction, transform.rotation.z, affectedObjects, getLayerMask());
		sendDamageToAffectedObjects(affectedObjects, numObjAffected);

		this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		/*bool isEnemy = false;
		switch (this.tag)
		{
			case GameConstant.PLAYER_TAG:
				isEnemy = other.CompareTag(GameConstant.ENEMY_TAG);
				break;
			case GameConstant.ENEMY_TAG:
				isEnemy = other.CompareTag(GameConstant.PLAYER_TAG);
				break;
			default:
				Assert.IsFalse(true, "A collider without tag is triggered. Not expect to happen.");
				break;
		}

		if (isEnemy)
		{
			activateExplosion();
			spellReset();
		}*/
		activateExplosion();
		spellReset();
	}

	private void sendDamageToAffectedObjects(Collider2D[] affectedObjs, int numObj)
	{
		for (int i = 0; i < numObj; i++)
		{
			//Debug.Log("affectedObj : " + affectedObjs[i].name);
			//Debug.Log("affectedObj : " + (IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable)));
			((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(championDamage, buildingDamage);
		}
	}

	public void fillPrefabPool(PrefabPool prefabPool) {
		prefabPool.addToPool(BULLET_HIT_ID, bulletHitPrefab, 18);
		this.prefabPool = prefabPool;
	}

	private int getLayerMask()
	{
		//return (int)GameConstant.LayerId.CastleAndPlayer + (int)GameConstant.LayerId.BuildingHeight1 + (int)GameConstant.LayerId.BuildingHeight2;
		int mask = 0;
		foreach (GameConstant.LayerId id in affectedLayers)
		{
			mask += (int)id;
		}

		Assert.IsTrue(mask != 0);
		return mask;
	}
}
