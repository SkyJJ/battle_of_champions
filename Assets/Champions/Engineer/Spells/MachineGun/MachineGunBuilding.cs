﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBuilding : StandardBuilding
{
	// buildId, currentHp, currentBuildTime, selected, isBuilt, spellCasters;
	[SerializeField] private SpriteRenderer spriteHead;
	[SerializeField] private SpriteRenderer spriteBase;

	protected override void setPreBuildDisplay()
	{
		setColor(Color.green);
	}

	protected override void setBuiltDisplay()
	{
		setColor(Color.white);
	}

	private void setColor(Color c)
	{
		spriteHead.color = c;
		spriteBase.color = c;
	}


}
