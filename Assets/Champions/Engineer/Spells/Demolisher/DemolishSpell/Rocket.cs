﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Rocket : MonoBehaviour, ISpell
{
	[SerializeField] private GameObject explosionPrefab;
	[SerializeField] private GameObject rocketTrailPrefab;

	private const int TRAIL_ID = 4;
	private const int EXPLOSION_ID = 5;

	[SerializeField] private AudioClip deployedAudio;
	[SerializeField] private AudioClip flyingAudio;
	[SerializeField] private AudioClip explosionAudio;

	[SerializeField][Range(0f, 1f)] private float deployedVol;
	[SerializeField][Range(0f, 1f)] private float flyingVol;
	[SerializeField][Range(0f, 1f)] private float explosionVol;

	[SerializeField] private float championDamage;
	[SerializeField] private float buildingDamage;
	[SerializeField] private float explosionRadius;
	[SerializeField] private GameConstant.LayerId[] explosionAffectedLayers;
	[SerializeField] private float rocketInitialSpeed;
	[SerializeField] private float rocketInitialAcceleration;
	//[SerializeField] private float rocketAccelerationMultiplier;
	[SerializeField] private float rocketAccelerationAcceleration;
	[SerializeField] private float rocketMaxSpeed;
	[SerializeField] private float spawnHorizontalOffsetFromBuilding;
	[SerializeField] private float waitTimeBeforeLaunch;
	[SerializeField] private Vector2 offsetFromBuildingBeforeLaunch; //0.62 0.12
	[SerializeField] private float initialTargetRotationAngle;

	private GameObject rocketTrail;
	private AudioSource audioSource;
	private Rigidbody2D rb;
	private bool spawnLeft;
	private Vector2 rocketTargetDestination;
	private float spawnPosY;
	private PrefabPool prefabPool;
	private float currentWaitTimeBeforeLaunch;
	private bool deploying;
	private bool launched;
	private float currentRocketSpeed;
	private float currentRocketAcceleration;
	private Collider2D[] affectedObjects;
	
	// === Variables for SmoothDamp function (during deploy) ===
	private Vector2 buildingLocation;
	private Vector2 posBeforeLaunch;
	private Vector2 speed;
	private Vector2 rocketStoredPos;

	// === for SmoothDamp function (during deploy rotation) ===
	private float currentRotation;
	private float targetRotation;
	private float rotationSpeed;

	public void fillPrefabPool(PrefabPool pool)
	{
		pool.addToPool(TRAIL_ID, rocketTrailPrefab, 2);
		pool.addToPool(EXPLOSION_ID, explosionPrefab, 2);
	}

	public void spellInit()
	{
		audioSource = GetComponent<AudioSource>();
		rb = GetComponent<Rigidbody2D>();
		deploying = false;
		launched = false;
		currentRocketSpeed = rocketInitialSpeed;
		currentRocketAcceleration = rocketInitialAcceleration;
		currentWaitTimeBeforeLaunch = waitTimeBeforeLaunch;
		speed = Vector2.zero;
		currentRotation = 0;
		rotationSpeed = 0;
		affectedObjects = new Collider2D[GameConstant.MAX_SPELL_HIT_OBJECTS];
	}
	public void spellReset()
	{
		rocketTrail.transform.parent = null;
		gameObject.SetActive(false);
		//transform.position = GameConstant.OUT_OF_CAMERA_POS;
		rb.position = GameConstant.OUT_OF_CAMERA_POS;

		audioSource.clip = null;
		deploying = false;
		launched = false;
		currentRocketSpeed = rocketInitialSpeed;
		currentRocketAcceleration = rocketInitialAcceleration;
		currentWaitTimeBeforeLaunch = waitTimeBeforeLaunch;
		speed = Vector2.zero;
		currentRotation = 0;
		rotationSpeed = 0;
		Array.Clear(affectedObjects, 0, affectedObjects.Length);
	}

	public void rocketSpellRelease(Vector2 targetDestination, PrefabPool pool, Vector2 buildingLocation, bool spawnLeft) {
		this.buildingLocation = buildingLocation;
		this.spawnLeft = spawnLeft;
		spellRelease(targetDestination, pool);
	}

	public void spellRelease(Vector2 targetDestination, PrefabPool pool)
	{
		rocketTargetDestination = (spawnLeft) ? new Vector2(targetDestination.x - offsetFromBuildingBeforeLaunch.x, targetDestination.y) : new Vector2(targetDestination.x + offsetFromBuildingBeforeLaunch.x, targetDestination.y);
		prefabPool = pool;
		setSpawnLocationAndLaunchPosition();
		setRocketLaunchTargetRotation();
		setUpRocketTrail();
		deployRocket();
		gameObject.SetActive(true);
	}

	private void setUpRocketTrail() {
		rocketTrail = prefabPool.getFromPool(TRAIL_ID, rocketTrailPrefab);
		rocketTrail.transform.localPosition = Vector3.zero;
		rocketTrail.transform.localRotation = Quaternion.identity;
		rocketTrail.transform.SetParent(transform, false);
		rocketTrail.SetActive(true);
	}

	private void setSpawnLocationAndLaunchPosition()
	{
		//TODO enemy calculation will be different 
		transform.position = (spawnLeft) ? new Vector2(buildingLocation.x - spawnHorizontalOffsetFromBuilding, buildingLocation.y) : new Vector2(buildingLocation.x + spawnHorizontalOffsetFromBuilding, buildingLocation.y);
		spawnPosY = transform.position.y;

		//transform.rotation = Quaternion.Euler(45, 0, 0);
		posBeforeLaunch = new Vector2(buildingLocation.x + offsetFromBuildingBeforeLaunch.x, buildingLocation.y + offsetFromBuildingBeforeLaunch.y);
	}

	private void setRocketLaunchTargetRotation() {
		if (spawnLeft)
		{
			//targetRotation = -90 + Mathf.Atan2(targetDestination.y - posBeforeLaunch.y, targetDestination.x - (buildingLocation.x - offsetFromBuildingBeforeLaunch.x)) * 180 / Mathf.PI;
			targetRotation =  initialTargetRotationAngle + Mathf.Atan2(rocketTargetDestination.y - posBeforeLaunch.y, rocketTargetDestination.x - (buildingLocation.x - offsetFromBuildingBeforeLaunch.x)) * 180 / Mathf.PI;

		}
		else {
			//targetRotation = -90 + Mathf.Atan2(targetDestination.y - posBeforeLaunch.y, targetDestination.x - posBeforeLaunch.x) * 180 / Mathf.PI;
			targetRotation =  initialTargetRotationAngle + Mathf.Atan2(rocketTargetDestination.y - posBeforeLaunch.y, rocketTargetDestination.x - posBeforeLaunch.x) * 180 / Mathf.PI;

		}
	}

	private void deployRocket()
	{
		AudioCenter.playOneShot(deployedAudio, deployedVol);
		rocketStoredPos = new Vector2(buildingLocation.x + spawnHorizontalOffsetFromBuilding, buildingLocation.y);
		deploying = true;
	}

	void Update()
    {
		if (deploying)
		{
			animateRocketDeploy();

			currentWaitTimeBeforeLaunch -= Time.deltaTime;	
			if (currentWaitTimeBeforeLaunch <= 0){
				deploying = false;
				launchRocket();
				launched = true;
			}
		}
		/*else if (launched) {
			//moveRocket();
			if (transform.position.y >= rocketTargetDestination.y)
			{
				activateExplosion();
				spellReset();
			}
		}*/

		//TODO rocketTargetDestination calculation
		//need to set a new target display for this spell. The clamping should happen earlier when user input.

	}

	void FixedUpdate() {
		if (launched) {
			moveRocket();
			if (crossedDestination())
			{
				activateExplosion();
				spellReset();
			}
		}
	}

	private bool crossedDestination()
	{
		if (rocketTargetDestination.y > spawnPosY)
			return (transform.position.y > rocketTargetDestination.y);
		else
			return (transform.position.y < rocketTargetDestination.y);
	}

	private void animateRocketDeploy() {
		rocketStoredPos = Vector2.SmoothDamp(rocketStoredPos, posBeforeLaunch, ref speed, waitTimeBeforeLaunch);
		transform.position = (spawnLeft) ? new Vector2(2 * buildingLocation.x - rocketStoredPos.x, rocketStoredPos.y) : rocketStoredPos;

		currentRotation = Mathf.SmoothDamp(currentRotation, targetRotation, ref rotationSpeed, waitTimeBeforeLaunch);
		transform.rotation = Quaternion.Euler(45, 0, currentRotation);
	}

	private void launchRocket() {
		audioSource.clip = flyingAudio;
		audioSource.volume = flyingVol;
		audioSource.Play();
	}

	private void moveRocket() {
		/*transform.position = Vector3.MoveTowards(transform.position, rocketTargetDestination, currentRocketSpeed * Time.deltaTime);
		currentRocketSpeed += currentRocketAcceleration * Time.deltaTime;
		currentRocketAcceleration += rocketAccelerationAcceleration * Time.deltaTime;*/

		Vector2 direction = (rocketTargetDestination - (Vector2)transform.position).normalized;
		rb.MovePosition((Vector2)transform.position + direction * currentRocketSpeed * Time.fixedDeltaTime);
		currentRocketSpeed += currentRocketAcceleration * Time.fixedDeltaTime;
		currentRocketAcceleration += rocketAccelerationAcceleration * Time.fixedDeltaTime;

		//Debug.Log("Rocket velocity : " + GetComponent<Rigidbody2D>().velocity);
		//Debug.Log("Rocket velocity : " + GetComponent<Rigidbody2D>().velocity.sqrMagnitude);
		//rocketAcceleration += rocketAccelerationMultiplier;

	}

	private void activateExplosion() {
		// === Visual ===
		GameObject explosionObj = prefabPool.getFromPool(EXPLOSION_ID, explosionPrefab);
		explosionObj.transform.position = transform.position;
		explosionObj.SetActive(true);
		// === Audio ===
		AudioCenter.playOneShot(explosionAudio, explosionVol);
		// === Send damage ===
		int numObjAffected = Physics2D.OverlapCircleNonAlloc(transform.position, explosionRadius, affectedObjects, getDamageMask());
		sendDamageToAffectedObjects(affectedObjects, numObjAffected);
	}

	private void sendDamageToAffectedObjects(Collider2D[] affectedObjs, int numObj){
		Assert.IsTrue(numObj <= 3);
		for (int i = 0; i < numObj; i++)
		{
			//Debug.Log("affectedObj : " + affectedObjs[i].name);
			//Debug.Log("affectedObj : " + (IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable)));
			((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(championDamage, buildingDamage);
		}
	}

	private int getDamageMask() {
		int mask = 0;
		foreach (GameConstant.LayerId id in explosionAffectedLayers){
			mask += (int)id;
		}
		Assert.IsTrue(mask != 0);
		return mask;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		/*bool isEnemy = false;
		switch (this.tag) {
			case GameConstant.PLAYER_TAG:
				isEnemy = other.CompareTag(GameConstant.ENEMY_TAG);
				break;
			case GameConstant.ENEMY_TAG:
				isEnemy = other.CompareTag(GameConstant.PLAYER_TAG);
				break;
			default:
				Assert.IsFalse(true, "A collider without tag is triggered. Not expect to happen.");
				break;
		}

		if (isEnemy)
		{
			activateExplosion();
			spellReset();
		}*/
		activateExplosion();
		spellReset();
	}

}
