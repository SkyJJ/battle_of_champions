﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemolisherBuilding : StandardBuilding
{
	[SerializeField] private SpriteRenderer demolisherBuildingSprite;

	protected override void setPreBuildDisplay()
	{
		demolisherBuildingSprite.color = Color.green;
	}

	protected override void setBuiltDisplay()
	{
		demolisherBuildingSprite.color = Color.white;
	}


}
