﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneBlessingEffect : PassiveSpellEffect
{
	public override void apply(Champion champion)
	{
		Debug.Log("Arcane blessing temporary effect.");
		champion.CurrentMagicalDefense += 20;
	}

	public override void apply(Champion champion, int level)
	{
		throw new System.NotImplementedException();
	}
}
