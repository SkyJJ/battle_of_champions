﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneBlessingCaster : PassiveSpellCaster
{
	private Champion[] allyChampions;

	protected override void Start() {
		base.Start();
		allyChampions = UnitsTracker.getAllyChampionsByTag(tag);

		for (int i = 0; i < allyChampions.Length; i++) {
			//sendPassiveToChampion(allyChampions[i]);
			allyChampions[i].receivePassive(passiveSpellEffect.GetInstanceID(), passiveSpellEffect);
		}
	}


}
