﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class IceShard : MonoBehaviour, ISpell
{
    [SerializeField] private GameObject ExplosionPrefab;
    private const int EXPLOSION_ID = 3;

    [SerializeField] private AudioClip explosionClip;

    [SerializeField] private float projectileSpeed;
    [SerializeField] private float championDamage;
    [SerializeField] private float buildingDamage;
    [SerializeField] private float explosionRangeRadius;
    [SerializeField] private GameConstant.LayerId[] explosionAffectedLayers;

    private bool move = false;
    private Vector2 targetDestination;
    private PrefabPool iceShardPool;
    private Collider2D[] affectedObjects;
	private Rigidbody2D rb;

    private float[] initialExplosionParameters;
    [SerializeField] private float explosionSizeController;

    private float spawnPosY;
    private float iceShardSizeRatio;
    public float IceShardSizeRatio { get { return iceShardSizeRatio; } set { iceShardSizeRatio = value; } }

	public void spellInit() {
		initialExplosionParameters = new float[3] { championDamage, buildingDamage, explosionRangeRadius };
		//Warning : affected objects may exceed 3
		affectedObjects = new Collider2D[GameConstant.MAX_SPELL_HIT_OBJECTS];
		rb = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
   /* void Update()
    {
        if (move)
        {
            //when reach targetDestination, explode
            this.transform.position = Vector3.MoveTowards(this.transform.position, targetDestination, projectileSpeed * Time.deltaTime);

            //explode
            if (this.transform.position.Equals(targetDestination))
            {
                activateExplosion();
                spellReset();
            }
        }
    }*/

	void FixedUpdate() {
		if (move)
		{
			//when reach targetDestination, explode
			//this.transform.position = Vector3.MoveTowards(this.transform.position, targetDestination, projectileSpeed * Time.deltaTime);
			Vector2 direction = (targetDestination - (Vector2)transform.position).normalized;
			rb.MovePosition((Vector2)transform.position + direction * projectileSpeed * Time.fixedDeltaTime);

			//explode
			if (crossedDestination())
			{
				activateExplosion();
				spellReset();
			}
		}
	}

    private bool crossedDestination()
    {
        if (targetDestination.y > spawnPosY)
            return (transform.position.y > targetDestination.y);
        else
            return (transform.position.y < targetDestination.y);
    }

    public void spellRelease(Vector2 targetDestination, PrefabPool pool)
    {
        this.iceShardPool = pool;
        move = true;
        this.targetDestination = targetDestination;
        spawnPosY = transform.position.y;
    }

    public void addToPool(PrefabPool pool)
    {
        pool.addToPool(EXPLOSION_ID, ExplosionPrefab, 4);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        activateExplosion();
        spellReset();
    }

    private void activateExplosion()
    {
        // ==== VISUAL ====
        GameObject explosionObj = iceShardPool.getFromPool(EXPLOSION_ID, ExplosionPrefab);
        resetExplosionParticleSystems(explosionObj);
        modifyExplosionParticleSystems(explosionObj);
        recalliberExplosionParameters(iceShardSizeRatio);
        explosionObj.transform.position = this.transform.position;
        explosionObj.SetActive(true);
        // ==== AUDIO ====
        AudioCenter.playOneShot(explosionClip, 0.3f * iceShardSizeRatio);
        // ==== LOGIC ====
        int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, explosionRangeRadius, affectedObjects, getIceShardLayerMask());
        sendDamageToAffectedObjects(affectedObjects, numObjAffected);
    }

    public void spellReset()
    {
        resetExplosionParameters();
        this.gameObject.SetActive(false); //set projectile to not active	
        this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
        move = false;
    }

    private void sendDamageToAffectedObjects(Collider2D[] affectedObjs, int numObj)
    {
        Assert.IsTrue(numObj <= 3);
        for (int i = 0; i < numObj; i++)
        {
            ((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(championDamage, buildingDamage);
        }
    }

    private int getIceShardLayerMask()
    {
        int mask = 0;
        foreach (GameConstant.LayerId id in explosionAffectedLayers)
        {
            mask += (int)id;
        }

        Assert.IsTrue(mask != 0);
        return mask;
    }

    public void recalliberExplosionParameters(float iceShardsRatio)
    {
        IceShard iceShard = this.gameObject.GetComponent<IceShard>();
        iceShard.championDamage *= iceShardsRatio;
        iceShard.buildingDamage *= iceShardsRatio;
        iceShard.explosionRangeRadius *= iceShardsRatio;
    }

    public void resetExplosionParameters()
    {
        IceShard iceShard = this.gameObject.GetComponent<IceShard>();
        iceShard.championDamage = initialExplosionParameters[0];
        iceShard.buildingDamage = initialExplosionParameters[1];
        iceShard.explosionRangeRadius = initialExplosionParameters[2];
    }

    public void resetExplosionParticleSystems(GameObject explosionGameObject)
    {
        ParticleSystem[] particleSystems = explosionGameObject.GetComponentsInChildren<ParticleSystem>();
        ParticleSystem[] prefabParticleSystems = ExplosionPrefab.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < 2; i++)
        {
            var main = particleSystems[i].main;
            var prefabMain = prefabParticleSystems[i].main;
            main.startSizeMultiplier = prefabMain.startSizeMultiplier; 
        }
    }

    public void modifyExplosionParticleSystems(GameObject explosionGameObject)
    {
        ParticleSystem[] particleSystems = explosionGameObject.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem particleSystem in particleSystems)
        {
            var main = particleSystem.main;
            main.startSizeMultiplier *= iceShardSizeRatio * explosionSizeController;
        }
    }
}
