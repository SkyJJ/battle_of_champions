﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceShardCaster : ActiveSpellCaster
{
	/*
	 * Inherited variables
	 * Designed data (protected) : icon, spellName, manaCost, cooldownTime, precastTime
	 * In game data (protected) : currentCooldownTime, currentPrecastTime
	 * */

	[SerializeField] private GameObject Precast;
	[SerializeField] private GameObject Spawn;
	[SerializeField] private GameObject ProjectileTrail;
	[SerializeField] private GameObject Explosion;
    
    [SerializeField] private IceShard iceShardScript;
    [SerializeField] private UnitsTracker.TargetMask targetMask;

    [SerializeField] private AudioClip precastAudio;
    [SerializeField] private AudioClip spawnAudio;

    [SerializeField] private float ySpawnOffsetFromChampion;
    private float xSpawnOffset;
    [SerializeField] private float sizeMultiplierController;

    [SerializeField] private float chargingTime;
    private float currentChargingTime;

    private float iceShardsSizeRatio;
    private float iceShardMinimumRatio=0.3f;

    private const int PRECAST_ID = 0;
    private const int PROJECTILE_ID = 1;
    private const int SPAWN_ID = 2;

    private bool casting;
    private bool precasting;
    private bool charging;

    private GameObject[] inUsePrecastObjs = new GameObject[4];
    private float[] particleSystemsStartSize = new float[4];

    private CameraScaler cameraScaler;
    private AudioSource audioSourceForPrecast;

    // Start is called before the first frame update
    protected override void Start()
	{
		base.Start();
        casting = false;
        precasting = false;
        charging = false;
        initializeParticleSystemsStartSize();
        fillPrefabPool();
        iceShardScript.addToPool(prefabPool);
        cameraScaler = GameObject.FindGameObjectWithTag("MainCamera").gameObject.GetComponent<CameraScaler>();
        xSpawnOffset = cameraScaler.Reso_width / 5;
        audioSourceForPrecast = GetComponent<AudioSource>();
        audioSourceForPrecast.volume = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (precasting)
        {
            currentPrecastTime -= Time.deltaTime;
            if (currentPrecastTime <= 0)
            {
                precasting = false;
                if (!charging)
                {
                    release();
                }
            }
        }
        else if (casting)
        {
            if (currentChargingTime < chargingTime)
            {
                currentChargingTime += Time.deltaTime;
            }
            else
            {
                currentChargingTime = chargingTime;
                release();
            }
        }
        else if (currentCooldownTime > 0)
        {
            currentCooldownTime -= Time.deltaTime;
        }
    }

	public override void select()
	{
        //Do nothing
    }

	public override void cast(Vector2 inputPos)
	{
        if (currentCooldownTime <= 0)
        {
            if (!casting)
            {
                // ==== LOGIC ====
                casting = true;
                precasting = true;
                currentPrecastTime = precastTime;
                charging = true;
                currentChargingTime = 0;
                //Precast effects
                for (int i = 0; i < 4; i++)
                {
                    inUsePrecastObjs[i] = prefabPool.getFromPool(PRECAST_ID, Precast);
                    inUsePrecastObjs[i].transform.position = new Vector2((i+1)*xSpawnOffset, UnitsTracker.getChampionByTag(tag).transform.position.y + ySpawnOffsetFromChampion);
                    inUsePrecastObjs[i].SetActive(true);
                }
                // ==== AUDIO ====
                audioSourceForPrecast.clip = precastAudio;
                audioSourceForPrecast.Play();
            }
            else if (!charging)
            {
                charging = true;
                currentChargingTime = 0;
            }
        }
	}

	public override void cancelCast()
	{
        if (precasting)
        {
            // ==== LOGIC ====
            casting = false;
            precasting = false;
            charging = false;
            currentPrecastTime = precastTime;
            //Deactivate precast effects
            for (int i = 0; i < 4; i++)
            {
                inUsePrecastObjs[i].SetActive(false);
                inUsePrecastObjs[i].transform.position = GameConstant.OUT_OF_CAMERA_POS;
            }
            //Reset precast audio
            audioSourceForPrecast.Stop();
            audioSourceForPrecast.clip = null;
        }
    }

	public override void release()
	{
        if (currentPrecastTime <= 0)
        {
            currentPrecastTime = precastTime;
            iceShardsSizeRatio = sizeMultiplierController * currentChargingTime / chargingTime;
            if (!charging || iceShardsSizeRatio < iceShardMinimumRatio)
            {
                iceShardsSizeRatio = iceShardMinimumRatio;
            }
            // ==== LOGIC ====
            charging = false;
            casting = false;
            currentCooldownTime = cooldownTime;
            recalliberParameters(iceShardsSizeRatio);
            // Activate projectile prefabs
            for (int i = 0; i < 4; i++)
            {
                GameObject iceShard = prefabPool.getFromPool(PROJECTILE_ID, ProjectileTrail);
                //Reset iceShard start size
                resetParticleSystemsStartSize(iceShard);
                iceShard.transform.position = new Vector2((i + 1) * xSpawnOffset, UnitsTracker.getChampionByTag(tag).transform.position.y + ySpawnOffsetFromChampion);
                Vector2 targetPos = UnitsTracker.getStandardTargetLocation(iceShard.transform.position, targetMask, tag);
                iceShard.GetComponent<IceShard>().IceShardSizeRatio = iceShardsSizeRatio;
                iceShard.GetComponent<IceShard>().spellRelease(targetPos, prefabPool);
                //Modify iceShard start size as a function of pointer down duration
                modifyParticleSystemsStartSize(iceShard);
                iceShard.SetActive(true);
            }
            // Activate Spawn Prefabs
            for (int i = 0; i < 4; i++)
            {
                GameObject iceShardSpawn = prefabPool.getFromPool(SPAWN_ID, Spawn);
                iceShardSpawn.transform.position = new Vector2((i + 1) * xSpawnOffset, UnitsTracker.getChampionByTag(tag).transform.position.y + ySpawnOffsetFromChampion);
                iceShardSpawn.SetActive(true);
            }
            // ==== AUDIO ====
            AudioCenter.playOneShot(spawnAudio, 0.2f * iceShardsSizeRatio);
        }
        else if (charging)
        {
            charging = false;
            currentChargingTime = chargingTime;
        }
    }

    private void fillPrefabPool()
    {
        prefabPool.addToPool(PRECAST_ID, Precast, 4);
        prefabPool.addToPool(SPAWN_ID, Spawn, 4);
        prefabPool.addToPool(PROJECTILE_ID, ProjectileTrail, 4);
    }

    private void initializeParticleSystemsStartSize()
    {
        ParticleSystem[] particleSystems = ProjectileTrail.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < 4; i++)
        {
            particleSystemsStartSize[i] = particleSystems[i].main.startSizeMultiplier;
        }
    }

    private void resetParticleSystemsStartSize(GameObject iceShard)
    {
        ParticleSystem[] particleSystems = iceShard.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < 4; i++)
        {
            var main = particleSystems[i].main;
            main.startSizeMultiplier = particleSystemsStartSize[i];
        }
    }

    private void modifyParticleSystemsStartSize(GameObject iceShard)
    {
        ParticleSystem[] particleSystems = iceShard.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem particleSystem in particleSystems)
        {
            var main = particleSystem.main;
            main.startSizeMultiplier *= iceShardsSizeRatio;
        }
    }

    private void recalliberParameters(float iceShardsRatio)
    {
        UnitsTracker.getChampionByTag(tag).CurrentMana -= manaCost * iceShardsRatio; 
    }
}
