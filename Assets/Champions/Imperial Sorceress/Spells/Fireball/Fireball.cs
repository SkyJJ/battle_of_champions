﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Fireball : MonoBehaviour, ISpell
{

	[SerializeField] private GameObject ExplosionPrefab;
	private const int EXPLOSION_ID = 3;

	[SerializeField] private AudioClip explosionClip;

	[SerializeField] private float projectileSpeed;
	[SerializeField] private float championDamage;
	[SerializeField] private float buildingDamage;
	[SerializeField] private float explosionRangeRadius;
	[SerializeField] private GameConstant.LayerId[] explosionAffectedLayers;


	private bool move = false;
	private float spawnPosY;
	private Vector2 targetDestination;
	private PrefabPool fireballPool;
	private Collider2D[] affectedObjects;
	private Rigidbody2D rb;

	public void spellInit()
	{
		affectedObjects = new Collider2D[GameConstant.MAX_SPELL_HIT_OBJECTS];
		rb = GetComponent<Rigidbody2D>();
	}

	public void spellReset()
	{
		this.gameObject.SetActive(false); //set projectile to not active	
		this.gameObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		move = false;
		Array.Clear(affectedObjects, 0, affectedObjects.Length);
	}

	void FixedUpdate() {
		if (move) {
			Vector2 direction = (targetDestination - (Vector2)transform.position).normalized;
			rb.MovePosition((Vector2)transform.position + direction * projectileSpeed * Time.fixedDeltaTime);
			if (crossedDestination()) {
				activateExplosion();
				spellReset();
			}
		}
	}

	private bool crossedDestination() {
		if (targetDestination.y > spawnPosY) 
			return (transform.position.y > targetDestination.y);
		else
			return (transform.position.y < targetDestination.y);
	}

	public void spellRelease(Vector2 targetDestination, PrefabPool pool) {
		fireballPool = pool;
		move = true;
		spawnPosY = transform.position.y;
		this.targetDestination = targetDestination;
	}

	public void fillPrefabPool(PrefabPool pool) {
		pool.addToPool(EXPLOSION_ID, ExplosionPrefab, 2);
	}


	private void OnTriggerEnter2D(Collider2D other)
	{
		activateExplosion();
		spellReset();
	}

	private void activateExplosion() {
		// ==== VISUAL ====
		GameObject explosionObj = fireballPool.getFromPool(EXPLOSION_ID, ExplosionPrefab);
		explosionObj.transform.position = this.transform.position;
		explosionObj.SetActive(true);
		// ==== AUDIO ====
		AudioCenter.playOneShot(explosionClip, 0.45f);
		// ==== LOGIC ====
		int numObjAffected = Physics2D.OverlapCircleNonAlloc(this.transform.position, explosionRangeRadius, affectedObjects, getFireballLayerMask());
		sendDamageToAffectedObjects(affectedObjects, numObjAffected);
	}

	private void sendDamageToAffectedObjects(Collider2D[] affectedObjs, int numObj)
	{
        Assert.IsTrue(numObj <= 3);
		for (int i = 0; i < numObj; i++) {
			//Debug.Log("affectedObj : " + affectedObjs[i].name);
			//Debug.Log("affectedObj : " + (IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable)));
			((IDamageable)affectedObjs[i].GetComponent(typeof(IDamageable))).receiveDamage(championDamage, buildingDamage);
		}	
	}

	private int getFireballLayerMask() {
		//return (int)GameConstant.LayerId.CastleAndPlayer + (int)GameConstant.LayerId.BuildingHeight1 + (int)GameConstant.LayerId.BuildingHeight2;
		int mask = 0;
		foreach (GameConstant.LayerId id in explosionAffectedLayers) {
			mask += (int)id;
		}

		Assert.IsTrue(mask != 0);
		return mask;
	}


}
