﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Precast, no charge or wait, targetable
 * */
public class FireballCaster : ActiveSpellCaster
{
	/*
	 * Inherited variables
	 * Designed data (serialized, protected) : icon, spellName, prefabPool, manaCost, cooldownTime, precastTime
	 * In game data (protected) : currentCooldownTime, currentPrecastTime
	 * */
	//Champion animation? Either store it here or in Champion class
	[SerializeField] private GameObject PrecastPrefab;
	[SerializeField] private GameObject ProjectilePrefab;
	[SerializeField] private GameObject ReleasePrefab;

	[SerializeField] private AudioClip precastAudio;
	[SerializeField] private AudioClip releaseAudio;

	[SerializeField] private Fireball fireballScript;

	[SerializeField] private float ySpawnOffsetFromChampion;
	[SerializeField] private UnitsTracker.TargetDisplayType targetDisplayType;
	[SerializeField] private UnitsTracker.TargetMask targetMask;

	private const int PRECAST_ID = 0;
	private const int PROJECTILE_ID = 1;
	private const int RELEASE_ID = 2;

	private bool precasting;
	private bool askedToRelease;

	private AudioSource audioSourceForPrecast;
	private GameObject inUsePrecastObj;
	private GameObject targetDisplayObject;
	private Vector2 fireballSpawnLocation;

	//TODO set bool block champion. But then in FireballStatue this bool should be false.
	/*
	Solve visual difference problem 
	Solve instantiating spell and tag problem
	Solve spawn and target destination

	Spawn position solved, offset solved.
	Now need to solve calculateTargetDisplayPosition problem. It's not really a prefab variant problem anymore.
	You need to modify UnitsTracker to handle targetDisplay for enemy side.
	And also getTargetDisplayObject. Fk. 
	*/

	protected override void Start()
	{
		base.Start();
		precasting = false;
		askedToRelease = false;
		fillPrefabPool();

		targetDisplayObject = UnitsTracker.getTargetDisplayObject(targetDisplayType, tag);
		audioSourceForPrecast = GetComponent<AudioSource>();
		audioSourceForPrecast.volume = 0.5f;

	}

	private void fillPrefabPool()
	{
		prefabPool.addToPool(PRECAST_ID, PrecastPrefab, 2);
		prefabPool.addToPool(RELEASE_ID, ReleasePrefab, 2);
		prefabPool.addToPool(PROJECTILE_ID, ProjectilePrefab, 2);
		fireballScript.fillPrefabPool(prefabPool);
	}

	// Update is called once per frame
	void Update()
	{
		if (precasting) {
			currentPrecastTime -= Time.deltaTime;
			if (currentPrecastTime <= 0)
				release();
		}
		else if (currentCooldownTime > 0) {
			currentCooldownTime -= Time.deltaTime;
		}
	}

	public override void select()
	{
		//Do nothing 
	}

	public override void cast(Vector2 inputPos)
	{
		if (currentCooldownTime <= 0)
		{
			if (precasting)
				//if it's already casting you only need to update the target display
				targetDisplayObject.transform.position = calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)));
			else if(!UnitsTracker.getChampionByTag(tag).ActionIsBlocked)
			{
				UnitsTracker.getChampionByTag(tag).ActionIsBlocked = true;
				castFireball(
					new Vector2(UnitsTracker.getChampionByTag(this.tag).transform.position.x, UnitsTracker.getChampionByTag(this.tag).transform.position.y + ySpawnOffsetFromChampion),
					calculateTargetDisplayPosition(new Vector2(GameConstant.convertToWorldSpaceX(inputPos.x), GameConstant.convertToWorldSpaceY(inputPos.y)))
					);
			}
		}

	}

	public void fireballStatueSpecificCast(Vector2 spawnPos, Vector2 worldInputPos) {
		if (currentCooldownTime <= 0)
		{
			castFireball(spawnPos, worldInputPos);
		}
	}

	private void castFireball(Vector2 spawnPos, Vector2 worldInputPos) {
		// ==== LOGIC ====
		precasting = true;
		currentPrecastTime = precastTime;
		// ==== VISUAL ====
		//Show target 
		targetDisplayObject.transform.position = worldInputPos;
		targetDisplayObject.gameObject.SetActive(true);
		//Precast effects
		inUsePrecastObj = prefabPool.getFromPool(PRECAST_ID, PrecastPrefab);
		inUsePrecastObj.transform.position = spawnPos;
		inUsePrecastObj.SetActive(true);
		// ==== AUDIO ====
		audioSourceForPrecast.clip = precastAudio;
		audioSourceForPrecast.Play();
	}

	public override void cancelCast()
	{
		if (askedToRelease == false && precasting) {
			resetPreCasting();
		}
	}

	public override void release()
	{
		askedToRelease = true;
		if (currentPrecastTime <= 0)
		{
			// ==== LOGIC ====
			Vector2 tempTargetPos = targetDisplayObject.transform.position;
			Vector2 tempPrecastPos = inUsePrecastObj.transform.position;
			resetPreCasting();
			askedToRelease = false;
			currentCooldownTime = cooldownTime;
            UnitsTracker.getChampionByTag(tag).CurrentMana -= manaCost;

            //Send Fireball
            GameObject fireball = prefabPool.getFromPool(PROJECTILE_ID, ProjectilePrefab);
			//fireball.transform.position = new Vector2(UnitsTracker.CurrentChampion.transform.position.x, UnitsTracker.CurrentChampion.transform.position.y + ySpawnOffsetFromChampion);
			fireball.transform.position = tempPrecastPos;
			fireball.SetActive(true);
			fireball.GetComponent<Fireball>().spellRelease(tempTargetPos, prefabPool);

			// ==== VISUAL ====
			//Play release animation
			GameObject releaseObj = prefabPool.getFromPool(RELEASE_ID, ReleasePrefab);
			//releaseObj.transform.position = new Vector2(UnitsTracker.CurrentChampion.transform.position.x, UnitsTracker.CurrentChampion.transform.position.y + ySpawnOffsetFromChampion);
			releaseObj.transform.position = tempPrecastPos;
			releaseObj.SetActive(true);

			// ==== AUDIO ====
			AudioCenter.playOneShot(releaseAudio, 0.5f);
		}
	}

	private void resetPreCasting() {
		//Update logic
		precasting = false;
		currentPrecastTime = precastTime;

		//Reset inUsePrecastObj
		inUsePrecastObj.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		inUsePrecastObj.SetActive(false);
		inUsePrecastObj = null;

		//Reset precast audio
		audioSourceForPrecast.Stop();
		audioSourceForPrecast.clip = null;

		//Reset target dispaly
		targetDisplayObject.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		targetDisplayObject.SetActive(false);

		//Free Champion
		UnitsTracker.getChampionByTag(tag).ActionIsBlocked = false;
	}



	private Vector3 calculateTargetDisplayPosition(Vector2 pos) {
		return UnitsTracker.getStandardTargetLocation(pos, targetMask, tag);
	}

	// ============= Functions to be implemented ================
	public void onReceiveBuildingUnitsChange()
	{
		//TODO onReceiveBuildingUnitsChange()
		//Think you will want to conver this to a interface or something. Research how C# or unity event works first 
	}

	// ====================================================================================================================
	// ======================== Properties and setter for FireballCaster configuration ====================================
	// ====================================================================================================================

	public void setTargetDisplayObjectTransparent() {
		SpriteRenderer s = targetDisplayObject.GetComponent<SpriteRenderer>();
		Color c = s.color;
		s.color = new Color(c.r, c.g, c.b, 0f);
	}

	public float ManaCost { set { manaCost = value; } }

	public float CooldownTime { set { cooldownTime = value; } }

	public float CurrentCooldownTime { get { return currentCooldownTime; } set { currentCooldownTime = value; } }

	public UnitsTracker.TargetMask TargetMask { get { return targetMask; } }
}
