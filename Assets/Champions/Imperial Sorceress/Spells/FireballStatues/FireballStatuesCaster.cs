﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
FireballStatuesCaster, FireballStatuesManager | FireballStatue
*/
public class FireballStatuesCaster : ActiveSpellCaster
{
	[SerializeField] private GameObject fireballPrecastPrefab;
	[SerializeField] private GameObject statuePrefab;

	[SerializeField] private Transform leftStatueTransform;

	[SerializeField] private AudioClip precastAudio;
	[SerializeField] private AudioClip releaseAudio;
	[SerializeField] private float yFireballPrecastOffsetFromChampion;
	[SerializeField] private GameObject fireballStatueTargetDisplay;
	[SerializeField] private float targetDisplayFadeTime;

	private const int PRECAST_ID = 0;
	private const int STATUE_ID = 1;

	private GameObject targetDisplayLeft;
	private GameObject targetDisplayRight;
	private FireballStatue fireballStatueScript;
	private bool precasting;
	private bool askedToRelease;
	private AudioSource audioSourceForPrecast;
	private GameObject inUsePrecastObj;

	// Start is called before the first frame update
	protected override void Start()
    {
		base.Start();

		targetDisplayLeft = GameObject.Instantiate(fireballStatueTargetDisplay);
		targetDisplayRight = GameObject.Instantiate(fireballStatueTargetDisplay);
		fireballStatueScript = statuePrefab.GetComponent<FireballStatue>();
		precasting = false;
		askedToRelease = false;
		audioSourceForPrecast = GetComponent<AudioSource>();
		audioSourceForPrecast.volume = 0.5f;
		inUsePrecastObj = null;
		fillPrefabPool();
	}

	private void fillPrefabPool() {
		prefabPool.addToPool(PRECAST_ID, fireballPrecastPrefab, 1);
		prefabPool.addToPool(STATUE_ID, statuePrefab, 2);
		fireballStatueScript.fillPrefabPool(prefabPool);
	}

	void Update()
    {
		if (precasting){
			currentPrecastTime -= Time.deltaTime;
			if (currentPrecastTime <= 0)
				release();
		}
		else if (currentCooldownTime > 0){
			currentCooldownTime -= Time.deltaTime;
		}
	}

	public override void select()
	{
		return;
	}

	public override void cast(Vector2 inputPos)
	{
		if (currentCooldownTime <= 0 && !precasting && !UnitsTracker.getChampionByTag(tag).ActionIsBlocked)
		{
			// ==== LOGIC ====
			precasting = true;
			currentPrecastTime = precastTime;
			UnitsTracker.getChampionByTag(tag).ActionIsBlocked = true;
			// ==== VISUAL ====
			//Precast effects
			inUsePrecastObj = prefabPool.getFromPool(PRECAST_ID, fireballPrecastPrefab);
			inUsePrecastObj.transform.position = new Vector2(UnitsTracker.getChampionByTag(tag).transform.position.x, UnitsTracker.getChampionByTag(tag).transform.position.y + yFireballPrecastOffsetFromChampion);
			inUsePrecastObj.SetActive(true);
			//Show target 
			targetDisplayLeft.transform.position = new Vector2(leftStatueTransform.position.x, leftStatueTransform.position.y + fireballStatueScript.getFireCircleOffset());
			targetDisplayLeft.gameObject.SetActive(true);
			targetDisplayRight.transform.position = new Vector2(9 - leftStatueTransform.position.x, leftStatueTransform.position.y + fireballStatueScript.getFireCircleOffset());
			targetDisplayRight.gameObject.SetActive(true);	
			// ==== AUDIO ====
			audioSourceForPrecast.clip = precastAudio;
			audioSourceForPrecast.Play();
		}
	}

	public override void cancelCast()
	{
		Debug.Log("FireballStatuesCaster cancelcast()");
		if (askedToRelease == false && precasting) {
			resetPreCasting();
			//Reset target display
			targetDisplayRight.transform.position = GameConstant.OUT_OF_CAMERA_POS;
			targetDisplayLeft.transform.position = GameConstant.OUT_OF_CAMERA_POS;
			targetDisplayRight.gameObject.SetActive(false);
			targetDisplayLeft.gameObject.SetActive(false);
		}		
	}


	public override void release()
	{
		askedToRelease = true;
		if (currentPrecastTime <= 0)
		{
			// ==== LOGIC ====
			resetPreCasting();
			askedToRelease = false;
			currentCooldownTime = cooldownTime;
			UnitsTracker.getChampionByTag(tag).CurrentMana -= manaCost;

			//Fade target display
			SpriteRenderer spriteCircleLeft = targetDisplayLeft.GetComponent<SpriteRenderer>();
			SpriteRenderer spriteCircleRight = targetDisplayRight.GetComponent<SpriteRenderer>();
			StartCoroutine(FadeAndDeactivateTargetDisplayRoutine(spriteCircleLeft, spriteCircleRight));

			//Release the spell 
			//GameObject statuesManager = prefabPool.getFromPool(STATUES_MANAGER_ID, fireballStatuesManagerPrefab);
			//statuesManager.SetActive(true);
			//statuesManager.GetComponent<FireballStatuesManager>().spellRelease(Vector2.zero, prefabPool);
			//GameObject fireballStatue = prefabPool.getFromPool(STATUE_ID, statuePrefab);
			prefabPool.getFromPool(STATUE_ID, statuePrefab).GetComponent<FireballStatue>().fireballStatueSpellRelease(
				 new Vector2(targetDisplayLeft.transform.position.x, targetDisplayLeft.transform.position.y - fireballStatueScript.getFireCircleOffset()), prefabPool, false);
			prefabPool.getFromPool(STATUE_ID, statuePrefab).GetComponent<FireballStatue>().fireballStatueSpellRelease(
				new Vector2(targetDisplayRight.transform.position.x, targetDisplayLeft.transform.position.y - fireballStatueScript.getFireCircleOffset()), prefabPool, true);

			// ==== AUDIO ====
			AudioCenter.playOneShot(releaseAudio, 0.8f);
		}
	}

	private IEnumerator FadeAndDeactivateTargetDisplayRoutine(SpriteRenderer spriteCircleLeft, SpriteRenderer spriteCircleRight)
	{
		Color oriColor = new Color(spriteCircleLeft.color.r, spriteCircleLeft.color.g, spriteCircleLeft.color.b, spriteCircleLeft.color.a);
		Color c = spriteCircleLeft.color; 
		for (float alpha = 1f; alpha >= 0; alpha -= 0.1f)
		{
			c.a = alpha;
			spriteCircleLeft.color = c;
			spriteCircleRight.color = c;
			yield return new WaitForSeconds(targetDisplayFadeTime / 10);
		}
		targetDisplayRight.gameObject.SetActive(false);
		targetDisplayLeft.gameObject.SetActive(false);
		//Reset the alpha value (else next time will be transparent when they're used)
		//I don't want them to reference the same Color. So I use create two new Color.
		spriteCircleLeft.color = new Color(oriColor.r, oriColor.g, oriColor.b, oriColor.a);
		spriteCircleRight.color = new Color(oriColor.r, oriColor.g, oriColor.b, oriColor.a);
	}

	private void resetPreCasting()
	{
		//Update logic
		precasting = false;
		currentPrecastTime = precastTime;

		//Reset inUsePrecastObj
		inUsePrecastObj.transform.position = GameConstant.OUT_OF_CAMERA_POS;
		inUsePrecastObj.SetActive(false);
		inUsePrecastObj = null;

		//Reset precast audio
		audioSourceForPrecast.Stop();
		audioSourceForPrecast.clip = null;

		//Free champion
		UnitsTracker.getChampionByTag(tag).ActionIsBlocked = false;
	}



}
