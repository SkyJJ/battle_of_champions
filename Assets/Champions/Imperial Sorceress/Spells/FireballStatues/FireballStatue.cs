﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballStatue : Building, ISpell
{
	//Inherited variables : buildId, currentHp, currentBuildTime, selected, isBuilt, spellCasters;
	[SerializeField] private GameObject fireCirlePrefab;
	[SerializeField] private GameObject dustPrefab;

	private const int FIRE_CIRCLE_ID = 2;
	private const int DUST_ID = 3;

	[SerializeField] private AudioClip fireCircleAudio;
	[SerializeField] private AudioClip statueMovingAudio;

	[SerializeField] private float fireCircleOffsetY;
	[SerializeField] private float dustOffsetY;
	[SerializeField] private float fireCircleSpawnDelay;
	[SerializeField] private float buildingSpawnDelayAfterFireCircle;

	[SerializeField] private GameObject canvas;
	[SerializeField] private GameObject activeFireballCaster;
	[SerializeField] private float fireballInstances;
	[SerializeField] private float fireballReleaseInterval;
	[SerializeField] private float fireballSpawnOffsetY;
	[SerializeField] private float waitTimeBeforeDescend;
	[SerializeField] private float buildingPosYOnSurface;
	[SerializeField] private float buildingPosYInGround;

	// === Variables regarding set up (before build) ===
	private bool waitingFireCircleSpawn;
	private bool waitingFireCircleFinished;
	private float currentFireCircleSpawnDelay;
	private float currentBuildingSpawnDelay;

	// === Variables regarding building ===
	private bool isBuilding;
	private GameObject theBuildingGameObject;

	// === Variables regarding fireball caster ===
	private FireballCaster fireballCaster;
	private float currentFireballInstances;

	// === Variables for ending ====
	private bool fireballFiringFinished;
	private float currentWaitTimeBeforeDescend;
	private bool descendingBuilding;
	private float currentDescendTime;

	// === Additional variables === 
	private PrefabPool prefabPool;
	private AudioSource audioSource;
	private bool needFlipX;

	public override void spellInit()
	{
		base.spellInit();

		//Set up before build
		waitingFireCircleSpawn = false;
		waitingFireCircleFinished = false;
		currentFireCircleSpawnDelay = fireCircleSpawnDelay;
		currentBuildingSpawnDelay = buildingSpawnDelayAfterFireCircle;

		//Building variables
		isBuilding = false;
		theBuildingGameObject = this.transform.GetChild(1).gameObject;

		//Fireball caster
		GameObject casterGo = GameObject.Instantiate(activeFireballCaster);
		casterGo.tag = this.tag;
		fireballCaster = casterGo.GetComponent<FireballCaster>();
		currentFireballInstances = fireballInstances;

		//Ending
		fireballFiringFinished = false;
		currentWaitTimeBeforeDescend = waitTimeBeforeDescend;
		descendingBuilding = false;
		currentDescendTime = buildTime;

		//Miscellaneous stuff
		audioSource = this.GetComponent<AudioSource>();
		currentHp = totalHp; // FireballStatue doesn't have build time. Its hp is already full when spawning from ground.
		updateUpUI();
	}

	private void Start() {
		//Calling it here because calling in spellInit posed problem (because of FireballCaster instantiation time)
		configurateFireballCaster(); 
	}

	private void configurateFireballCaster(){
		fireballCaster.ManaCost = 0f;
		fireballCaster.setTargetDisplayObjectTransparent();
		fireballCaster.CooldownTime = fireballReleaseInterval;
	}

	public void fireballStatueSpellRelease(Vector2 targetDestination, PrefabPool pool, bool flipX)
	{
		needFlipX = flipX;
		spellRelease(targetDestination, pool);
	}

	public override void spellRelease(Vector2 targetDestination, PrefabPool pool)
	{
		transform.position = targetDestination;
		prefabPool = pool;
		gameObject.SetActive(true);
		waitingFireCircleSpawn = true;
	}

	void Update()
	{
		if (waitingFireCircleSpawn)
		{
			currentFireCircleSpawnDelay -= Time.deltaTime;

			if (currentFireCircleSpawnDelay <= 0){
				waitingFireCircleSpawn = false;
				spawnFireCircle();
				waitingFireCircleFinished = true;
			}
		}

		else if (waitingFireCircleFinished) {
			currentBuildingSpawnDelay -= Time.deltaTime;
			if (currentBuildingSpawnDelay <= 0) {
				waitingFireCircleFinished = false;
				spawnStatue();
				isBuilding = true;
				canvas.SetActive(true);
				buildingCollider.enabled = true;
				registerToBuildingList();
			}
		}

		else if (isBuilding) {
			currentBuildTime -= Time.deltaTime;
			if (currentBuildTime <= 0) {
				isBuilding = false;
				isBuilt = true;
				reactivateSpellCasters();
				StartCoroutine(fireballFiring());
			}
		}

		else if (fireballFiringFinished && !descendingBuilding) {
			currentWaitTimeBeforeDescend -= Time.deltaTime;
			if (currentWaitTimeBeforeDescend <= 0) {
				descendStatue();
				descendingBuilding = true;
			}
		}

		else if (descendingBuilding) {
			currentDescendTime -= Time.deltaTime;
			if (currentDescendTime <= 0) {
				resetStatue();
			}
		}
	}

	private void spawnFireCircle() {
		// ==== LOGIC ====
		// ==== VISUAL ====
		GameObject fireCircle = prefabPool.getFromPool(FIRE_CIRCLE_ID, fireCirlePrefab);
		fireCircle.transform.position = new Vector2(transform.position.x, transform.position.y + fireCircleOffsetY);
		fireCircle.SetActive(true);
		// ==== AUDIO ====
		AudioCenter.playOneShot(fireCircleAudio, 0.35f);
	}

	private void spawnStatue() {
		//spawn statue
		theBuildingGameObject.GetComponent<SpriteRenderer>().flipX = needFlipX;
		//StartCoroutine(spawnStatueFromGround());
		StartCoroutine(animateStatueMoving(buildingPosYOnSurface));

		//dust visual
		GameObject dust = prefabPool.getFromPool(DUST_ID, dustPrefab);
		dust.transform.position = new Vector2(transform.position.x, transform.position.y + dustOffsetY);
		dust.SetActive(true);

		//statue moving audio
		audioSource.clip = statueMovingAudio;
		audioSource.volume = 0.5f;
		audioSource.Play();
	}

	private IEnumerator fireballFiring() {
		while (currentFireballInstances > 0)
		{
			if (fireballCaster.CurrentCooldownTime <= 0)
			{
				currentFireballInstances--;
				Building[] targetBuildingList = UnitsTracker.getBuildingsOnMapByMask(fireballCaster.TargetMask, tag).ToArray();
				fireballCaster.fireballStatueSpecificCast(new Vector2(this.transform.position.x, this.transform.position.y + fireballSpawnOffsetY), getRandomFireLocation(targetBuildingList));
				//Prevent cooldown time being 0 when precasting. Will be reset by caster itself later.
				fireballCaster.CurrentCooldownTime = fireballReleaseInterval;
			}
			yield return new WaitForSeconds(0.1f);
		}
		fireballFiringFinished = true;
	}

	private Vector2 getRandomFireLocation(Building[] enemyBuildingUnits){
		/*
		Castle proba : 4 / (4 + num of buildings on map)
		*/
		int castleIntRange = 4;
		int highestRange = castleIntRange + enemyBuildingUnits.Length;
		int randomInt = Random.Range(0, highestRange);
		Vector2 fireLocation;
		if (randomInt < castleIntRange)
		{
			//random again castle pos X
			float randomX = Random.Range(0f, 9f);
			fireLocation = (tag.Equals(GameConstant.PLAYER_TAG)) ?
							BuildingLocations.getStandardBuildingLocation(randomX, BuildingLocations.BuildingRow.ENEMY_CASTLE) :
							BuildingLocations.getStandardBuildingLocation(randomX, BuildingLocations.BuildingRow.CASTLE);
		}
		else
		{
			fireLocation = enemyBuildingUnits[randomInt - castleIntRange].gameObject.transform.position;
		}
		return fireLocation;
	}

	private void descendStatue() {
		//descend statue
		//StartCoroutine(descendStatueIntoGround());
		StartCoroutine(animateStatueMoving(buildingPosYInGround));

		//dust visual
		GameObject dust = prefabPool.getFromPool(DUST_ID, dustPrefab);
		dust.transform.position = new Vector2(transform.position.x, transform.position.y + dustOffsetY);
		dust.SetActive(true);

		//statue moving audio
		audioSource.clip = statueMovingAudio;
		audioSource.volume = 0.4f;
		audioSource.Play();
	}

	private IEnumerator animateStatueMoving(float destination)
	{
		float posY = theBuildingGameObject.transform.localPosition.y;
		float totalDistanceToMove = destination - posY;
		float moveDelta = Mathf.Abs( totalDistanceToMove / buildTime);
		float rate = 1f / 30; //This can change because of frame rate. 	
		float savedTime = Time.time;
		bool condition;
		if (destination > posY)
			condition = (posY < destination);
		else //when dest is smaller than posY
			condition = (posY > destination);

		//int numEnter = 0;
		//float startTimeDebug = Time.time;
		while (condition) {
			condition = (destination > posY) ? (posY < destination) : (posY > destination);
			posY = Mathf.MoveTowards(posY, destination, moveDelta * (Time.time - savedTime));
			theBuildingGameObject.transform.localPosition = new Vector2(theBuildingGameObject.transform.localPosition.x, posY);
			savedTime = Time.time;
			yield return new WaitForSeconds(rate);
		}
		//Debug.Log("Num enter : " + numEnter);
		//Debug.Log("Total build time : " + (Time.time - startTimeDebug));
	}

	private void resetStatue() {
		// ====== Object reset ======
		gameObject.SetActive(false);
		canvas.SetActive(false);
		buildingCollider.enabled = false;

		// ====== Spell release reset ======
		transform.position = Vector3.zero;
		prefabPool = null;

		// ====== Building unregistering ======
		unregisterFromBuildingList();

		// ====== Script attributes reset ======
		// === Base reset ===
		hpUI.fillAmount = 0f;
		selectOutline.color = Color.clear;
		currentHp = 0;
		currentBuildTime = buildTime;
		selected = false;
		isBuilt = false;

		// === Spell specific reset ===
		//Set up before build
		waitingFireCircleSpawn = false;
		waitingFireCircleFinished = false;
		currentFireCircleSpawnDelay = fireCircleSpawnDelay;
		currentBuildingSpawnDelay = buildingSpawnDelayAfterFireCircle;

		//Building variables
		isBuilding = false;

		//Fireball caster
		currentFireballInstances = fireballInstances;

		//Ending
		fireballFiringFinished = false;
		currentWaitTimeBeforeDescend = waitTimeBeforeDescend;
		descendingBuilding = false;
		currentDescendTime = buildTime;

		//Miscellaneous stuff
		currentHp = totalHp; 
		updateUpUI();
	}



	public void fillPrefabPool(PrefabPool pool){
		pool.addToPool(FIRE_CIRCLE_ID, fireCirlePrefab, 2);
		pool.addToPool(DUST_ID, dustPrefab, 2);
	}

	public float getFireCircleOffset() {
		return fireCircleOffsetY;
	}
}
