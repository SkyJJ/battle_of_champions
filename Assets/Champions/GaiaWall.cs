﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaiaWall : Building
{
	void Start() {
		spellInit();
	}

	public override void spellInit()
	{
		base.spellInit();
		currentHp = totalHp;
		isBuilt = true;
		updateUpUI();
		registerToBuildingList();
	}

	public override void spellRelease(Vector2 targetDestination, PrefabPool pool)
	{
		throw new System.NotImplementedException();
	}
}
